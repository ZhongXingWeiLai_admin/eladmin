
DROP TABLE IF EXISTS `medical_history`;
CREATE TABLE `medical_history` (
                                   `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
                                   `p_id` bigint(20) NOT NULL COMMENT '患者id',
                                   `first_morbidity_time` date DEFAULT NULL COMMENT '首次发病时间',
                                   `morbidity_age` int(11) DEFAULT NULL COMMENT '发病年龄',
                                   `progress` varchar(255) DEFAULT NULL COMMENT '病程',
                                   `confirmed_time` date DEFAULT NULL COMMENT '确诊时间',
                                   `t_interval` int(11) DEFAULT NULL COMMENT '发病与确诊时间间隔',
                                   `initial_symptom` varchar(255) DEFAULT NULL COMMENT '首发症状',
                                   `detail` varchar(255) DEFAULT NULL COMMENT '首发症状的具体内容',
                                   `reason` int(11) DEFAULT NULL COMMENT '诱因',
                                   `most_symptoms` varchar(255) DEFAULT NULL COMMENT '最重症状',
                                   `most_symptoms_time` date DEFAULT NULL COMMENT '病情最重时间',
                                   `osserman` int(11) DEFAULT NULL COMMENT 'osserman分型',
                                   `mgfa` int(11) DEFAULT NULL COMMENT 'mgfa分型',
                                   `img` varchar(255) DEFAULT NULL COMMENT '病例图片',
                                   `ignition` varchar(255) DEFAULT NULL COMMENT '眼外肌无力',
                                   `spherical_symptom` varchar(255) DEFAULT NULL COMMENT '球部症状',
                                   `fatigue` varchar(255) DEFAULT NULL COMMENT '肢体无力-疲劳',
                                   `plant_neurogenic` varchar(255) DEFAULT NULL COMMENT '植物神经受累',
                                   `is_most_symptoms` int(11) DEFAULT NULL COMMENT '是否有最重症状',
                                   `specific_cause` varchar(255) DEFAULT NULL COMMENT '具体诱因',
                                   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='现病史';

DROP TABLE IF EXISTS `patient`;
CREATE TABLE `patient`
(
    `id`                   bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `name`                 varchar(255) NOT NULL COMMENT '姓名',
    `sex`                  tinyint(4) DEFAULT '0' COMMENT '性别',
    `phone`                varchar(11)  DEFAULT NULL COMMENT '电话',
    `age`                  int(11) DEFAULT '0' COMMENT '年龄',
    `level`                int(11) DEFAULT '0' COMMENT '体力劳动等级',
    `nation`               varchar(255) DEFAULT NULL COMMENT '民族',
    `education_background` varchar(255) DEFAULT NULL COMMENT '教育程度',
    `marry`                tinyint(4) DEFAULT NULL COMMENT '是否结婚',
    `fertility`            varchar(255) DEFAULT NULL COMMENT '生育情况',
    `profession`           varchar(255) DEFAULT NULL COMMENT '职业',
    `first_hospital`       varchar(255) DEFAULT NULL COMMENT '首诊医院',
    `first_office`         varchar(255) DEFAULT NULL COMMENT '首诊科室',
    `draw_blood`           tinyint(4) DEFAULT NULL COMMENT '是否抽血',
    `join_time`            datetime     DEFAULT NULL COMMENT '入组时间',
    `contacts`             varchar(255) DEFAULT NULL COMMENT '联系人',
    `phone1`               varchar(11)  DEFAULT NULL COMMENT '联系电话1',
    `phone2`               varchar(11)  DEFAULT NULL COMMENT '联系电话2',
    `province`             varchar(255) DEFAULT NULL COMMENT '省',
    `city`             varchar(255) DEFAULT NULL COMMENT '市',
    `area`             varchar(255) DEFAULT NULL COMMENT '区',
    `province2`             varchar(255) DEFAULT NULL COMMENT '长期居住地省',
    `city2`             varchar(255) DEFAULT NULL COMMENT '长期居住地市',
    `area2`             varchar(255) DEFAULT NULL COMMENT '长期居住区',
    `birth`             datetime DEFAULT NULL COMMENT '出生日期',
    `money`             int(11) DEFAULT NULL COMMENT '家庭收入',
    `percentage`             int(11) DEFAULT NULL COMMENT '病例完整度',
    `id_card`             varchar(25) DEFAULT NULL COMMENT '身份证号',
    `relation`             varchar(255) DEFAULT NULL COMMENT '关系',
    `doctor`             varchar(255) DEFAULT NULL COMMENT '负责医生',
    `affiliation`             varchar(255) DEFAULT NULL COMMENT '所属机构',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='患者';



DROP TABLE IF EXISTS `treatment_before_enrollment`;
CREATE TABLE `treatment_before_enrollment`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `state`                  tinyint(4) DEFAULT '0' COMMENT '入組前是否接受治療',
    `mg_treatment`                varchar(255)  DEFAULT NULL COMMENT '用药',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='入组前治疗';
alter table treatment_before_enrollment add UNIQUE index treatment_before_enrollmentid (p_id);




DROP TABLE IF EXISTS `drug_details`;
CREATE TABLE `drug_details`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `d_id`                bigint(20)  DEFAULT NULL COMMENT '药编号',
    `start_date`                  datetime DEFAULT NULL COMMENT '开始用药时间',
    `end_date`         datetime DEFAULT NULL COMMENT '结束用药时间',
    `days`                varchar(255)  DEFAULT NULL COMMENT '用药天数',
    `use_method`                varchar(255)  DEFAULT NULL COMMENT '用法',
    `onset_date`                datetime  DEFAULT NULL COMMENT '起效时间',
    `side_effect`                varchar(255)  DEFAULT NULL COMMENT '副作用',
    `stop_state`                varchar(255)  DEFAULT NULL COMMENT '是否停药',
    `reason`                varchar(255)  DEFAULT NULL COMMENT '停药原因',
    `aggravate_state`                varchar(255)  DEFAULT NULL COMMENT '是否-过性加重',
    `treatment_details`                varchar(255)  DEFAULT NULL COMMENT '治疗情况',
    `gene`                varchar(255)  DEFAULT NULL COMMENT '基因',
    `suppression_position`                varchar(255)  DEFAULT NULL COMMENT '抑制位置',
    `genotype`                varchar(255)  DEFAULT NULL COMMENT '基因型',
    `take`                varchar(255)  DEFAULT NULL COMMENT '每次服用（粒）',
    `dose`                varchar(255)  DEFAULT NULL COMMENT '每次剂量（mg）',
    `dose_today`                varchar(255)  DEFAULT NULL COMMENT '当日服药剂量（mg）',
    `dose_total`                varchar(255)  DEFAULT NULL COMMENT '目前总服药剂量',
    `improve`                varchar(255)  DEFAULT NULL COMMENT '服药后症状改善情况/使用效果',
    `abnormal_describe`                varchar(255)  DEFAULT NULL COMMENT '异常指标描述',
    `side_effect_abnormal_index`                varchar(255)  DEFAULT NULL COMMENT '副作用异常指标',
    `ta_check`                varchar(255)  DEFAULT NULL COMMENT '他克莫司相关检查',
    `ta_treatment`                varchar(255)  DEFAULT NULL COMMENT '他克莫司治疗情况',
    `ta_concentration`                varchar(255)  DEFAULT NULL COMMENT '他克莫司（ng/ml）浓度',
    `ta_detection_date`                varchar(255)  DEFAULT NULL COMMENT '检测日期',
    `ta_take_wu`                varchar(255)  DEFAULT NULL COMMENT '服用五酯滴丸',
    `ta_take_wu_dose`                varchar(255)  DEFAULT NULL COMMENT '日用药剂量（mg/kg）五酯滴丸',
    `ta_take_wu_effective_time`                varchar(255)  DEFAULT NULL COMMENT '起效时间',
    `ta_take_wu_cd`                varchar(255)  DEFAULT NULL COMMENT 'C/D值五酯滴丸',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用药详情';
alter table drug_details add UNIQUE index idx_drug_detailsid (p_id,d_id);


DROP TABLE IF EXISTS `medical_history_before`;
CREATE TABLE `medical_history_before`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `have_medical_history`                tinyint(20)  DEFAULT NULL COMMENT '有无既往病史',
    `medical_history_details_string`  varchar(255) DEFAULT NULL COMMENT '病史详情',
    `have_medical_family_history`                tinyint(20)  DEFAULT NULL COMMENT '有无家族病史',
    `relationship`                int(11)   DEFAULT NULL COMMENT '与患者关系',
    `pedigree`                  varchar(255) DEFAULT NULL COMMENT '系谱图',
    `have_immune_medical_family_history`      tinyint(20)  DEFAULT NULL COMMENT '免疫系统疾病家族史有无',
    `immune_medical_family_name_string` varchar(255) DEFAULT NULL COMMENT '免疫系统疾病家族史_诊断名称',
    `smoke`      int(11)  DEFAULT NULL COMMENT '吸烟史',
    `quit_smoking`      int(11)  DEFAULT NULL COMMENT '是否戒烟:',
    `drink`      int(11)  DEFAULT NULL COMMENT '饮酒史:',
    `quit_drinking`      int(11)  DEFAULT NULL COMMENT '是否戒酒:',
    `is_ruzu`      int(11)  DEFAULT NULL COMMENT '患者入组情况:',
    `remarks`      varchar(255)  DEFAULT NULL COMMENT '备注::',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='既往史';


DROP TABLE IF EXISTS `past_disease`;
CREATE TABLE `past_disease`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `disease_name`                varchar(255)  DEFAULT NULL COMMENT '疾病名称',
    `diagnosis_name`                varchar(255)  DEFAULT NULL COMMENT '诊断名称/心率失常等，分割',
    `years`                varchar(255)  DEFAULT NULL COMMENT '年份',
    `treatment`                varchar(255)  DEFAULT NULL COMMENT '治疗方式',
    `medicine`                varchar(255)  DEFAULT NULL COMMENT '服用药物',

    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='既往疾病';


DROP TABLE IF EXISTS `past_disease_info`;
CREATE TABLE `past_disease_info`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `disease_name`                varchar(255)  DEFAULT NULL COMMENT '疾病名称',
    `diagnosis_name`                varchar(255)  DEFAULT NULL COMMENT '免疫系统疾病和眼科疾病的名称',
    `years`                varchar(255)  DEFAULT NULL COMMENT '年份',
    `medicine`                varchar(255)  DEFAULT NULL COMMENT '服用药物',

    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='免疫系统疾病和眼科疾病的细分名称';



DROP TABLE IF EXISTS `immune_system_disease_family_history`;
CREATE TABLE `immune_system_disease_family_history`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `name`                varchar(255)  DEFAULT NULL COMMENT '免疫系统疾病家族史_诊断名称',
    `relationship`                int(11)   DEFAULT NULL COMMENT '与患者关系',
    `pedigree`                  varchar(255) DEFAULT NULL COMMENT '系谱图',

    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='免疫系统疾病家族史';




DROP TABLE IF EXISTS `health_checkup`;
CREATE TABLE `health_checkup`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `height`                varchar(255)  DEFAULT NULL COMMENT '身高',
    `weight`                  varchar(255) DEFAULT NULL COMMENT '体重',
    `bmi`                varchar(255)  DEFAULT NULL COMMENT 'BMI',
    `blepharoptosis`                int(11)   DEFAULT NULL COMMENT '眼睑下垂',
    `eye_movement_disorder`                  int(11) DEFAULT NULL COMMENT '眼动障碍',
    `diplopia`      int(11)  DEFAULT NULL COMMENT '复视',
    `sorenes`      int(11)  DEFAULT NULL COMMENT '闭目',
    `mumps`      int(11)  DEFAULT NULL COMMENT '鼓腮',
    `dysarthria`      int(11)  DEFAULT NULL COMMENT '构音障碍:',
    `drink`      int(11)  DEFAULT NULL COMMENT '屈颈:',
    `neck_flexion`      int(11)  DEFAULT NULL COMMENT '伸颈:',
    `toe_walking`      int(11)  DEFAULT NULL COMMENT '脚尖走路:',
    `heel_walking`      int(11)  DEFAULT NULL COMMENT '脚跟走路',
    `squat`      int(11)  DEFAULT NULL COMMENT '蹲起',
    `muscular_strength1`      int(11)  DEFAULT NULL COMMENT '左上肢近端肌力:',
    `muscular_strength2`      int(11)  DEFAULT NULL COMMENT '左上肢远端肌力',
    `muscular_strength3`      int(11)  DEFAULT NULL COMMENT '右上肢近端肌力',
    `muscular_strength4`      int(11)  DEFAULT NULL COMMENT '右上肢远端肌力',
    `muscular_strength5`      int(11)  DEFAULT NULL COMMENT '左下肢近端肌力',
    `muscular_strength6`      int(11)  DEFAULT NULL COMMENT '左下肢远端肌力',
    `muscular_strength7`      int(11)  DEFAULT NULL COMMENT '右下肢近端肌力',
    `muscular_strength8`      int(11)  DEFAULT NULL COMMENT '右下肢远端肌力',
    `eye`      int(11)  DEFAULT NULL COMMENT '眼肌型量表',
    `osserman`      int(11)  DEFAULT NULL COMMENT 'Osserman分型',
    `mgfa`      int(11)  DEFAULT NULL COMMENT 'MGFA',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='体格检查';

DROP TABLE IF EXISTS `mg_adl_score`;
CREATE TABLE `mg_adl_score`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `speech`      int(11)  DEFAULT NULL COMMENT '言语:',
    `chew`      int(11)  DEFAULT NULL COMMENT '咀嚼',
    `swallow`      int(11)  DEFAULT NULL COMMENT '吞咽',
    `breathing`      int(11)  DEFAULT NULL COMMENT '呼吸',
    `teeth_hair`      int(11)  DEFAULT NULL COMMENT '刷牙或梳头',
    `sit_up`      int(11)  DEFAULT NULL COMMENT '坐立站起',
    `diplopia`      int(11)  DEFAULT NULL COMMENT '复视',
    `blepharoptosis`      int(11)  DEFAULT NULL COMMENT '眼睑下垂',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='MG-ADL评分量表';

DROP TABLE IF EXISTS `qmg_score`;
CREATE TABLE `qmg_score`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `diplopia`      int(11)  DEFAULT NULL COMMENT '左右视出现复视',
    `blepharoptosis`      int(11)  DEFAULT NULL COMMENT '上视出现眼睑下垂',
    `eyelid_closure`      int(11)  DEFAULT NULL COMMENT '眼睑闭合:',
    `swallow`      int(11)  DEFAULT NULL COMMENT '吞咽100ml水',
    `count`      int(11)  DEFAULT NULL COMMENT '数数1-50',
    `sit_up1`      int(11)  DEFAULT NULL COMMENT '坐位右上肢抬起90度',
    `sit_up2`      int(11)  DEFAULT NULL COMMENT '坐位左上肢抬起90度',
    `vital_capacity`      int(11)  DEFAULT NULL COMMENT '肺活量（%预计值）',
    `right_hand_grip`      int(11)  DEFAULT NULL COMMENT '右手握力kgW',
    `left_hand_grip`      int(11)  DEFAULT NULL COMMENT '左手握力kgW',
    `pwwtt`      int(11)  DEFAULT NULL COMMENT '平卧位抬头（秒）',
    `pwwtq1`      int(11)  DEFAULT NULL COMMENT '平卧位右下肢抬起（秒）',
    `pwwtq2`      int(11)  DEFAULT NULL COMMENT '平卧位左下肢抬起（秒）',
    `sex`      int(11)  DEFAULT NULL COMMENT '握力性别',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='QMG评分量表';


DROP TABLE IF EXISTS `mg_score`;
CREATE TABLE `mg_score`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `Eyelid`      int(11)  DEFAULT NULL COMMENT '眼睑下垂（上视）',
    `diplopia`      int(11)  DEFAULT NULL COMMENT '复视（左右视）',
    `close`      int(11)  DEFAULT NULL COMMENT '闭目',
    `speech`      int(11)  DEFAULT NULL COMMENT '言语',
    `chew`      int(11)  DEFAULT NULL COMMENT '咀嚼',
    `swallow`      int(11)  DEFAULT NULL COMMENT '吞咽',
    `breathe`      int(11)  DEFAULT NULL COMMENT '呼吸',
    `flexible_neck`      int(11)  DEFAULT NULL COMMENT '屈伸颈',
    `shoulder_outfield`      int(11)  DEFAULT NULL COMMENT '肩部外展',
    `pile`      int(11)  DEFAULT NULL COMMENT '屈髋（平卧位）',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='QMG评分量表';



DROP TABLE IF EXISTS `mg_qol_score`;
CREATE TABLE `mg_qol_score`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `mg_qol1`      int(11)  DEFAULT NULL COMMENT '我对自己的病情有挫折感',
    `mg_qol2`      int(11)  DEFAULT NULL COMMENT '我用眼有困难',
    `mg_qol3`      int(11)  DEFAULT NULL COMMENT '我吃东西有困难',
    `mg_qol4`      int(11)  DEFAULT NULL COMMENT '因为病情而导致我社交活动受限',
    `mg_qol5`      int(11)  DEFAULT NULL COMMENT '我的病情限制了我的兴趣爱好活动',
    `mg_qol6`      int(11)  DEFAULT NULL COMMENT '我不能很好地满足家庭需要',
    `mg_qol7`      int(11)  DEFAULT NULL COMMENT '我需要根据我的身体情况定制计划',
    `mg_qol8`      int(11)  DEFAULT NULL COMMENT '我的职业技能和工作状态受到了影响',
    `mg_qol9`      int(11)  DEFAULT NULL COMMENT '我说话有困难',
    `mg_qol10`      int(11)  DEFAULT NULL COMMENT '我骑车、开车有困难',
    `mg_qol11`      int(11)  DEFAULT NULL COMMENT '我对我的病情感到抑郁',
    `mg_qol12`      int(11)  DEFAULT NULL COMMENT '我行走有困难',
    `mg_qol13`      int(11)  DEFAULT NULL COMMENT '我对我的病情感到悲哀、悲痛、绝望',
    `mg_qol14`      int(11)  DEFAULT NULL COMMENT '我在个人打扮、穿着方面有困难、悲痛、绝望',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='MG-QOL15评分量表';


DROP TABLE IF EXISTS `ocular_qmg_score`;
CREATE TABLE `ocular_qmg_score`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `ocular_qmg1`      int(11)  DEFAULT NULL COMMENT '上睑下垂',
    `ocular_qmg2`      int(11)  DEFAULT NULL COMMENT '左凝视',
    `ocular_qmg3`      int(11)  DEFAULT NULL COMMENT '右凝视',
    `ocular_qmg4`      int(11)  DEFAULT NULL COMMENT '上凝视',
    `ocular_qmg5`      int(11)  DEFAULT NULL COMMENT '下凝视',
    `ocular_qmg6`      int(11)  DEFAULT NULL COMMENT '眼睑闭合',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ocular-QMG评分量表';

DROP TABLE IF EXISTS `eye_movements_score`;
CREATE TABLE `eye_movements_score`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `eye_movements1`      int(11)  DEFAULT NULL COMMENT '左眼向上',
    `eye_movements2`      int(11)  DEFAULT NULL COMMENT '左眼向下',
    `eye_movements3`      int(11)  DEFAULT NULL COMMENT '左眼向左',
    `eye_movements4`      int(11)  DEFAULT NULL COMMENT '左眼向右',
    `eye_movements5`      int(11)  DEFAULT NULL COMMENT '右眼向上',
    `eye_movements6`      int(11)  DEFAULT NULL COMMENT '右眼向下',
    `eye_movements7`      int(11)  DEFAULT NULL COMMENT '右眼向左',
    `eye_movements8`      int(11)  DEFAULT NULL COMMENT '右眼向右',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='EyeMovements评分量表';

DROP TABLE IF EXISTS `typing_record`;
CREATE TABLE `typing_record`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `ossrman`      int(11)  DEFAULT NULL COMMENT 'Ossrman分型记录',
    `mgfa`      int(11)  DEFAULT NULL COMMENT 'MGFA分型记录',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='EyeMovements评分量表';

DROP TABLE IF EXISTS `inspection_results`;
CREATE TABLE `inspection_results`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`    bigint(20) NOT NULL  COMMENT 'pID',
    `type`    int(11) DEFAULT NULL COMMENT '检查类型',
    `result`  varchar(255) DEFAULT NULL COMMENT '检查结果（多结果，分割）',
    `picture` varchar(255) DEFAULT NULL COMMENT '图片',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='检查';

DROP TABLE IF EXISTS `antibody_detection`;
CREATE TABLE `antibody_detection`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `date`      int(11)  DEFAULT NULL COMMENT '检测时间',
    `mechanism`      varchar(255)  DEFAULT NULL COMMENT '检测机构',
    `source`      varchar(255)  DEFAULT NULL COMMENT '数据来源',
    `picture`      varchar(255)  DEFAULT NULL COMMENT '图片',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='抗体检测';



DROP TABLE IF EXISTS `antibody_detection_project`;
CREATE TABLE `antibody_detection_project`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`        bigint(20) NOT NULL  COMMENT 'pID',
    `a_id`        bigint(20) NOT NULL  COMMENT '抗体检测ID',
    `antibody_type`      int(11)  DEFAULT NULL COMMENT '检测时间',
    `antibody_result`      varchar(255)  DEFAULT NULL COMMENT '结果',
    `antibody_method`      varchar(255)  DEFAULT NULL COMMENT '检查方法',
    `antibody_num`      varchar(255)  DEFAULT NULL COMMENT '数值结果',
    `picture`      varchar(255)  DEFAULT NULL COMMENT '图片',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='抗体检测项目';


DROP TABLE IF EXISTS `baseline_treatment`;
CREATE TABLE `baseline_treatment`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`        bigint(20) NOT NULL  COMMENT 'pID',
    `mg_drug_treatment`      varchar(255)  DEFAULT NULL COMMENT 'MG药物治疗',
    `mg_immune_treatment`      int(11)  DEFAULT NULL COMMENT '有无MG免疫治疗',
    `thymic_surgery_treatment`      int(11)  DEFAULT NULL COMMENT '有无胸腺手术治疗',
    `other_treatment`      int(11)  DEFAULT NULL COMMENT '有无其他治疗',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='基线治疗';


DROP TABLE IF EXISTS `other_treatment`;
CREATE TABLE `other_treatment`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`        bigint(20) NOT NULL  COMMENT 'pID',
    `other_treatment_info`      varchar(255)  DEFAULT NULL COMMENT '其他治疗情况',
    `other_treatment_date`      datetime  DEFAULT NULL COMMENT '其他治疗起效时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='药物';

DROP TABLE IF EXISTS `mg_immune_treatment`;
CREATE TABLE `mg_immune_treatment`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`        bigint(20) NOT NULL  COMMENT 'pID',
    `glycopathy_use_date`      datetime  DEFAULT NULL COMMENT '丙种球蛋白_使用日期',
    `glycopathy_dose`       varchar(255) DEFAULT NULL COMMENT '丙种球蛋白_使用剂量',
    `glycopathy_effective_time`      datetime  DEFAULT NULL COMMENT '丙种球蛋白_起效时间',
    `glycopathy_use_effect`      varchar(255)  DEFAULT NULL COMMENT '丙种球蛋白_使用效果',
    `plasma_replacement_date`      datetime  DEFAULT NULL COMMENT '血浆置换日期',
    `plasma_replacement_time`      datetime  DEFAULT NULL COMMENT '血浆置换_起效时间',
    `plasma_replacement`      varchar(255)  DEFAULT NULL COMMENT '血浆置换内容',
    `immune_adsorption_date`      datetime  DEFAULT NULL COMMENT '免疫吸附日期',
    `immune_adsorption_effective_time`      datetime  DEFAULT NULL COMMENT '免疫吸附_起效时间',
    `immune_adsorption_content`      varchar(255)  DEFAULT NULL COMMENT '免疫吸附内容',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='MG免疫治疗';

DROP TABLE IF EXISTS `baseline_treatment_details`;
CREATE TABLE `baseline_treatment_details`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`        bigint(20) NOT NULL  COMMENT 'pID',
    `d_id`       bigint(20)  NOT NULL  COMMENT '药物ID',
    `daily_usage_first`      varchar(255)  DEFAULT NULL COMMENT '入组当日用法（次/日）',
    `take_num_first`      varchar(255)  DEFAULT NULL COMMENT '入组当日每次服用',
    `dose_first`      varchar(255)  DEFAULT NULL COMMENT '每次剂量（mg）',
    `dose_count_first`      varchar(255)  DEFAULT NULL COMMENT '当日服药剂量（mg）',
    `starting_medication_time`     datetime  DEFAULT NULL COMMENT '起始用药时间',
    `end_medication_time`     datetime  DEFAULT NULL COMMENT '结束用药时间',
    `daily_usage`      varchar(255)  DEFAULT NULL COMMENT '用法（次/日）',
    `take_num`      varchar(255)  DEFAULT NULL COMMENT '每次服用（粒）',
    `dose`      varchar(255)  DEFAULT NULL COMMENT '每次剂量（mg）',
    `dose_count`     varchar(255)  DEFAULT NULL COMMENT '当日服药剂量（mg）',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='基线治疗详细信息';

DROP TABLE IF EXISTS `drug`;
CREATE TABLE `drug`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `name`      varchar(255)  DEFAULT NULL COMMENT '药名字',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='药物';

DROP TABLE IF EXISTS `thymic_surgery_history`;
CREATE TABLE `thymic_surgery_history`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`        bigint(20) NOT NULL  COMMENT 'pID',
    `thymic_surgery_time`      datetime DEFAULT NULL COMMENT '胸腺手术时间',
    `time_interval`      varchar(255) DEFAULT NULL COMMENT '与首发症状的时间间隔',
    `crisis`      int(11) DEFAULT NULL COMMENT '术前肌无力危象',
    `crisis_times`      varchar(255) DEFAULT NULL COMMENT '危象次数',
    `mgfa`      varchar(255) DEFAULT NULL COMMENT '术前MGFA分型(术前1周)',
    `spherical_muscle`      int(11) DEFAULT NULL COMMENT '有无球部肌肉受累',
    `preoperative_medication`      int(11) DEFAULT NULL COMMENT '术前用药（1月内）',
    `medication_type`      varchar(255) DEFAULT NULL COMMENT '用药种类',
    `dose`      varchar(255) DEFAULT NULL COMMENT '溴吡斯的明日剂量',
    `preoperative_use_time`      datetime DEFAULT NULL COMMENT '溴吡斯的明术前使用时间',
    `immunosuppressive`      varchar(255) DEFAULT NULL COMMENT '免疫抑制剂',
    `bing_qiu`      int(11) DEFAULT NULL COMMENT '术前丙球冲击（1月内）',
    `hormone`      int(11) DEFAULT NULL COMMENT '术前激素冲击（1月内）',
    `plasma`      int(11) DEFAULT NULL COMMENT '术前血浆置换（1月内）',
    `fei_gong`      int(11) DEFAULT NULL COMMENT '术前肺功',
    `fcv`      varchar(255) DEFAULT NULL COMMENT 'FVC',
    `fev1`      varchar(255) DEFAULT NULL COMMENT 'FEV1',
    `muscle_weakness`      int(11) DEFAULT NULL COMMENT '术前肌无力症状',
    `surgical_approach`      int(11) DEFAULT NULL COMMENT '手术方式',
    `surgery`      int(11) DEFAULT NULL COMMENT '剑突下肋缘三孔手术切除:',
    `scope`      int(11) DEFAULT NULL COMMENT '切除范围',
    `surgical_duration`      varchar(255) DEFAULT NULL COMMENT '手术时长',
    `lose_weight`      varchar(255) DEFAULT NULL COMMENT '术中失血量（ml）',
    `muscle_weakness_after`      int(11) DEFAULT NULL COMMENT '术后肌无力危象',
    `complication`      varchar(255) DEFAULT NULL COMMENT '术后并发症',
    `pathology`      varchar(255) DEFAULT NULL COMMENT '术后病理',
    `picture`      varchar(255) DEFAULT NULL COMMENT '图片',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='胸腺手术史';

DROP TABLE IF EXISTS `immunosuppressant`;
CREATE TABLE `immunosuppressant`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `d_name`                varchar(255)  DEFAULT NULL COMMENT '抑制剂名字',
    `dose`                varchar(255)  DEFAULT NULL COMMENT '剂量',
    `duration`                varchar(255)  DEFAULT NULL COMMENT '术前使用时长',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='胸腺手术史免疫抑制剂使用情况';

DROP TABLE IF EXISTS `health_economics`;
CREATE TABLE `health_economics`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`        bigint(20) NOT NULL  COMMENT 'pID',
    `follow_up_start_date`        datetime DEFAULT NULL  COMMENT '实际随访日期-起始时间',
    `follow_up_end_date`        datetime DEFAULT NULL  COMMENT '实际随访日期-起始时间',
    `mg_cost`      varchar(255) DEFAULT NULL COMMENT 'MG治疗总费用（元）',
    `medical_insurance_type`      varchar(255) DEFAULT NULL COMMENT '医保类型',
    `is_remote_medical`      int(11) DEFAULT NULL COMMENT '是否异地就医',
    `edical_insurance_reimbursement_ratio`      varchar(255) DEFAULT NULL COMMENT '医保报销比例',
    `is_have_supplementary_medical_insurance`      int(11) DEFAULT NULL COMMENT '是否有补充医疗保险',
    `business_insurance_type`      varchar(255) DEFAULT NULL COMMENT '商业保险类型',
    `business_insurance_reimbursement_ratio`      varchar(255) DEFAULT NULL COMMENT '商业保险报销比例',
    `edical_insurance_reimbursement_cost`      varchar(255) DEFAULT NULL COMMENT '医保报销费用（元）',
    `own_cost`      varchar(255) DEFAULT NULL COMMENT '自费金额（元）',
    `mg_hospitalization_start_date`      datetime DEFAULT NULL COMMENT 'MG患者住院起始时间',
    `mg_hospitalization_end_date`      datetime DEFAULT NULL COMMENT 'MG患者住院结束时间',
    `mg_hospitalization_days`      varchar(255) DEFAULT NULL COMMENT 'MG患者住院时长（天）',
    `mg_hospitalization_cost`      varchar(255) DEFAULT NULL COMMENT 'MG患者住院期间总费用（元）',
    `extra_hospital_treatment_cost`      varchar(255) DEFAULT NULL COMMENT '院外附加治疗费用（元）',
    `blood_products_protein_products_cost`      varchar(255) DEFAULT NULL COMMENT '血液制品和蛋白质制品费用（元）',
    `family_members`      varchar(255) DEFAULT NULL COMMENT '陪护家属人数',
    `visits_number_transportation`      varchar(255) DEFAULT NULL COMMENT '乘坐交通工具前往就诊次数',
    `transportation_cost_average`      varchar(255) DEFAULT NULL COMMENT '平均每次人均交通费用（元）',
    `total_transportation_cost`      varchar(255) DEFAULT NULL COMMENT '总交通费（元）',
    `out_of_hospital_accommodation_days`      varchar(255) DEFAULT NULL COMMENT '患者院外住宿时长（天）',
    `out_of_hospital_accommodation_cost_average`      varchar(255) DEFAULT NULL COMMENT '患者院外住宿日均费用（元）',
    `diet_cost_average`      varchar(255) DEFAULT NULL COMMENT '患者膳食日均费用（元）',
    `family_members_out_of_hospital_accommodation_days`      varchar(255) DEFAULT NULL COMMENT '家属院外住宿时长（天）',
    `family_members_out_of_hospital_accommodation_cost_average`      varchar(255) DEFAULT NULL COMMENT '家属院外住宿日均费用（元）',
    `family_members_diet_cost_average`      varchar(255) DEFAULT NULL COMMENT '家属膳食日均费用（元）',
    `total_diet_cost`      varchar(255) DEFAULT NULL COMMENT '总食宿费（元）',
    `paramedic_num`      varchar(255) DEFAULT NULL COMMENT '护理人员人数',
    `escort_fee`      varchar(255) DEFAULT NULL COMMENT '总陪护费（非家属）（元）',
    `non_medical_costs`      varchar(255) DEFAULT NULL COMMENT '直接非医疗成本总费用（元）',
    `days_lost`      varchar(255) DEFAULT NULL COMMENT '患者误工天数',
    `work_type`      varchar(255) DEFAULT NULL COMMENT '患者工种',
    `loss_cost`      varchar(255) DEFAULT NULL COMMENT '患者误工总损失费（元）',
    `family_members_days_lost`      varchar(255) DEFAULT NULL COMMENT '家属误工天数',
    `family_members_work_type`      varchar(255) DEFAULT NULL COMMENT '家属工种',
    `family_members_loss_cost`      varchar(255) DEFAULT NULL COMMENT '家属误工总损失费（元）',
    `is_change_work_type`      int(11) DEFAULT NULL COMMENT '患者是否因患病更换工种',
    `is_suspension`     int(11) DEFAULT NULL COMMENT '患者是否因患病而休学',
    `id_retire`      int(11) DEFAULT NULL COMMENT '患者是否因患病而退休',
    `indirect_cost`      varchar(255) DEFAULT NULL COMMENT '间接成本总费用（元）',
    `eq_5d_5l1`      int(11) DEFAULT NULL COMMENT '行动能力----我四处活动',
    `eq_5d_5l2`      int(11) DEFAULT NULL COMMENT '自我照顾----我自己洗澡或洗衣',
    `eq_5d_5l3`      int(11) DEFAULT NULL COMMENT '日常活动（如工作、学习、家务、家庭或休闲活动）',
    `eq_5d_5l4`      int(11) DEFAULT NULL COMMENT '疼痛或不舒服',
    `eq_5d_5l5`      int(11) DEFAULT NULL COMMENT '焦虑或沮丧',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='卫生经济学';

DROP TABLE IF EXISTS `blood_specimen`;
CREATE TABLE `blood_specimen`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`        bigint(20) NOT NULL  COMMENT 'pID',
    `is_immunosuppressant`        int(11) DEFAULT NULL  COMMENT '抽血前3个月内是否服用免疫抑制剂',
    `immunosuppressant_options`        varchar(255) DEFAULT NULL  COMMENT '免疫抑制剂选项',
    `is_immunotherapy`        int(11) DEFAULT NULL  COMMENT '抽血前1个月是否进行免疫治疗',
    `immunotherapy_options`        varchar(255) DEFAULT NULL  COMMENT '免疫治疗选项',
    `blood_specimen`        int(11) DEFAULT NULL  COMMENT '血液标本',
    `specimen_collection_time`        datetime DEFAULT NULL  COMMENT '标本采集时间',
    `specimen_no`        varchar(255) DEFAULT NULL  COMMENT '标本号',
    `fasting`        int(11) DEFAULT NULL  COMMENT '是否空腹',
    `specimen_collection_category`        int(11) DEFAULT NULL  COMMENT '标本采集类别',
    `purple_anticoagulant_tube`        varchar(255) DEFAULT NULL  COMMENT '紫头抗凝管6ml（管）',
    `red_anticoagulant_tube`         varchar(255) DEFAULT NULL  COMMENT '红头促凝管10ml（管）',
    `specimen_processing_time`        datetime DEFAULT NULL  COMMENT '标本处理时间',
    `serum`        varchar(255) DEFAULT NULL  COMMENT '血清（管）',
    `plasma`        varchar(255) DEFAULT NULL  COMMENT '血浆（管）',
    `leukocyte`        varchar(255) DEFAULT NULL  COMMENT '白细胞（管）',
    `lymphocyte`        varchar(255) DEFAULT NULL  COMMENT '淋巴细胞（管）',
    `dna`        varchar(255) DEFAULT NULL  COMMENT 'DNA（管）',
    `result`        varchar(255) DEFAULT NULL  COMMENT '结果',
    `sample_storage_time`        datetime DEFAULT NULL  COMMENT '样本存储时间',
    `sample_type`        varchar(255) DEFAULT NULL  COMMENT '样本种类',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='血液标本';

DROP TABLE IF EXISTS `sample`;
CREATE TABLE `sample`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`        bigint(20) NOT NULL  COMMENT 'pID',
    `sample_name`        datetime DEFAULT NULL  COMMENT '样本名称',
    `sample_num`        varchar(255) DEFAULT NULL  COMMENT '样本数量',
    `sample_refrigerator`        varchar(255) DEFAULT NULL  COMMENT '样本存放零下80℃冰箱（号）',
    `sample_storage_layer`        varchar(255) DEFAULT NULL  COMMENT '样本存放层',
    `sample_storage_rack`        varchar(255) DEFAULT NULL  COMMENT '样本存放架',
    `sample_storage_sublayer`        varchar(255) DEFAULT NULL  COMMENT '样本存放亚层',
    `sample_storage_box`        varchar(255) DEFAULT NULL  COMMENT '样本存放盒',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='样本';



DROP TABLE IF EXISTS `thymus_specimen`;
CREATE TABLE `thymus_specimen`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`        bigint(20) NOT NULL  COMMENT 'pID',
    `specimen_collection_time`        datetime DEFAULT NULL  COMMENT '标本采集时间',
    `thymus_superior`        varchar(255) DEFAULT NULL  COMMENT '胸腺上级（管）',
    `thymus_middle`        varchar(255) DEFAULT NULL  COMMENT '胸腺中部（管）',
    `thymus_inferior`        varchar(255) DEFAULT NULL  COMMENT '胸腺下级（管）',
    `tumor_tissue`        varchar(255) DEFAULT NULL  COMMENT '瘤体组织（管）',
    `storage_location`        varchar(255) DEFAULT NULL  COMMENT '储存位置',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='胸腺标本';


DROP TABLE IF EXISTS `thymus_operation_record`;
CREATE TABLE `thymus_operation_record`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`        bigint(20) NOT NULL  COMMENT 'pID',
    `thymic_surgery_history`        int(11) DEFAULT NULL  COMMENT '胸腺手术史',
    `thymic_surgery_time`       datetime DEFAULT NULL  COMMENT '胸腺手术时间',
    `time_interval`        varchar(255) DEFAULT NULL  COMMENT '与首发症状的时间间隔',
    `myasthenia_crisis`        int(11) DEFAULT NULL  COMMENT '术前肌无力危象',
    `mgfa`        int(11) DEFAULT NULL  COMMENT '术前MGFA分型（术前1周）',
    `ball_muscle_involvement`        int(11) DEFAULT NULL  COMMENT '有无球部肌肉受累',
    `preoperative_medication`       int(11) DEFAULT NULL  COMMENT '术前用药（1月内）',
    `c_ball_impact`        int(11) DEFAULT NULL  COMMENT '术前丙球冲击（1月内）',
    `hormonal_shock`        int(11) DEFAULT NULL  COMMENT '术前激素冲击（1月内）',
    `plasma_exchange`        int(11) DEFAULT NULL  COMMENT '术前血浆置换（1月内）',
    `pulmonary_function`        int(11) DEFAULT NULL  COMMENT '术前肺功',
    `operation_mode`        int(11) DEFAULT NULL  COMMENT '手术方式',
    `resection_range`        int(11) DEFAULT NULL  COMMENT '切除范围',
    `operation_duration`        varchar(255) DEFAULT NULL  COMMENT '手术时长',
    `intraoperative_blood_loss`        varchar(255) DEFAULT NULL  COMMENT '术中失血量（ml）',
    `myasthenia_crisis_after`        int(11) DEFAULT NULL  COMMENT '术后肌无力危象',
    `complication`        int(11) DEFAULT NULL  COMMENT '术后并发症',
    `pathology`        int(11) DEFAULT NULL  COMMENT '术后病理',
    `picture`        varchar(255) DEFAULT NULL  COMMENT '图片',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='胸腺手术记录';

DROP TABLE IF EXISTS `adjust_medication`;
CREATE TABLE `adjust_medication`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `adjust_medication`                varchar(255)  DEFAULT NULL COMMENT '调整用药',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='调整用药';

DROP TABLE IF EXISTS `drug_details2`;
CREATE TABLE `drug_details2`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `d_id`                bigint(20)  DEFAULT NULL COMMENT '药编号',
    `start_date`                  datetime DEFAULT NULL COMMENT '开始用药时间',
    `end_date`         datetime DEFAULT NULL COMMENT '结束用药时间',
    `days`                varchar(255)  DEFAULT NULL COMMENT '用药天数',
    `use_method`                varchar(255)  DEFAULT NULL COMMENT '用法',
    `onset_date`                datetime  DEFAULT NULL COMMENT '起效时间',
    `side_effect`                varchar(255)  DEFAULT NULL COMMENT '副作用',
    `stop_state`                varchar(255)  DEFAULT NULL COMMENT '是否停药',
    `reason`                varchar(255)  DEFAULT NULL COMMENT '停药原因',
    `aggravate_state`                varchar(255)  DEFAULT NULL COMMENT '是否-过性加重',
    `treatment_details`                varchar(255)  DEFAULT NULL COMMENT '治疗情况',
    `gene`                varchar(255)  DEFAULT NULL COMMENT '基因',
    `suppression_position`                varchar(255)  DEFAULT NULL COMMENT '抑制位置',
    `genotype`                varchar(255)  DEFAULT NULL COMMENT '基因型',
    `take`                varchar(255)  DEFAULT NULL COMMENT '每次服用（粒）',
    `dose`                varchar(255)  DEFAULT NULL COMMENT '每次剂量（mg）',
    `dose_today`                varchar(255)  DEFAULT NULL COMMENT '当日服药剂量（mg）',
    `dose_total`                varchar(255)  DEFAULT NULL COMMENT '目前总服药剂量',
    `improve`                varchar(255)  DEFAULT NULL COMMENT '服药后症状改善情况/使用效果',
    `abnormal_describe`                varchar(255)  DEFAULT NULL COMMENT '异常指标描述',
    `side_effect_abnormal_index`                varchar(255)  DEFAULT NULL COMMENT '副作用异常指标',
    `ta_check`                varchar(255)  DEFAULT NULL COMMENT '他克莫司相关检查',
    `ta_treatment`                varchar(255)  DEFAULT NULL COMMENT '他克莫司治疗情况',
    `ta_concentration`                varchar(255)  DEFAULT NULL COMMENT '他克莫司（ng/ml）浓度',
    `ta_detection_date`                varchar(255)  DEFAULT NULL COMMENT '检测日期',
    `ta_take_wu`                varchar(255)  DEFAULT NULL COMMENT '服用五酯滴丸',
    `ta_take_wu_dose`                varchar(255)  DEFAULT NULL COMMENT '日用药剂量（mg/kg）五酯滴丸',
    `ta_take_wu_effective_time`                varchar(255)  DEFAULT NULL COMMENT '起效时间',
    `ta_take_wu_cd`                varchar(255)  DEFAULT NULL COMMENT 'C/D值五酯滴丸',
    `examination_case`                varchar(255)  DEFAULT NULL COMMENT '检查病例',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用药治疗记录';
alter table drug_details add UNIQUE index idx_drug_detailsid (p_id,d_id);

DROP TABLE IF EXISTS `surgical_records`;
CREATE TABLE `surgical_records`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `thymic_surgery_history`                varchar(255)  DEFAULT NULL COMMENT '胸腺手术史',
    `thymic_surgery_time`                varchar(255)  DEFAULT NULL COMMENT '胸腺手术时间',
    `time_interval`                varchar(255)  DEFAULT NULL COMMENT '与首发症状的时间间隔',
    `crisis`                varchar(255)  DEFAULT NULL COMMENT '术前肌无力危象',
    `crisis_number`                varchar(255)  DEFAULT NULL COMMENT '危象次数',
    `mgfa`                varchar(255)  DEFAULT NULL COMMENT '术前MGFA分型（术前1周）',
    `ball_muscle_involvement`                varchar(255)  DEFAULT NULL COMMENT '有无球部肌肉受累',
    `preoperative_medication`                varchar(255)  DEFAULT NULL COMMENT '术前用药（1月内）',
    `medication_type`                varchar(255)  DEFAULT NULL COMMENT '用药种类',
    `tomorrow_dose`                varchar(255)  DEFAULT NULL COMMENT '溴吡斯的明日剂量',
    `preoperative_use_time`                varchar(255)  DEFAULT NULL COMMENT '溴吡斯的明术前使用时间',
    `immunosuppressant`                varchar(255)  DEFAULT NULL COMMENT '免疫抑制剂',
    `cball_impact`                varchar(255)  DEFAULT NULL COMMENT '术前丙球冲击（1月内）',
    `hormonal_shock`                varchar(255)  DEFAULT NULL COMMENT '术前激素冲击（1月内）',
    `plasma_exchange`                varchar(255)  DEFAULT NULL COMMENT '术前血浆置换（1月内）',
    `pulmonary_function`                varchar(255)  DEFAULT NULL COMMENT '术前肺功',
    `operation_mode`                varchar(255)  DEFAULT NULL COMMENT '手术方式',
    `resection`                varchar(255)  DEFAULT NULL COMMENT '剑突下肋缘三孔手术切除',
    `resection_range`                varchar(255)  DEFAULT NULL COMMENT '切除范围',
    `operation_duration`                varchar(255)  DEFAULT NULL COMMENT '手术时长',
    `blood_loss`                varchar(255)  DEFAULT NULL COMMENT '术中失血量（ml）',
    `after_crisis`                varchar(255)  DEFAULT NULL COMMENT '术后肌无力危象',
    `complication`                varchar(255)  DEFAULT NULL COMMENT '术后并发症',
    `pathology`                varchar(255)  DEFAULT NULL COMMENT '术后病理',
    `picture`                varchar(255)  DEFAULT NULL COMMENT '图片',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='手术记录';

DROP TABLE IF EXISTS `immunosuppressant2`;
CREATE TABLE `immunosuppressant2`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `d_name`                varchar(255)  DEFAULT NULL COMMENT '抑制剂名字',
    `dose`                varchar(255)  DEFAULT NULL COMMENT '剂量',
    `duration`                varchar(255)  DEFAULT NULL COMMENT '术前使用时长',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='免疫抑制剂使用情况';

DROP  TABLE IF EXISTS  `inspection_diagnosis`;
CREATE TABLE `inspection_diagnosis`
(
    `id`        bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `p_id`                   bigint(20) NOT NULL COMMENT 'pID',
    `name`                varchar(255)  DEFAULT NULL COMMENT '术后肌无力危象',
    `name2`                varchar(255)  DEFAULT NULL COMMENT '术后肌无力危象',
    `name3`                varchar(255)  DEFAULT NULL COMMENT '术后肌无力危象',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='检查与诊断';