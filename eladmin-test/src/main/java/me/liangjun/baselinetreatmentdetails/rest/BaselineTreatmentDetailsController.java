/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatmentdetails.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.baselinetreatmentdetails.domain.BaselineTreatmentDetails;
import me.liangjun.baselinetreatmentdetails.service.BaselineTreatmentDetailsService;
import me.liangjun.baselinetreatmentdetails.service.dto.BaselineTreatmentDetailsQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-04
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "baselinetreatmentdetails管理")
@RequestMapping("/api/baselineTreatmentDetails")
public class BaselineTreatmentDetailsController {

    private final BaselineTreatmentDetailsService baselineTreatmentDetailsService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('baselineTreatmentDetails:list')")
    public void download(HttpServletResponse response, BaselineTreatmentDetailsQueryCriteria criteria) throws IOException {
        baselineTreatmentDetailsService.download(baselineTreatmentDetailsService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询baselinetreatmentdetails")
    @ApiOperation("查询baselinetreatmentdetails")
    @PreAuthorize("@el.check('baselineTreatmentDetails:list')")
    public ResponseEntity<Object> query(BaselineTreatmentDetailsQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(baselineTreatmentDetailsService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增baselinetreatmentdetails")
    @ApiOperation("新增baselinetreatmentdetails")
    @PreAuthorize("@el.check('baselineTreatmentDetails:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody BaselineTreatmentDetails resources){
        return new ResponseEntity<>(baselineTreatmentDetailsService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改baselinetreatmentdetails")
    @ApiOperation("修改baselinetreatmentdetails")
    @PreAuthorize("@el.check('baselineTreatmentDetails:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody BaselineTreatmentDetails resources){
        baselineTreatmentDetailsService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除baselinetreatmentdetails")
    @ApiOperation("删除baselinetreatmentdetails")
    @PreAuthorize("@el.check('baselineTreatmentDetails:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        baselineTreatmentDetailsService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}