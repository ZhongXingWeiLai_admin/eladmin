/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatmentdetails.service.impl;

import me.liangjun.baselinetreatmentdetails.domain.BaselineTreatmentDetails;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.baselinetreatmentdetails.repository.BaselineTreatmentDetailsRepository;
import me.liangjun.baselinetreatmentdetails.service.BaselineTreatmentDetailsService;
import me.liangjun.baselinetreatmentdetails.service.dto.BaselineTreatmentDetailsDto;
import me.liangjun.baselinetreatmentdetails.service.dto.BaselineTreatmentDetailsQueryCriteria;
import me.liangjun.baselinetreatmentdetails.service.mapstruct.BaselineTreatmentDetailsMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-04
**/
@Service
@RequiredArgsConstructor
public class BaselineTreatmentDetailsServiceImpl implements BaselineTreatmentDetailsService {

    private final BaselineTreatmentDetailsRepository baselineTreatmentDetailsRepository;
    private final BaselineTreatmentDetailsMapper baselineTreatmentDetailsMapper;

    @Override
    public Map<String,Object> queryAll(BaselineTreatmentDetailsQueryCriteria criteria, Pageable pageable){
        Page<BaselineTreatmentDetails> page = baselineTreatmentDetailsRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(baselineTreatmentDetailsMapper::toDto));
    }

    @Override
    public List<BaselineTreatmentDetailsDto> queryAll(BaselineTreatmentDetailsQueryCriteria criteria){
        return baselineTreatmentDetailsMapper.toDto(baselineTreatmentDetailsRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public BaselineTreatmentDetailsDto findById(Long id) {
        BaselineTreatmentDetails baselineTreatmentDetails = baselineTreatmentDetailsRepository.findById(id).orElseGet(BaselineTreatmentDetails::new);
        ValidationUtil.isNull(baselineTreatmentDetails.getId(),"BaselineTreatmentDetails","id",id);
        return baselineTreatmentDetailsMapper.toDto(baselineTreatmentDetails);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaselineTreatmentDetailsDto create(BaselineTreatmentDetails resources) {
        return baselineTreatmentDetailsMapper.toDto(baselineTreatmentDetailsRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(BaselineTreatmentDetails resources) {
        BaselineTreatmentDetails baselineTreatmentDetails = baselineTreatmentDetailsRepository.findById(resources.getId()).orElseGet(BaselineTreatmentDetails::new);
        ValidationUtil.isNull( baselineTreatmentDetails.getId(),"BaselineTreatmentDetails","id",resources.getId());
        baselineTreatmentDetails.copy(resources);
        baselineTreatmentDetailsRepository.save(baselineTreatmentDetails);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            baselineTreatmentDetailsRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<BaselineTreatmentDetailsDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (BaselineTreatmentDetailsDto baselineTreatmentDetails : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", baselineTreatmentDetails.getPId());
            map.put("药物ID", baselineTreatmentDetails.getDId());
            map.put("入组当日用法（次/日）", baselineTreatmentDetails.getDailyUsageFirst());
            map.put("入组当日每次服用", baselineTreatmentDetails.getTakeNumFirst());
            map.put("每次剂量（mg）", baselineTreatmentDetails.getDoseFirst());
            map.put("当日服药剂量（mg）", baselineTreatmentDetails.getDoseCountFirst());
            map.put("起始用药时间", baselineTreatmentDetails.getStartingMedicationTime());
            map.put("用法（次/日）", baselineTreatmentDetails.getDailyUsage());
            map.put("每次服用（粒）", baselineTreatmentDetails.getTakeNum());
            map.put("每次剂量（mg）", baselineTreatmentDetails.getDose());
            map.put("当日服药剂量（mg）", baselineTreatmentDetails.getDoseCount());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public List<BaselineTreatmentDetailsDto> findByPid(Long pid) {
        return baselineTreatmentDetailsMapper.toDto(baselineTreatmentDetailsRepository.findByPid(pid));
    }
}