/*
 *  Copyright 2019-2020 Zheng Jie
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package me.liangjun.baselinetreatmentdetails.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;

/**
 * @website https://el-admin.vip
 * @description /
 * @author wei
 * @date 2021-09-04
 **/
@Data
public class BaselineTreatmentDetailsDto implements Serializable {

    /** ID */
    private Long id;

    /** pID */
    private Long pId;

    /** 药物ID */
    private Long dId;

    /** 入组当日用法（次/日） */
    private String dailyUsageFirst;

    /** 入组当日每次服用 */
    private String takeNumFirst;

    /** 每次剂量（mg） */
    private String doseFirst;

    /** 当日服药剂量（mg） */
    private String doseCountFirst;

    /** 起始用药时间 */
    private Timestamp startingMedicationTime;

    private Timestamp endMedicationTime;

    /** 用法（次/日） */
    private String dailyUsage;

    /** 每次服用（粒） */
    private String takeNum;

    /** 每次剂量（mg） */
    private String dose;

    /** 当日服药剂量（mg） */
    private String doseCount;
}