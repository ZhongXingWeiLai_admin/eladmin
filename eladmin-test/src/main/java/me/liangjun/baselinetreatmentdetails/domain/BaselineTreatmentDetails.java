/*
 *  Copyright 2019-2020 Zheng Jie
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package me.liangjun.baselinetreatmentdetails.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
 * @website https://el-admin.vip
 * @description /
 * @author wei
 * @date 2021-09-04
 **/
@Entity
@Data
@Table(name="baseline_treatment_details")
public class BaselineTreatmentDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "d_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "药物ID")
    private Long dId;

    @Column(name = "daily_usage_first")
    @ApiModelProperty(value = "入组当日用法（次/日）")
    private String dailyUsageFirst;

    @Column(name = "take_num_first")
    @ApiModelProperty(value = "入组当日每次服用")
    private String takeNumFirst;

    @Column(name = "dose_first")
    @ApiModelProperty(value = "每次剂量（mg）")
    private String doseFirst;

    @Column(name = "dose_count_first")
    @ApiModelProperty(value = "当日服药剂量（mg）")
    private String doseCountFirst;

    @Column(name = "starting_medication_time")
    @ApiModelProperty(value = "起始用药时间")
    private Timestamp startingMedicationTime;

    @Column(name = "end_medication_time")
    @ApiModelProperty(value = "结束用药时间")
    private Timestamp endMedicationTime;

    @Column(name = "daily_usage")
    @ApiModelProperty(value = "用法（次/日）")
    private String dailyUsage;

    @Column(name = "take_num")
    @ApiModelProperty(value = "每次服用（粒）")
    private String takeNum;

    @Column(name = "dose")
    @ApiModelProperty(value = "每次剂量（mg）")
    private String dose;

    @Column(name = "dose_count")
    @ApiModelProperty(value = "当日服药剂量（mg）")
    private String doseCount;

    public void copy(BaselineTreatmentDetails source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}