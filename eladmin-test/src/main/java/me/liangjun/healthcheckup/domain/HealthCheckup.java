/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.healthcheckup.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-04
**/
@Entity
@Data
@Table(name="health_checkup")
public class HealthCheckup implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "height")
    @ApiModelProperty(value = "身高")
    private String height;

    @Column(name = "weight")
    @ApiModelProperty(value = "体重")
    private String weight;

    @Column(name = "bmi")
    @ApiModelProperty(value = "BMI")
    private String bmi;

    @Column(name = "blepharoptosis")
    @ApiModelProperty(value = "眼睑下垂")
    private Integer blepharoptosis;

    @Column(name = "eye_movement_disorder")
    @ApiModelProperty(value = "眼动障碍")
    private Integer eyeMovementDisorder;

    @Column(name = "diplopia")
    @ApiModelProperty(value = "复视")
    private Integer diplopia;

    @Column(name = "sorenes")
    @ApiModelProperty(value = "闭目")
    private Integer sorenes;

    @Column(name = "mumps")
    @ApiModelProperty(value = "鼓腮")
    private Integer mumps;

    @Column(name = "dysarthria")
    @ApiModelProperty(value = "构音障碍:")
    private Integer dysarthria;

    @Column(name = "drink")
    @ApiModelProperty(value = "屈颈:")
    private Integer drink;

    @Column(name = "neck_flexion")
    @ApiModelProperty(value = "伸颈:")
    private Integer neckFlexion;

    @Column(name = "toe_walking")
    @ApiModelProperty(value = "脚尖走路:")
    private Integer toeWalking;

    @Column(name = "heel_walking")
    @ApiModelProperty(value = "脚跟走路")
    private Integer heelWalking;

    @Column(name = "squat")
    @ApiModelProperty(value = "蹲起")
    private Integer squat;

    @Column(name = "muscular_strength1")
    @ApiModelProperty(value = "左上肢近端肌力:")
    private Integer muscularStrength1;

    @Column(name = "muscular_strength2")
    @ApiModelProperty(value = "左上肢远端肌力")
    private Integer muscularStrength2;

    @Column(name = "muscular_strength3")
    @ApiModelProperty(value = "右上肢近端肌力")
    private Integer muscularStrength3;

    @Column(name = "muscular_strength4")
    @ApiModelProperty(value = "右上肢远端肌力")
    private Integer muscularStrength4;

    @Column(name = "muscular_strength5")
    @ApiModelProperty(value = "左下肢近端肌力")
    private Integer muscularStrength5;

    @Column(name = "muscular_strength6")
    @ApiModelProperty(value = "左下肢远端肌力")
    private Integer muscularStrength6;

    @Column(name = "muscular_strength7")
    @ApiModelProperty(value = "右下肢近端肌力")
    private Integer muscularStrength7;

    @Column(name = "muscular_strength8")
    @ApiModelProperty(value = "右下肢远端肌力")
    private Integer muscularStrength8;

    @Column(name = "eye")
    @ApiModelProperty(value = "是否眼肌型量表")
    private Integer eye;

    @Column(name = "osserman")
    @ApiModelProperty(value = "Osserman分型")
    private Integer osserman;

    @Column(name = "mgfa")
    @ApiModelProperty(value = "MGFA")
    private Integer mgfa;

    public void copy(HealthCheckup source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}