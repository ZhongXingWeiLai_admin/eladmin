/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.healthcheckup.service.impl;

import me.liangjun.eyemovements.domain.EyeMovementsScore;
import me.liangjun.eyemovements.repository.EyeMovementsScoreRepository;
import me.liangjun.eyemovements.service.mapstruct.EyeMovementsScoreMapper;
import me.liangjun.healthcheckup.domain.HealthCheckup;
import me.liangjun.medicalhistorybefore.service.dto.BaseLineCaseDto;
import me.liangjun.mg.domain.MgScore;
import me.liangjun.mg.repository.MgScoreRepository;
import me.liangjun.mg.service.mapstruct.MgScoreMapper;
import me.liangjun.mgadl.domain.MgAdlScore;
import me.liangjun.mgadl.repository.MgAdlScoreRepository;
import me.liangjun.mgadl.service.mapstruct.MgAdlScoreMapper;
import me.liangjun.mgqol.domain.MgQolScore;
import me.liangjun.mgqol.repository.MgQolScoreRepository;
import me.liangjun.mgqol.service.mapstruct.MgQolScoreMapper;
import me.liangjun.ocularqmg.domain.OcularQmgScore;
import me.liangjun.ocularqmg.repository.OcularQmgScoreRepository;
import me.liangjun.ocularqmg.service.mapstruct.OcularQmgScoreMapper;
import me.liangjun.qmg.domain.QmgScore;
import me.liangjun.qmg.repository.QmgScoreRepository;
import me.liangjun.qmg.service.mapstruct.QmgScoreMapper;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.healthcheckup.repository.HealthCheckupRepository;
import me.liangjun.healthcheckup.service.HealthCheckupService;
import me.liangjun.healthcheckup.service.dto.HealthCheckupDto;
import me.liangjun.healthcheckup.service.dto.HealthCheckupQueryCriteria;
import me.liangjun.healthcheckup.service.mapstruct.HealthCheckupMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-04
**/
@Service
@RequiredArgsConstructor
public class HealthCheckupServiceImpl implements HealthCheckupService {

    private final HealthCheckupRepository healthCheckupRepository;
    private final HealthCheckupMapper healthCheckupMapper;
    private final EyeMovementsScoreRepository eyeMovementsScoreRepository;
    private final EyeMovementsScoreMapper eyeMovementsScoreMapper;
    private final OcularQmgScoreRepository ocularQmgScoreRepository;
    private final OcularQmgScoreMapper ocularQmgScoreMapper;
    private final MgAdlScoreRepository mgAdlScoreRepository;
    private final MgAdlScoreMapper mgAdlScoreMapper;
    private final QmgScoreRepository qmgScoreRepository;
    private final QmgScoreMapper qmgScoreMapper;
    private final MgScoreRepository mgScoreRepository;
    private final MgScoreMapper mgScoreMapper;
    private final MgQolScoreRepository mgQolScoreRepository;
    private final MgQolScoreMapper mgQolScoreMapper;

    @Override
    public Map<String,Object> queryAll(HealthCheckupQueryCriteria criteria, Pageable pageable){
        Page<HealthCheckup> page = healthCheckupRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(healthCheckupMapper::toDto));
    }

    @Override
    public List<HealthCheckupDto> queryAll(HealthCheckupQueryCriteria criteria){
        return healthCheckupMapper.toDto(healthCheckupRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public HealthCheckupDto findById(Long id) {
        HealthCheckup healthCheckup = healthCheckupRepository.findById(id).orElseGet(HealthCheckup::new);
        ValidationUtil.isNull(healthCheckup.getId(),"HealthCheckup","id",id);
        return healthCheckupMapper.toDto(healthCheckup);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseLineCaseDto create(BaseLineCaseDto resources) {
        Long pId = resources.getHealthCheckup().getPId();
        HealthCheckup checkup = healthCheckupRepository.findByPid(pId);
        if (checkup != null) {
            checkup.copy(healthCheckupMapper.toEntity(resources.getHealthCheckup()));
            healthCheckupRepository.save(checkup);
        }else{
            healthCheckupRepository.save(healthCheckupMapper.toEntity(resources.getHealthCheckup()));
        }
        EyeMovementsScore eyeMovementsScore = eyeMovementsScoreRepository.findByPid(pId);
        if(eyeMovementsScore!=null){
            eyeMovementsScore.copy(eyeMovementsScoreMapper.toEntity(resources.getEyeMovementsScore()));
            eyeMovementsScoreRepository.save(eyeMovementsScore);
        }else {
            eyeMovementsScoreRepository.save(eyeMovementsScoreMapper.toEntity(resources.getEyeMovementsScore()));
        }
        OcularQmgScore ocularQmgScore = ocularQmgScoreRepository.findByPid(pId);
        if (ocularQmgScore != null) {
            ocularQmgScore.copy(ocularQmgScoreMapper.toEntity(resources.getOcularQmgScore()));
            ocularQmgScoreRepository.save(ocularQmgScore);
        }else {
            ocularQmgScoreRepository.save(ocularQmgScoreMapper.toEntity(resources.getOcularQmgScore()));
        }
        MgAdlScore adlScore = mgAdlScoreRepository.findByPid(pId);
        if (adlScore != null) {
            adlScore.copy(mgAdlScoreMapper.toEntity(resources.getMgAdlScore()));
            mgAdlScoreRepository.save(adlScore);
        }else {
            mgAdlScoreRepository.save(mgAdlScoreMapper.toEntity(resources.getMgAdlScore()));
        }
        QmgScore qmgScore = qmgScoreRepository.findByPid(pId);
        if (qmgScore != null) {
            qmgScore.copy(qmgScoreMapper.toEntity(resources.getQmgScore()));
            qmgScoreRepository.save(qmgScore);
        }else {
            qmgScoreRepository.save(qmgScoreMapper.toEntity(resources.getQmgScore()));
        }
        MgScore mgScore = mgScoreRepository.findByPid(pId);
        if (mgScore != null) {
            mgScore.copy(mgScoreMapper.toEntity(resources.getMgScore()));
            mgScoreRepository.save(mgScore);
        }else {
            mgScoreRepository.save(mgScoreMapper.toEntity(resources.getMgScore()));
        }
        MgQolScore qolScore = mgQolScoreRepository.findByPid(pId);
        if(qolScore!=null){
            qolScore.copy(mgQolScoreMapper.toEntity(resources.getMgQolScore()));
            mgQolScoreRepository.save(qolScore);
        }else {
            mgQolScoreRepository.save(mgQolScoreMapper.toEntity(resources.getMgQolScore()));
        }
        return resources;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(HealthCheckup resources) {
        HealthCheckup healthCheckup = healthCheckupRepository.findById(resources.getId()).orElseGet(HealthCheckup::new);
        ValidationUtil.isNull( healthCheckup.getId(),"HealthCheckup","id",resources.getId());
        healthCheckup.copy(resources);
        healthCheckupRepository.save(healthCheckup);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            healthCheckupRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<HealthCheckupDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (HealthCheckupDto healthCheckup : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", healthCheckup.getPId());
            map.put("身高", healthCheckup.getHeight());
            map.put("体重", healthCheckup.getWeight());
            map.put("BMI", healthCheckup.getBmi());
            map.put("眼睑下垂", healthCheckup.getBlepharoptosis());
            map.put("眼动障碍", healthCheckup.getEyeMovementDisorder());
            map.put("复视", healthCheckup.getDiplopia());
            map.put("闭目", healthCheckup.getSorenes());
            map.put("鼓腮", healthCheckup.getMumps());
            map.put("构音障碍:", healthCheckup.getDysarthria());
            map.put("屈颈:", healthCheckup.getDrink());
            map.put("伸颈:", healthCheckup.getNeckFlexion());
            map.put("脚尖走路:", healthCheckup.getToeWalking());
            map.put("脚跟走路", healthCheckup.getHeelWalking());
            map.put("蹲起", healthCheckup.getSquat());
            map.put("左上肢近端肌力:", healthCheckup.getMuscularStrength1());
            map.put("左上肢远端肌力", healthCheckup.getMuscularStrength2());
            map.put("右上肢近端肌力", healthCheckup.getMuscularStrength3());
            map.put("右上肢远端肌力", healthCheckup.getMuscularStrength4());
            map.put("左下肢近端肌力", healthCheckup.getMuscularStrength5());
            map.put("左下肢远端肌力", healthCheckup.getMuscularStrength6());
            map.put("右下肢近端肌力", healthCheckup.getMuscularStrength7());
            map.put("右下肢远端肌力", healthCheckup.getMuscularStrength8());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public HealthCheckupDto findByPid(Long pid) {
        return healthCheckupMapper.toDto(healthCheckupRepository.findByPid(pid));
    }
}