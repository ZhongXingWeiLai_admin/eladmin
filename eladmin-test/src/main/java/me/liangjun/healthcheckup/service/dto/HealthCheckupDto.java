/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.healthcheckup.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-04
**/
@Data
public class HealthCheckupDto implements Serializable {

    /** ID */
    private Long id;

    /** pID */
    private Long pId;

    /** 身高 */
    private String height;

    /** 体重 */
    private String weight;

    /** BMI */
    private String bmi;

    /** 眼睑下垂 */
    private Integer blepharoptosis;

    /** 眼动障碍 */
    private Integer eyeMovementDisorder;

    /** 复视 */
    private Integer diplopia;

    /** 闭目 */
    private Integer sorenes;

    /** 鼓腮 */
    private Integer mumps;

    /** 构音障碍: */
    private Integer dysarthria;

    /** 屈颈: */
    private Integer drink;

    /** 伸颈: */
    private Integer neckFlexion;

    /** 脚尖走路: */
    private Integer toeWalking;

    /** 脚跟走路 */
    private Integer heelWalking;

    /** 蹲起 */
    private Integer squat;

    /** 左上肢近端肌力: */
    private Integer muscularStrength1;

    /** 左上肢远端肌力 */
    private Integer muscularStrength2;

    /** 右上肢近端肌力 */
    private Integer muscularStrength3;

    /** 右上肢远端肌力 */
    private Integer muscularStrength4;

    /** 左下肢近端肌力 */
    private Integer muscularStrength5;

    /** 左下肢远端肌力 */
    private Integer muscularStrength6;

    /** 右下肢近端肌力 */
    private Integer muscularStrength7;

    /** 右下肢远端肌力 */
    private Integer muscularStrength8;

    private Integer eye;

    private Integer osserman;

    private Integer mgfa;
}