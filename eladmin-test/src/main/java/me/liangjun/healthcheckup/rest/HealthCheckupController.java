/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.healthcheckup.rest;

import me.liangjun.medicalhistorybefore.service.dto.BaseLineCaseDto;
import me.zhengjie.annotation.Log;
import me.liangjun.healthcheckup.domain.HealthCheckup;
import me.liangjun.healthcheckup.service.HealthCheckupService;
import me.liangjun.healthcheckup.service.dto.HealthCheckupQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-04
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "health_checkup管理")
@RequestMapping("/api/healthCheckup")
public class HealthCheckupController {

    private final HealthCheckupService healthCheckupService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('healthCheckup:list')")
    public void download(HttpServletResponse response, HealthCheckupQueryCriteria criteria) throws IOException {
        healthCheckupService.download(healthCheckupService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询health_checkup")
    @ApiOperation("查询health_checkup")
    @PreAuthorize("@el.check('healthCheckup:list')")
    public ResponseEntity<Object> query(HealthCheckupQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(healthCheckupService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增health_checkup")
    @ApiOperation("新增health_checkup")
    @PreAuthorize("@el.check('healthCheckup:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody BaseLineCaseDto resources){
        return new ResponseEntity<>(healthCheckupService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改health_checkup")
    @ApiOperation("修改health_checkup")
    @PreAuthorize("@el.check('healthCheckup:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody HealthCheckup resources){
        healthCheckupService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除health_checkup")
    @ApiOperation("删除health_checkup")
    @PreAuthorize("@el.check('healthCheckup:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        healthCheckupService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}