/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.inspectionresults.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.inspectionresults.domain.InspectionResults;
import me.liangjun.inspectionresults.service.InspectionResultsService;
import me.liangjun.inspectionresults.service.dto.InspectionResultsQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-04
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "inspectionresults管理")
@RequestMapping("/api/inspectionResults")
public class InspectionResultsController {

    private final InspectionResultsService inspectionResultsService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('inspectionResults:list')")
    public void download(HttpServletResponse response, InspectionResultsQueryCriteria criteria) throws IOException {
        inspectionResultsService.download(inspectionResultsService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询inspectionresults")
    @ApiOperation("查询inspectionresults")
    @PreAuthorize("@el.check('inspectionResults:list')")
    public ResponseEntity<Object> query(InspectionResultsQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(inspectionResultsService.queryAll(criteria,pageable),HttpStatus.OK);
    }
    @GetMapping(value = "findByPid")
    @Log("查询inspectionresults")
    @ApiOperation("查询inspectionresults")
    @PreAuthorize("@el.check('inspectionResults:list')")
    public ResponseEntity<Object> findByPid(@RequestParam("pid") Long pid){
        return new ResponseEntity<>(inspectionResultsService.findByPid(pid),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增inspectionresults")
    @ApiOperation("新增inspectionresults")
    @PreAuthorize("@el.check('inspectionResults:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody InspectionResults resources){
        return new ResponseEntity<>(inspectionResultsService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改inspectionresults")
    @ApiOperation("修改inspectionresults")
    @PreAuthorize("@el.check('inspectionResults:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody InspectionResults resources){
        inspectionResultsService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除inspectionresults")
    @ApiOperation("删除inspectionresults")
    @PreAuthorize("@el.check('inspectionResults:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        inspectionResultsService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}