/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.inspectionresults.service.mapstruct;

import cn.hutool.core.collection.CollUtil;
import me.liangjun.inspectionresults.domain.InspectionResults;
import me.liangjun.inspectionresults.service.dto.InspectionResultsDto;
import me.liangjun.medicalhistorybefore.domain.ImmuneSystemDiseaseFamilyHistory;
import me.liangjun.medicalhistorybefore.domain.MedicalHistoryBefore;
import me.liangjun.medicalhistorybefore.service.dto.MedicalHistoryBeforeDto;
import me.liangjun.medicalhistorybefore.service.dto.PastDiseaseDto;
import me.liangjun.medicalhistorybefore.service.mapstruct.ImmuneSystemDiseaseFamilyHistoryMapper;
import me.liangjun.medicalhistorybefore.service.mapstruct.PastDiseaseMapper;
import me.zhengjie.base.BaseMapper;
import me.liangjun.inspectionresults.domain.InspectionDiagnosis;
import me.liangjun.inspectionresults.service.dto.InspectionDiagnosisDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import java.util.Arrays;
import java.util.List;

/**
* @website https://el-admin.vip
* @author wei
* @date 2022-01-16
**/
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {InspectionResultsMapper.class})
public interface InspectionDiagnosisMapper extends BaseMapper<InspectionDiagnosisDto, InspectionDiagnosis> {

    @Override
    @Mappings({
            @Mapping(source = "name",target = "name"),
            @Mapping(source = "name2",target = "name2"),
            @Mapping(source = "name3",target = "name3"),
    })
    InspectionDiagnosis toEntity(InspectionDiagnosisDto dtoList);

    @Override
    @Mappings({
            @Mapping(source = "name",target = "name"),
            @Mapping(source = "name2",target = "name2"),
            @Mapping(source = "name3",target = "name3"),
    })
    InspectionDiagnosisDto toDto(InspectionDiagnosis entity);

        default List<String> str2List(String src){
        String[] split = src.split(",");
        List<String> result = Arrays.asList(split);
        return result;
    }

    @Mappings({
            @Mapping(source = "nameDto",target = "nameDto"),
            @Mapping(source = "name2Dto",target = "name2Dto"),
            @Mapping(source = "name3Dto",target = "name3Dto"),
            @Mapping(source = "inspectionDiagnosis.name",target = "name"),
            @Mapping(source = "inspectionDiagnosis.name2",target = "name2"),
            @Mapping(source = "inspectionDiagnosis.name3",target = "name3"),
    })
    InspectionDiagnosisDto domain2dto(InspectionDiagnosis inspectionDiagnosis,
                                       List<InspectionResults> nameDto,
                                       List<InspectionResults> name2Dto,
                                       List<InspectionResults> name3Dto);



    // list转str
    default String list2Str(List<String> src){
        if (CollUtil.isEmpty(src)) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        src.stream().forEach(item -> sb.append(item.replace("\"","")).append(","));
        return sb.substring(0, sb.length() - 1).toString();
    }
}