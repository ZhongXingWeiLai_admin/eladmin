/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.inspectionresults.service.impl;

import me.liangjun.inspectionresults.domain.InspectionResults;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.inspectionresults.repository.InspectionResultsRepository;
import me.liangjun.inspectionresults.service.InspectionResultsService;
import me.liangjun.inspectionresults.service.dto.InspectionResultsDto;
import me.liangjun.inspectionresults.service.dto.InspectionResultsQueryCriteria;
import me.liangjun.inspectionresults.service.mapstruct.InspectionResultsMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-04
**/
@Service
@RequiredArgsConstructor
public class InspectionResultsServiceImpl implements InspectionResultsService {

    private final InspectionResultsRepository inspectionResultsRepository;
    private final InspectionResultsMapper inspectionResultsMapper;

    @Override
    public Map<String,Object> queryAll(InspectionResultsQueryCriteria criteria, Pageable pageable){
        Page<InspectionResults> page = inspectionResultsRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(inspectionResultsMapper::toDto));
    }

    @Override
    public List<InspectionResultsDto> queryAll(InspectionResultsQueryCriteria criteria){
        return inspectionResultsMapper.toDto(inspectionResultsRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public InspectionResultsDto findById(Long id) {
        InspectionResults inspectionResults = inspectionResultsRepository.findById(id).orElseGet(InspectionResults::new);
        ValidationUtil.isNull(inspectionResults.getId(),"InspectionResults","id",id);
        return inspectionResultsMapper.toDto(inspectionResults);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public InspectionResultsDto create(InspectionResults resources) {
        return inspectionResultsMapper.toDto(inspectionResultsRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(InspectionResults resources) {
        InspectionResults inspectionResults = inspectionResultsRepository.findById(resources.getId()).orElseGet(InspectionResults::new);
        ValidationUtil.isNull( inspectionResults.getId(),"InspectionResults","id",resources.getId());
        inspectionResults.copy(resources);
        inspectionResultsRepository.save(inspectionResults);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            inspectionResultsRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<InspectionResultsDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (InspectionResultsDto inspectionResults : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", inspectionResults.getPId());
            map.put("检查类型", inspectionResults.getType());
            map.put("检查结果（多结果，分割）", inspectionResults.getResult());
            map.put("图片", inspectionResults.getPicture());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public List<InspectionResultsDto> findByPid(Long pid) {
        List<InspectionResultsDto> dtoList = inspectionResultsMapper.toDto(inspectionResultsRepository.findByPid(pid));
        return dtoList;
    }
}