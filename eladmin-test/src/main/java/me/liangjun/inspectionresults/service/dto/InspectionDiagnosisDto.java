/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.inspectionresults.service.dto;

import lombok.Data;
import java.io.Serializable;
import java.util.List;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2022-01-16
**/
@Data
public class InspectionDiagnosisDto implements Serializable {

    /** ID */
    private Long id;

    /** pID */
    private Long pId;

    /** 诊断相关检查 */
    private List<String> name;
    private List<InspectionResultsDto> nameDto;

    /** 治疗相关检查 */
    private List<String> name2;
    private List<InspectionResultsDto> name2Dto;

    /** 其他检查 */
    private List<String> name3;
    private List<InspectionResultsDto> name3Dto;
}