/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.antibodydetection.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.antibodydetection.domain.AntibodyDetection;
import me.liangjun.antibodydetection.service.AntibodyDetectionService;
import me.liangjun.antibodydetection.service.dto.AntibodyDetectionQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-04
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "antibodydetection管理")
@RequestMapping("/api/antibodyDetection")
public class AntibodyDetectionController {

    private final AntibodyDetectionService antibodyDetectionService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('antibodyDetection:list')")
    public void download(HttpServletResponse response, AntibodyDetectionQueryCriteria criteria) throws IOException {
        antibodyDetectionService.download(antibodyDetectionService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询antibodydetection")
    @ApiOperation("查询antibodydetection")
    @PreAuthorize("@el.check('antibodyDetection:list')")
    public ResponseEntity<Object> query(AntibodyDetectionQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(antibodyDetectionService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增antibodydetection")
    @ApiOperation("新增antibodydetection")
    @PreAuthorize("@el.check('antibodyDetection:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody AntibodyDetection resources){
        return new ResponseEntity<>(antibodyDetectionService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改antibodydetection")
    @ApiOperation("修改antibodydetection")
    @PreAuthorize("@el.check('antibodyDetection:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody AntibodyDetection resources){
        antibodyDetectionService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除antibodydetection")
    @ApiOperation("删除antibodydetection")
    @PreAuthorize("@el.check('antibodyDetection:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        antibodyDetectionService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}