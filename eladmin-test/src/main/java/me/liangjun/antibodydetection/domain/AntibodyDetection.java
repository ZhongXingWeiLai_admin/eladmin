/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.antibodydetection.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-04
**/
@Entity
@Data
@Table(name="antibody_detection")
public class AntibodyDetection implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "date")
    @ApiModelProperty(value = "检测时间")
    private Integer date;

    @Column(name = "mechanism")
    @ApiModelProperty(value = "检测机构")
    private String mechanism;

    @Column(name = "source")
    @ApiModelProperty(value = "数据来源")
    private String source;

    @Column(name = "picture")
    @ApiModelProperty(value = "图片")
    private String picture;

    public void copy(AntibodyDetection source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}