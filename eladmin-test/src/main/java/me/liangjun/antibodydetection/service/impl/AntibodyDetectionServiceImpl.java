/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.antibodydetection.service.impl;

import me.liangjun.antibodydetection.domain.AntibodyDetection;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.antibodydetection.repository.AntibodyDetectionRepository;
import me.liangjun.antibodydetection.service.AntibodyDetectionService;
import me.liangjun.antibodydetection.service.dto.AntibodyDetectionDto;
import me.liangjun.antibodydetection.service.dto.AntibodyDetectionQueryCriteria;
import me.liangjun.antibodydetection.service.mapstruct.AntibodyDetectionMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-04
**/
@Service
@RequiredArgsConstructor
public class AntibodyDetectionServiceImpl implements AntibodyDetectionService {

    private final AntibodyDetectionRepository antibodyDetectionRepository;
    private final AntibodyDetectionMapper antibodyDetectionMapper;

    @Override
    public Map<String,Object> queryAll(AntibodyDetectionQueryCriteria criteria, Pageable pageable){
        Page<AntibodyDetection> page = antibodyDetectionRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(antibodyDetectionMapper::toDto));
    }

    @Override
    public List<AntibodyDetectionDto> queryAll(AntibodyDetectionQueryCriteria criteria){
        return antibodyDetectionMapper.toDto(antibodyDetectionRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public AntibodyDetectionDto findById(Long id) {
        AntibodyDetection antibodyDetection = antibodyDetectionRepository.findById(id).orElseGet(AntibodyDetection::new);
        ValidationUtil.isNull(antibodyDetection.getId(),"AntibodyDetection","id",id);
        return antibodyDetectionMapper.toDto(antibodyDetection);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public AntibodyDetectionDto create(AntibodyDetection resources) {
        return antibodyDetectionMapper.toDto(antibodyDetectionRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(AntibodyDetection resources) {
        AntibodyDetection antibodyDetection = antibodyDetectionRepository.findById(resources.getId()).orElseGet(AntibodyDetection::new);
        ValidationUtil.isNull( antibodyDetection.getId(),"AntibodyDetection","id",resources.getId());
        antibodyDetection.copy(resources);
        antibodyDetectionRepository.save(antibodyDetection);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            antibodyDetectionRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<AntibodyDetectionDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (AntibodyDetectionDto antibodyDetection : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", antibodyDetection.getPId());
            map.put("检测时间", antibodyDetection.getDate());
            map.put("检测机构", antibodyDetection.getMechanism());
            map.put("数据来源", antibodyDetection.getSource());
            map.put("图片", antibodyDetection.getPicture());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}