/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.medicalhistorybefore.rest;

import me.liangjun.medicalhistorybefore.service.dto.MedicalHistoryBeforeDto;
import me.zhengjie.annotation.AnonymousAccess;
import me.zhengjie.annotation.Log;
import me.liangjun.medicalhistorybefore.domain.MedicalHistoryBefore;
import me.liangjun.medicalhistorybefore.service.MedicalHistoryBeforeService;
import me.liangjun.medicalhistorybefore.service.dto.MedicalHistoryBeforeQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-04
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "medicalhistorybefore管理")
@RequestMapping("/api/medicalHistoryBefore")
public class MedicalHistoryBeforeController {

    private final MedicalHistoryBeforeService medicalHistoryBeforeService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('medicalHistoryBefore:list')")
    public void download(HttpServletResponse response, MedicalHistoryBeforeQueryCriteria criteria) throws IOException {
        medicalHistoryBeforeService.download(medicalHistoryBeforeService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询medicalhistorybefore")
    @ApiOperation("查询medicalhistorybefore")
    @PreAuthorize("@el.check('medicalHistoryBefore:list')")
    public ResponseEntity<Object> query(MedicalHistoryBeforeQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(medicalHistoryBeforeService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @GetMapping(value = "findByPid")
    @Log("查询medicalhistorybefore")
    @ApiOperation("查询medicalhistorybefore")
//    @PreAuthorize("@el.check('medicalHistoryBefore:list')")
    @AnonymousAccess
    public ResponseEntity<Object> findByPid(@RequestParam("pid") Long pid){
        return new ResponseEntity<>(medicalHistoryBeforeService.findByPid(pid),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增medicalhistorybefore")
    @ApiOperation("新增medicalhistorybefore")
    @PreAuthorize("@el.check('medicalHistoryBefore:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody MedicalHistoryBeforeDto resources){
        return new ResponseEntity<>(medicalHistoryBeforeService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改medicalhistorybefore")
    @ApiOperation("修改medicalhistorybefore")
    @PreAuthorize("@el.check('medicalHistoryBefore:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody MedicalHistoryBeforeDto resources){
        medicalHistoryBeforeService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除medicalhistorybefore")
    @ApiOperation("删除medicalhistorybefore")
    @PreAuthorize("@el.check('medicalHistoryBefore:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        medicalHistoryBeforeService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}