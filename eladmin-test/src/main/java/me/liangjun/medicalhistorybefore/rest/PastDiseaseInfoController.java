/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.medicalhistorybefore.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.medicalhistorybefore.domain.PastDiseaseInfo;
import me.liangjun.medicalhistorybefore.service.PastDiseaseInfoService;
import me.liangjun.medicalhistorybefore.service.dto.PastDiseaseInfoQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-12
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "disease管理")
@RequestMapping("/api/pastDiseaseInfo")
public class PastDiseaseInfoController {

    private final PastDiseaseInfoService pastDiseaseInfoService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('pastDiseaseInfo:list')")
    public void download(HttpServletResponse response, PastDiseaseInfoQueryCriteria criteria) throws IOException {
        pastDiseaseInfoService.download(pastDiseaseInfoService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询disease")
    @ApiOperation("查询disease")
    @PreAuthorize("@el.check('pastDiseaseInfo:list')")
    public ResponseEntity<Object> query(PastDiseaseInfoQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(pastDiseaseInfoService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增disease")
    @ApiOperation("新增disease")
    @PreAuthorize("@el.check('pastDiseaseInfo:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody PastDiseaseInfo resources){
        return new ResponseEntity<>(pastDiseaseInfoService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改disease")
    @ApiOperation("修改disease")
    @PreAuthorize("@el.check('pastDiseaseInfo:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody PastDiseaseInfo resources){
        pastDiseaseInfoService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除disease")
    @ApiOperation("删除disease")
    @PreAuthorize("@el.check('pastDiseaseInfo:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        pastDiseaseInfoService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}