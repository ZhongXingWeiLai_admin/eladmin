/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.medicalhistorybefore.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-04
**/
@Entity
@Data
@Table(name="medical_history_before")
public class MedicalHistoryBefore implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "have_medical_history")
    @ApiModelProperty(value = "有无既往病史")
    private Integer haveMedicalHistory;

    @Column(name = "medical_history_details_string")
    @ApiModelProperty(value = "病史详情")
    private String medicalHistoryDetailsString;

    @Column(name = "have_medical_family_history")
    @ApiModelProperty(value = "有无家族病史")
    private Integer haveMedicalFamilyHistory;

    @Column(name = "relationship")
    @ApiModelProperty(value = "与患者关系")
    private Integer relationship;

    @Column(name = "pedigree")
    @ApiModelProperty(value = "系谱图")
    private String pedigree;

    @Column(name = "have_immune_medical_family_history")
    @ApiModelProperty(value = "免疫系统疾病家族史有无")
    private Integer haveImmuneMedicalFamilyHistory;

    @Column(name = "immune_medical_family_name_string")
    @ApiModelProperty(value = "免疫系统疾病家族史_诊断名称")
    private String immuneMedicalFamilyNameString;

    @Column(name = "smoke")
    @ApiModelProperty(value = "吸烟史")
    private Integer smoke;

    @Column(name = "quit_smoking")
    @ApiModelProperty(value = "是否戒烟:")
    private Integer quitSmoking;

    @Column(name = "drink")
    @ApiModelProperty(value = "饮酒史:")
    private Integer drink;

    @Column(name = "quit_drinking")
    @ApiModelProperty(value = "是否戒酒:")
    private Integer quitDrinking;

    @Column(name = "is_ruzu")
    @ApiModelProperty(value = "患者入组情况:")
    private Integer isRuzu;

    @Column(name = "remarks")
    @ApiModelProperty(value = "备注::")
    private String remarks;

    public void copy(MedicalHistoryBefore source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}