/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.medicalhistorybefore.service.impl;

import me.liangjun.medicalhistorybefore.domain.PastDiseaseInfo;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.medicalhistorybefore.repository.PastDiseaseInfoRepository;
import me.liangjun.medicalhistorybefore.service.PastDiseaseInfoService;
import me.liangjun.medicalhistorybefore.service.dto.PastDiseaseInfoDto;
import me.liangjun.medicalhistorybefore.service.dto.PastDiseaseInfoQueryCriteria;
import me.liangjun.medicalhistorybefore.service.mapstruct.PastDiseaseInfoMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-12
**/
@Service
@RequiredArgsConstructor
public class PastDiseaseInfoServiceImpl implements PastDiseaseInfoService {

    private final PastDiseaseInfoRepository pastDiseaseInfoRepository;
    private final PastDiseaseInfoMapper pastDiseaseInfoMapper;

    @Override
    public Map<String,Object> queryAll(PastDiseaseInfoQueryCriteria criteria, Pageable pageable){
        Page<PastDiseaseInfo> page = pastDiseaseInfoRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(pastDiseaseInfoMapper::toDto));
    }

    @Override
    public List<PastDiseaseInfoDto> queryAll(PastDiseaseInfoQueryCriteria criteria){
        return pastDiseaseInfoMapper.toDto(pastDiseaseInfoRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public PastDiseaseInfoDto findById(Long id) {
        PastDiseaseInfo pastDiseaseInfo = pastDiseaseInfoRepository.findById(id).orElseGet(PastDiseaseInfo::new);
        ValidationUtil.isNull(pastDiseaseInfo.getId(),"PastDiseaseInfo","id",id);
        return pastDiseaseInfoMapper.toDto(pastDiseaseInfo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PastDiseaseInfoDto create(PastDiseaseInfo resources) {
        return pastDiseaseInfoMapper.toDto(pastDiseaseInfoRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PastDiseaseInfo resources) {
        PastDiseaseInfo pastDiseaseInfo = pastDiseaseInfoRepository.findById(resources.getId()).orElseGet(PastDiseaseInfo::new);
        ValidationUtil.isNull( pastDiseaseInfo.getId(),"PastDiseaseInfo","id",resources.getId());
        pastDiseaseInfo.copy(resources);
        pastDiseaseInfoRepository.save(pastDiseaseInfo);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            pastDiseaseInfoRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<PastDiseaseInfoDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (PastDiseaseInfoDto pastDiseaseInfo : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", pastDiseaseInfo.getPId());
            map.put("免疫系统疾病和眼科疾病的名称", pastDiseaseInfo.getDiagnosisName());
            map.put("年份", pastDiseaseInfo.getYears());
            map.put("服用药物", pastDiseaseInfo.getMedicine());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}