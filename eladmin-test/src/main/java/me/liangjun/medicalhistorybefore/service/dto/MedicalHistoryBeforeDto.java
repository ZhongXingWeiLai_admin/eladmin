/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.medicalhistorybefore.service.dto;

import lombok.Data;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-04
**/
@Data
public class MedicalHistoryBeforeDto implements Serializable {

    /** ID */
    private Long id;

    /** pID */
    private Long pId;

    /** 有无既往病史 */
    private Integer haveMedicalHistory = 0;

    /** 病史详情 */
    private List<String> medicalHistoryDetailsString = new ArrayList<>();
    private List<PastDiseaseDto> medicalHistoryDetails = new ArrayList<>();

    /** 有无家族病史 */
    private Integer haveMedicalFamilyHistory = 0;

    /** 与患者关系 */
    private Integer relationship = 0;

    /** 系谱图 */
    private String pedigree = "";

    /** 免疫系统疾病家族史有无 */
    private Integer haveImmuneMedicalFamilyHistory = 0;

    /** 免疫系统疾病家族史_诊断名称 */
    private List<String> familyHistoryDtoListString = new ArrayList<>();
    private List<ImmuneSystemDiseaseFamilyHistoryDto> familyHistoryDtoList = new ArrayList<>();

    /** 吸烟史 */
    private Integer smoke = 0;

    /** 是否戒烟: */
    private Integer quitSmoking = 0;

    /** 饮酒史: */
    private Integer drink = 0;

    /** 是否戒酒: */
    private Integer quitDrinking = 0;

    /** 患者入组情况: */
    private Integer isRuzu = 0;

    /** 备注:: */
    private String remarks = "";
}