/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.medicalhistorybefore.service.dto;

import lombok.Data;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-12
**/
@Data
public class PastDiseaseInfoDto implements Serializable {

    /** ID */
    private Long id;

    /** pID */
    private Long pId;

    /** 疾病名称 */
    private String diseaseName;

    /** 免疫系统疾病和眼科疾病的名称 */
    private String diagnosisName;

    /** 年份 */
    private String years;

    /** 服用药物 */
    private String medicine;
}