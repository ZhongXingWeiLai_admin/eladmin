/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.medicalhistorybefore.service.mapstruct;

import cn.hutool.core.collection.CollUtil;
import me.liangjun.medicalhistorybefore.domain.ImmuneSystemDiseaseFamilyHistory;
import me.liangjun.medicalhistorybefore.domain.PastDisease;
import me.liangjun.medicalhistorybefore.service.dto.PastDiseaseDto;
import me.zhengjie.base.BaseMapper;
import me.liangjun.medicalhistorybefore.domain.MedicalHistoryBefore;
import me.liangjun.medicalhistorybefore.service.dto.MedicalHistoryBeforeDto;
import org.mapstruct.*;

import java.util.Arrays;
import java.util.List;

/**
 * @author wei
 * @website https://el-admin.vip
 * @date 2021-09-04
 **/
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {ImmuneSystemDiseaseFamilyHistoryMapper.class, PastDiseaseMapper.class})
public interface MedicalHistoryBeforeMapper extends BaseMapper<MedicalHistoryBeforeDto, MedicalHistoryBefore> {

    @Override
    @Mappings({
            @Mapping(source = "medicalHistoryDetailsString",target = "medicalHistoryDetailsString"),
            @Mapping(source = "familyHistoryDtoListString",target = "immuneMedicalFamilyNameString"),
    })
    MedicalHistoryBefore toEntity(MedicalHistoryBeforeDto dto);

    @Override
    @Mappings({
            @Mapping(source = "medicalHistoryDetailsString",target = "medicalHistoryDetailsString"),
            @Mapping(source = "immuneMedicalFamilyNameString",target = "familyHistoryDtoListString"),
    })
    MedicalHistoryBeforeDto toDto(MedicalHistoryBefore entity);

    @Mappings({
            @Mapping(source = "immuneSystemDiseaseFamilyHistories",target = "familyHistoryDtoList"),
            @Mapping(source = "pastDiseases",target = "medicalHistoryDetails"),
            @Mapping(source = "medicalHistoryBefore.medicalHistoryDetailsString",target = "medicalHistoryDetailsString"),
            @Mapping(source = "medicalHistoryBefore.immuneMedicalFamilyNameString",target = "familyHistoryDtoListString"),
    })
    MedicalHistoryBeforeDto domain2dto(MedicalHistoryBefore medicalHistoryBefore,
                                       List<ImmuneSystemDiseaseFamilyHistory> immuneSystemDiseaseFamilyHistories,
                                       List<PastDiseaseDto> pastDiseases);



//    default List<String> str2List(String src){
//        String[] split = src.split(",");
//        List<String> result = Arrays.asList(split);
//        return result;
//    }
//
//    // list转str
//    default String list2Str(List<String> src){
//        if (CollUtil.isEmpty(src)) {
//            return "";
//        }
//        StringBuffer sb = new StringBuffer();
//        src.stream().forEach(item -> sb.append(item.replace("\"","")).append(","));
//        return sb.substring(0, sb.length() - 1).toString();
//    }

}