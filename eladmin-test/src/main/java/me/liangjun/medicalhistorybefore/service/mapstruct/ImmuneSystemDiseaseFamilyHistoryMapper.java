package me.liangjun.medicalhistorybefore.service.mapstruct;

import me.liangjun.medicalhistorybefore.domain.ImmuneSystemDiseaseFamilyHistory;
import me.liangjun.medicalhistorybefore.service.dto.ImmuneSystemDiseaseFamilyHistoryDto;
import me.zhengjie.base.BaseMapper;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ImmuneSystemDiseaseFamilyHistoryMapper extends BaseMapper<ImmuneSystemDiseaseFamilyHistoryDto, ImmuneSystemDiseaseFamilyHistory> {

}
