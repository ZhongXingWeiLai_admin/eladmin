package me.liangjun.medicalhistorybefore.service.dto;

import lombok.Data;
import me.liangjun.eyemovements.service.dto.EyeMovementsScoreDto;
import me.liangjun.healthcheckup.service.dto.HealthCheckupDto;
import me.liangjun.mg.service.dto.MgScoreDto;
import me.liangjun.mgadl.service.dto.MgAdlScoreDto;
import me.liangjun.mgqol.service.dto.MgQolScoreDto;
import me.liangjun.ocularqmg.service.dto.OcularQmgScoreDto;
import me.liangjun.qmg.service.dto.QmgScoreDto;

import java.io.Serializable;

/**
 * @author jun
 * @version 1.0
 * @date 2021/10/12 8:53 下午
 */
@Data
public class BaseLineCaseDto implements Serializable {
    private HealthCheckupDto healthCheckup;
    private EyeMovementsScoreDto eyeMovementsScore;
    private OcularQmgScoreDto ocularQmgScore;
    private MgAdlScoreDto mgAdlScore;
    private QmgScoreDto qmgScore;
    private MgScoreDto mgScore;
    private MgQolScoreDto mgQolScore;
}
