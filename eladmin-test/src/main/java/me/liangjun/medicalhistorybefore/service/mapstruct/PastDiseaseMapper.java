package me.liangjun.medicalhistorybefore.service.mapstruct;

import cn.hutool.core.collection.CollUtil;
import me.liangjun.medicalhistorybefore.domain.PastDisease;
import me.liangjun.medicalhistorybefore.domain.PastDiseaseInfo;
import me.liangjun.medicalhistorybefore.service.dto.PastDiseaseDto;
import me.zhengjie.base.BaseMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import java.util.Arrays;
import java.util.List;


@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {PastDiseaseInfoMapper.class})
public interface PastDiseaseMapper extends BaseMapper<PastDiseaseDto, PastDisease> {

    @Override
    @Mappings({
            @Mapping(source = "diagnosisName",target = "diagnosisName"),
            @Mapping(source = "treatment",target = "treatment"),
    })
    PastDiseaseDto toDto(PastDisease entity);

    @Override
    @Mappings({
            @Mapping(source = "diagnosisName",target = "diagnosisName"),
            @Mapping(source = "treatment",target = "treatment"),
    })
    PastDisease toEntity(PastDiseaseDto dto);

    @Mapping(source = "pastDiseaseInfos",target = "pastDiseaseInfoDtos")
    PastDiseaseDto domain2dto(PastDisease pastDisease, List<PastDiseaseInfo> pastDiseaseInfos);

    default List<String> str2List(String src){
        String[] split = src.split(",");
        List<String> result = Arrays.asList(split);
        return result;
    }

    // list转str
    default String list2Str(List<String> src){
        if (CollUtil.isEmpty(src)) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        src.stream().forEach(item -> sb.append(item.replace("\"","")).append(","));
        return sb.substring(0, sb.length() - 1).toString();
    }
}
