package me.liangjun.medicalhistorybefore.repository;

import me.liangjun.medicalhistorybefore.domain.PastDisease;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PastDiseaseRepository extends JpaRepository<PastDisease, Long>, JpaSpecificationExecutor<PastDisease> {

    @Query(value = "select * from past_disease where p_id = ?1",nativeQuery = true)
    List<PastDisease> findListByPid(long pid);

    @Query(value = "select * from past_disease where p_id = ?1 and disease_name = ?2", nativeQuery = true)
    PastDisease findOne(long pid, String diseaseName);
}
