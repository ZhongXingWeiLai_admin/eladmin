package me.liangjun.medicalhistorybefore.repository;

import me.liangjun.medicalhistorybefore.domain.ImmuneSystemDiseaseFamilyHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ImmuneSystemDiseaseFamilyHistoryRepository extends JpaRepository<ImmuneSystemDiseaseFamilyHistory, Long>, JpaSpecificationExecutor<ImmuneSystemDiseaseFamilyHistory> {

    @Query(value = "select * from immune_system_disease_family_history where p_id = ?1",nativeQuery = true)
    List<ImmuneSystemDiseaseFamilyHistory> findListByPid(long pid);

    @Query(value = "select * from immune_system_disease_family_history where p_id = ?1 and name = ?2",nativeQuery = true)
    ImmuneSystemDiseaseFamilyHistory findOne(long pid,Integer name);


}
