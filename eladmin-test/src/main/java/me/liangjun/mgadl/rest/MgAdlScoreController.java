/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.mgadl.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.mgadl.domain.MgAdlScore;
import me.liangjun.mgadl.service.MgAdlScoreService;
import me.liangjun.mgadl.service.dto.MgAdlScoreQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-04
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "mgadl管理")
@RequestMapping("/api/mgAdlScore")
public class MgAdlScoreController {

    private final MgAdlScoreService mgAdlScoreService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('mgAdlScore:list')")
    public void download(HttpServletResponse response, MgAdlScoreQueryCriteria criteria) throws IOException {
        mgAdlScoreService.download(mgAdlScoreService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询mgadl")
    @ApiOperation("查询mgadl")
    @PreAuthorize("@el.check('mgAdlScore:list')")
    public ResponseEntity<Object> query(MgAdlScoreQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(mgAdlScoreService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增mgadl")
    @ApiOperation("新增mgadl")
    @PreAuthorize("@el.check('mgAdlScore:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody MgAdlScore resources){
        return new ResponseEntity<>(mgAdlScoreService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改mgadl")
    @ApiOperation("修改mgadl")
    @PreAuthorize("@el.check('mgAdlScore:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody MgAdlScore resources){
        mgAdlScoreService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除mgadl")
    @ApiOperation("删除mgadl")
    @PreAuthorize("@el.check('mgAdlScore:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        mgAdlScoreService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}