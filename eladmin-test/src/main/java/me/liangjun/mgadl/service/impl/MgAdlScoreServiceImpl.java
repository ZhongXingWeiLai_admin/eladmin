/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.mgadl.service.impl;

import me.liangjun.mgadl.domain.MgAdlScore;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.mgadl.repository.MgAdlScoreRepository;
import me.liangjun.mgadl.service.MgAdlScoreService;
import me.liangjun.mgadl.service.dto.MgAdlScoreDto;
import me.liangjun.mgadl.service.dto.MgAdlScoreQueryCriteria;
import me.liangjun.mgadl.service.mapstruct.MgAdlScoreMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-04
**/
@Service
@RequiredArgsConstructor
public class MgAdlScoreServiceImpl implements MgAdlScoreService {

    private final MgAdlScoreRepository mgAdlScoreRepository;
    private final MgAdlScoreMapper mgAdlScoreMapper;

    @Override
    public Map<String,Object> queryAll(MgAdlScoreQueryCriteria criteria, Pageable pageable){
        Page<MgAdlScore> page = mgAdlScoreRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(mgAdlScoreMapper::toDto));
    }

    @Override
    public List<MgAdlScoreDto> queryAll(MgAdlScoreQueryCriteria criteria){
        return mgAdlScoreMapper.toDto(mgAdlScoreRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public MgAdlScoreDto findById(Long id) {
        MgAdlScore mgAdlScore = mgAdlScoreRepository.findById(id).orElseGet(MgAdlScore::new);
        ValidationUtil.isNull(mgAdlScore.getId(),"MgAdlScore","id",id);
        return mgAdlScoreMapper.toDto(mgAdlScore);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public MgAdlScoreDto create(MgAdlScore resources) {
        return mgAdlScoreMapper.toDto(mgAdlScoreRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(MgAdlScore resources) {
        MgAdlScore mgAdlScore = mgAdlScoreRepository.findById(resources.getId()).orElseGet(MgAdlScore::new);
        ValidationUtil.isNull( mgAdlScore.getId(),"MgAdlScore","id",resources.getId());
        mgAdlScore.copy(resources);
        mgAdlScoreRepository.save(mgAdlScore);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            mgAdlScoreRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<MgAdlScoreDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (MgAdlScoreDto mgAdlScore : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", mgAdlScore.getPId());
            map.put("言语:", mgAdlScore.getSpeech());
            map.put("咀嚼", mgAdlScore.getChew());
            map.put("吞咽", mgAdlScore.getSwallow());
            map.put("呼吸", mgAdlScore.getBreathing());
            map.put("刷牙或梳头", mgAdlScore.getTeethHair());
            map.put("坐立站起", mgAdlScore.getSitUp());
            map.put("复视", mgAdlScore.getDiplopia());
            map.put("眼睑下垂", mgAdlScore.getBlepharoptosis());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public MgAdlScoreDto findByPid(Long pid) {
        return mgAdlScoreMapper.toDto(mgAdlScoreRepository.findByPid(pid));
    }
}