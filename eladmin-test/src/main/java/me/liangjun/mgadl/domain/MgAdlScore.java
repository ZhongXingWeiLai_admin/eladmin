/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.mgadl.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-04
**/
@Entity
@Data
@Table(name="mg_adl_score")
public class MgAdlScore implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "speech")
    @ApiModelProperty(value = "言语:")
    private Integer speech;

    @Column(name = "chew")
    @ApiModelProperty(value = "咀嚼")
    private Integer chew;

    @Column(name = "swallow")
    @ApiModelProperty(value = "吞咽")
    private Integer swallow;

    @Column(name = "breathing")
    @ApiModelProperty(value = "呼吸")
    private Integer breathing;

    @Column(name = "teeth_hair")
    @ApiModelProperty(value = "刷牙或梳头")
    private Integer teethHair;

    @Column(name = "sit_up")
    @ApiModelProperty(value = "坐立站起")
    private Integer sitUp;

    @Column(name = "diplopia")
    @ApiModelProperty(value = "复视")
    private Integer diplopia;

    @Column(name = "blepharoptosis")
    @ApiModelProperty(value = "眼睑下垂")
    private Integer blepharoptosis;

    public void copy(MgAdlScore source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}