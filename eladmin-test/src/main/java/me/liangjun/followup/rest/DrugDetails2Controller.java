/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.followup.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.followup.domain.DrugDetails2;
import me.liangjun.followup.service.DrugDetails2Service;
import me.liangjun.followup.service.dto.DrugDetails2QueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-17
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "adjustMedication管理")
@RequestMapping("/api/drugDetails2")
public class DrugDetails2Controller {

    private final DrugDetails2Service drugDetails2Service;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('drugDetails2:list')")
    public void download(HttpServletResponse response, DrugDetails2QueryCriteria criteria) throws IOException {
        drugDetails2Service.download(drugDetails2Service.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询adjustMedication")
    @ApiOperation("查询adjustMedication")
    @PreAuthorize("@el.check('drugDetails2:list')")
    public ResponseEntity<Object> query(DrugDetails2QueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(drugDetails2Service.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增adjustMedication")
    @ApiOperation("新增adjustMedication")
    @PreAuthorize("@el.check('drugDetails2:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody DrugDetails2 resources){
        return new ResponseEntity<>(drugDetails2Service.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改adjustMedication")
    @ApiOperation("修改adjustMedication")
    @PreAuthorize("@el.check('drugDetails2:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody DrugDetails2 resources){
        drugDetails2Service.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除adjustMedication")
    @ApiOperation("删除adjustMedication")
    @PreAuthorize("@el.check('drugDetails2:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        drugDetails2Service.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}