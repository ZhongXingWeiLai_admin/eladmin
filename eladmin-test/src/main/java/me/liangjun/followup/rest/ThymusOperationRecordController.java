/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.followup.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.followup.domain.ThymusOperationRecord;
import me.liangjun.followup.service.ThymusOperationRecordService;
import me.liangjun.followup.service.dto.ThymusOperationRecordQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-17
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "thymusOperationRecord管理")
@RequestMapping("/api/thymusOperationRecord")
public class ThymusOperationRecordController {

    private final ThymusOperationRecordService thymusOperationRecordService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('thymusOperationRecord:list')")
    public void download(HttpServletResponse response, ThymusOperationRecordQueryCriteria criteria) throws IOException {
        thymusOperationRecordService.download(thymusOperationRecordService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询thymusOperationRecord")
    @ApiOperation("查询thymusOperationRecord")
    @PreAuthorize("@el.check('thymusOperationRecord:list')")
    public ResponseEntity<Object> query(ThymusOperationRecordQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(thymusOperationRecordService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增thymusOperationRecord")
    @ApiOperation("新增thymusOperationRecord")
    @PreAuthorize("@el.check('thymusOperationRecord:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody ThymusOperationRecord resources){
        return new ResponseEntity<>(thymusOperationRecordService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改thymusOperationRecord")
    @ApiOperation("修改thymusOperationRecord")
    @PreAuthorize("@el.check('thymusOperationRecord:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody ThymusOperationRecord resources){
        thymusOperationRecordService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除thymusOperationRecord")
    @ApiOperation("删除thymusOperationRecord")
    @PreAuthorize("@el.check('thymusOperationRecord:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        thymusOperationRecordService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}