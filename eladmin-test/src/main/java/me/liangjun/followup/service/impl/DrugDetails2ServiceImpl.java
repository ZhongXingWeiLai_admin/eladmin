/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.followup.service.impl;

import me.liangjun.followup.domain.DrugDetails2;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.followup.repository.DrugDetails2Repository;
import me.liangjun.followup.service.DrugDetails2Service;
import me.liangjun.followup.service.dto.DrugDetails2Dto;
import me.liangjun.followup.service.dto.DrugDetails2QueryCriteria;
import me.liangjun.followup.service.mapstruct.DrugDetails2Mapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-17
**/
@Service
@RequiredArgsConstructor
public class DrugDetails2ServiceImpl implements DrugDetails2Service {

    private final DrugDetails2Repository drugDetails2Repository;
    private final DrugDetails2Mapper drugDetails2Mapper;

    @Override
    public Map<String,Object> queryAll(DrugDetails2QueryCriteria criteria, Pageable pageable){
        Page<DrugDetails2> page = drugDetails2Repository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(drugDetails2Mapper::toDto));
    }

    @Override
    public List<DrugDetails2Dto> queryAll(DrugDetails2QueryCriteria criteria){
        return drugDetails2Mapper.toDto(drugDetails2Repository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public DrugDetails2Dto findById(Long id) {
        DrugDetails2 drugDetails2 = drugDetails2Repository.findById(id).orElseGet(DrugDetails2::new);
        ValidationUtil.isNull(drugDetails2.getId(),"DrugDetails2","id",id);
        return drugDetails2Mapper.toDto(drugDetails2);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public DrugDetails2Dto create(DrugDetails2 resources) {
        return drugDetails2Mapper.toDto(drugDetails2Repository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(DrugDetails2 resources) {
        DrugDetails2 drugDetails2 = drugDetails2Repository.findById(resources.getId()).orElseGet(DrugDetails2::new);
        ValidationUtil.isNull( drugDetails2.getId(),"DrugDetails2","id",resources.getId());
        drugDetails2.copy(resources);
        drugDetails2Repository.save(drugDetails2);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            drugDetails2Repository.deleteById(id);
        }
    }

    @Override
    public void download(List<DrugDetails2Dto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (DrugDetails2Dto drugDetails2 : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", drugDetails2.getPId());
            map.put("药编号", drugDetails2.getDId());
            map.put("开始用药时间", drugDetails2.getStartDate());
            map.put("结束用药时间", drugDetails2.getEndDate());
            map.put("用药天数", drugDetails2.getDays());
            map.put("用法", drugDetails2.getUseMethod());
            map.put("起效时间", drugDetails2.getOnsetDate());
            map.put("副作用", drugDetails2.getSideEffect());
            map.put("是否停药", drugDetails2.getStopState());
            map.put("停药原因", drugDetails2.getReason());
            map.put("是否-过性加重", drugDetails2.getAggravateState());
            map.put("治疗情况", drugDetails2.getTreatmentDetails());
            map.put("基因", drugDetails2.getGene());
            map.put("抑制位置", drugDetails2.getSuppressionPosition());
            map.put("基因型", drugDetails2.getGenotype());
            map.put("每次服用（粒）", drugDetails2.getTake());
            map.put("每次剂量（mg）", drugDetails2.getDose());
            map.put("当日服药剂量（mg）", drugDetails2.getDoseToday());
            map.put("目前总服药剂量", drugDetails2.getDoseTotal());
            map.put("服药后症状改善情况/使用效果", drugDetails2.getImprove());
            map.put("异常指标描述", drugDetails2.getAbnormalDescribe());
            map.put("副作用异常指标", drugDetails2.getSideEffectAbnormalIndex());
            map.put("他克莫司相关检查", drugDetails2.getTaCheck());
            map.put("他克莫司治疗情况", drugDetails2.getTaTreatment());
            map.put("他克莫司（ng/ml）浓度", drugDetails2.getTaConcentration());
            map.put("检测日期", drugDetails2.getTaDetectionDate());
            map.put("服用五酯滴丸", drugDetails2.getTaTakeWu());
            map.put("日用药剂量（mg/kg）五酯滴丸", drugDetails2.getTaTakeWuDose());
            map.put("起效时间", drugDetails2.getTaTakeWuEffectiveTime());
            map.put("C/D值五酯滴丸", drugDetails2.getTaTakeWuCd());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}