/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.followup.service.impl;

import me.liangjun.followup.domain.ThymusOperationRecord;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.followup.repository.ThymusOperationRecordRepository;
import me.liangjun.followup.service.ThymusOperationRecordService;
import me.liangjun.followup.service.dto.ThymusOperationRecordDto;
import me.liangjun.followup.service.dto.ThymusOperationRecordQueryCriteria;
import me.liangjun.followup.service.mapstruct.ThymusOperationRecordMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-17
**/
@Service
@RequiredArgsConstructor
public class ThymusOperationRecordServiceImpl implements ThymusOperationRecordService {

    private final ThymusOperationRecordRepository thymusOperationRecordRepository;
    private final ThymusOperationRecordMapper thymusOperationRecordMapper;

    @Override
    public Map<String,Object> queryAll(ThymusOperationRecordQueryCriteria criteria, Pageable pageable){
        Page<ThymusOperationRecord> page = thymusOperationRecordRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(thymusOperationRecordMapper::toDto));
    }

    @Override
    public List<ThymusOperationRecordDto> queryAll(ThymusOperationRecordQueryCriteria criteria){
        return thymusOperationRecordMapper.toDto(thymusOperationRecordRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public ThymusOperationRecordDto findById(Long id) {
        ThymusOperationRecord thymusOperationRecord = thymusOperationRecordRepository.findById(id).orElseGet(ThymusOperationRecord::new);
        ValidationUtil.isNull(thymusOperationRecord.getId(),"ThymusOperationRecord","id",id);
        return thymusOperationRecordMapper.toDto(thymusOperationRecord);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ThymusOperationRecordDto create(ThymusOperationRecord resources) {
        return thymusOperationRecordMapper.toDto(thymusOperationRecordRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(ThymusOperationRecord resources) {
        ThymusOperationRecord thymusOperationRecord = thymusOperationRecordRepository.findById(resources.getId()).orElseGet(ThymusOperationRecord::new);
        ValidationUtil.isNull( thymusOperationRecord.getId(),"ThymusOperationRecord","id",resources.getId());
        thymusOperationRecord.copy(resources);
        thymusOperationRecordRepository.save(thymusOperationRecord);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            thymusOperationRecordRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<ThymusOperationRecordDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (ThymusOperationRecordDto thymusOperationRecord : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", thymusOperationRecord.getPId());
            map.put("胸腺手术史", thymusOperationRecord.getThymicSurgeryHistory());
            map.put("胸腺手术时间", thymusOperationRecord.getThymicSurgeryTime());
            map.put("与首发症状的时间间隔", thymusOperationRecord.getTimeInterval());
            map.put("术前肌无力危象", thymusOperationRecord.getMyastheniaCrisis());
            map.put("术前MGFA分型（术前1周）", thymusOperationRecord.getMgfa());
            map.put("有无球部肌肉受累", thymusOperationRecord.getBallMuscleInvolvement());
            map.put("术前用药（1月内）", thymusOperationRecord.getPreoperativeMedication());
            map.put("术前丙球冲击（1月内）", thymusOperationRecord.getCBallImpact());
            map.put("术前激素冲击（1月内）", thymusOperationRecord.getHormonalShock());
            map.put("术前血浆置换（1月内）", thymusOperationRecord.getPlasmaExchange());
            map.put("术前肺功", thymusOperationRecord.getPulmonaryFunction());
            map.put("手术方式", thymusOperationRecord.getOperationMode());
            map.put("切除范围", thymusOperationRecord.getResectionRange());
            map.put("手术时长", thymusOperationRecord.getOperationDuration());
            map.put("术中失血量（ml）", thymusOperationRecord.getIntraoperativeBloodLoss());
            map.put("术后肌无力危象", thymusOperationRecord.getMyastheniaCrisisAfter());
            map.put("术后并发症", thymusOperationRecord.getComplication());
            map.put("术后病理", thymusOperationRecord.getPathology());
            map.put("图片", thymusOperationRecord.getPicture());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}