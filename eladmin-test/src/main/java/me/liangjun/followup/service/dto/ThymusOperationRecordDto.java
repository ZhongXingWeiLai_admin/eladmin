/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.followup.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-17
**/
@Data
public class ThymusOperationRecordDto implements Serializable {

    /** ID */
    private Long id;

    /** pID */
    private Long pId;

    /** 胸腺手术史 */
    private Integer thymicSurgeryHistory;

    /** 胸腺手术时间 */
    private Timestamp thymicSurgeryTime;

    /** 与首发症状的时间间隔 */
    private String timeInterval;

    /** 术前肌无力危象 */
    private Integer myastheniaCrisis;

    /** 术前MGFA分型（术前1周） */
    private Integer mgfa;

    /** 有无球部肌肉受累 */
    private Integer ballMuscleInvolvement;

    /** 术前用药（1月内） */
    private Integer preoperativeMedication;

    /** 术前丙球冲击（1月内） */
    private Integer cBallImpact;

    /** 术前激素冲击（1月内） */
    private Integer hormonalShock;

    /** 术前血浆置换（1月内） */
    private Integer plasmaExchange;

    /** 术前肺功 */
    private Integer pulmonaryFunction;

    /** 手术方式 */
    private Integer operationMode;

    /** 切除范围 */
    private Integer resectionRange;

    /** 手术时长 */
    private String operationDuration;

    /** 术中失血量（ml） */
    private String intraoperativeBloodLoss;

    /** 术后肌无力危象 */
    private Integer myastheniaCrisisAfter;

    /** 术后并发症 */
    private Integer complication;

    /** 术后病理 */
    private Integer pathology;

    /** 图片 */
    private String picture;
}