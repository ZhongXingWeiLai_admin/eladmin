/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.followup.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-17
**/
@Entity
@Data
@Table(name="thymus_operation_record")
public class ThymusOperationRecord implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "thymic_surgery_history")
    @ApiModelProperty(value = "胸腺手术史")
    private Integer thymicSurgeryHistory;

    @Column(name = "thymic_surgery_time")
    @ApiModelProperty(value = "胸腺手术时间")
    private Timestamp thymicSurgeryTime;

    @Column(name = "time_interval")
    @ApiModelProperty(value = "与首发症状的时间间隔")
    private String timeInterval;

    @Column(name = "myasthenia_crisis")
    @ApiModelProperty(value = "术前肌无力危象")
    private Integer myastheniaCrisis;

    @Column(name = "mgfa")
    @ApiModelProperty(value = "术前MGFA分型（术前1周）")
    private Integer mgfa;

    @Column(name = "ball_muscle_involvement")
    @ApiModelProperty(value = "有无球部肌肉受累")
    private Integer ballMuscleInvolvement;

    @Column(name = "preoperative_medication")
    @ApiModelProperty(value = "术前用药（1月内）")
    private Integer preoperativeMedication;

    @Column(name = "c_ball_impact")
    @ApiModelProperty(value = "术前丙球冲击（1月内）")
    private Integer cBallImpact;

    @Column(name = "hormonal_shock")
    @ApiModelProperty(value = "术前激素冲击（1月内）")
    private Integer hormonalShock;

    @Column(name = "plasma_exchange")
    @ApiModelProperty(value = "术前血浆置换（1月内）")
    private Integer plasmaExchange;

    @Column(name = "pulmonary_function")
    @ApiModelProperty(value = "术前肺功")
    private Integer pulmonaryFunction;

    @Column(name = "operation_mode")
    @ApiModelProperty(value = "手术方式")
    private Integer operationMode;

    @Column(name = "resection_range")
    @ApiModelProperty(value = "切除范围")
    private Integer resectionRange;

    @Column(name = "operation_duration")
    @ApiModelProperty(value = "手术时长")
    private String operationDuration;

    @Column(name = "intraoperative_blood_loss")
    @ApiModelProperty(value = "术中失血量（ml）")
    private String intraoperativeBloodLoss;

    @Column(name = "myasthenia_crisis_after")
    @ApiModelProperty(value = "术后肌无力危象")
    private Integer myastheniaCrisisAfter;

    @Column(name = "complication")
    @ApiModelProperty(value = "术后并发症")
    private Integer complication;

    @Column(name = "pathology")
    @ApiModelProperty(value = "术后病理")
    private Integer pathology;

    @Column(name = "picture")
    @ApiModelProperty(value = "图片")
    private String picture;

    public void copy(ThymusOperationRecord source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}