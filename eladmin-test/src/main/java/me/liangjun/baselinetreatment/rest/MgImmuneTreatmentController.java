/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.baselinetreatment.domain.MgImmuneTreatment;
import me.liangjun.baselinetreatment.service.MgImmuneTreatmentService;
import me.liangjun.baselinetreatment.service.dto.MgImmuneTreatmentQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-10-20
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "mgImmuneTreatment管理")
@RequestMapping("/api/mgImmuneTreatment")
public class MgImmuneTreatmentController {

    private final MgImmuneTreatmentService mgImmuneTreatmentService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('mgImmuneTreatment:list')")
    public void download(HttpServletResponse response, MgImmuneTreatmentQueryCriteria criteria) throws IOException {
        mgImmuneTreatmentService.download(mgImmuneTreatmentService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询mgImmuneTreatment")
    @ApiOperation("查询mgImmuneTreatment")
    @PreAuthorize("@el.check('mgImmuneTreatment:list')")
    public ResponseEntity<Object> query(MgImmuneTreatmentQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(mgImmuneTreatmentService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增mgImmuneTreatment")
    @ApiOperation("新增mgImmuneTreatment")
    @PreAuthorize("@el.check('mgImmuneTreatment:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody MgImmuneTreatment resources){
        return new ResponseEntity<>(mgImmuneTreatmentService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改mgImmuneTreatment")
    @ApiOperation("修改mgImmuneTreatment")
    @PreAuthorize("@el.check('mgImmuneTreatment:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody MgImmuneTreatment resources){
        mgImmuneTreatmentService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除mgImmuneTreatment")
    @ApiOperation("删除mgImmuneTreatment")
    @PreAuthorize("@el.check('mgImmuneTreatment:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        mgImmuneTreatmentService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}