/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.rest;

import me.liangjun.baselinetreatment.service.*;
import me.liangjun.baselinetreatment.service.dto.BaseLineTreatmentFDto;
import me.liangjun.baselinetreatment.service.dto.ImmunosuppressantDto;
import me.liangjun.baselinetreatmentdetails.service.BaselineTreatmentDetailsService;
import me.liangjun.baselinetreatmentdetails.service.dto.BaselineTreatmentDetailsDto;
import me.liangjun.medicalhistorybefore.service.dto.BaseLineCaseDto;
import me.zhengjie.annotation.AnonymousAccess;
import me.zhengjie.annotation.Log;
import me.liangjun.baselinetreatment.domain.BaselineTreatment;
import me.liangjun.baselinetreatment.service.dto.BaselineTreatmentQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-10-19
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "baselinetreatment管理")
@RequestMapping("/api/baselineTreatment")
public class BaselineTreatmentController {

    private final BaselineTreatmentService baselineTreatmentService;
    private final OtherTreatmentService otherTreatmentService;
    private final MgImmuneTreatmentService mgImmuneTreatmentService;
    private final ThymicSurgeryHistoryService thymicSurgeryHistoryService;
    private final BaselineTreatmentDetailsService baselineTreatmentDetailsService;
    private final ImmunosuppressantService immunosuppressantService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('baselineTreatment:list')")
    public void download(HttpServletResponse response, BaselineTreatmentQueryCriteria criteria) throws IOException {
        baselineTreatmentService.download(baselineTreatmentService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询baselinetreatment")
    @ApiOperation("查询baselinetreatment")
    @PreAuthorize("@el.check('baselineTreatment:list')")
    public ResponseEntity<Object> query(BaselineTreatmentQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(baselineTreatmentService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增baselinetreatment")
    @ApiOperation("新增baselinetreatment")
    @PreAuthorize("@el.check('baselineTreatment:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody BaseLineTreatmentFDto resources){
        return new ResponseEntity<>(baselineTreatmentService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改baselinetreatment")
    @ApiOperation("修改baselinetreatment")
    @PreAuthorize("@el.check('baselineTreatment:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody BaselineTreatment resources){
        baselineTreatmentService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除baselinetreatment")
    @ApiOperation("删除baselinetreatment")
    @PreAuthorize("@el.check('baselineTreatment:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        baselineTreatmentService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/queryByPid")
    @Log("查询基线治疗")
    @ApiOperation("查询基线治疗")
    @PreAuthorize("@el.check('baselineTreatment:list')")
    public ResponseEntity<Object> queryByPid(@RequestParam("pid") Long pid) {
        BaseLineTreatmentFDto dto = new BaseLineTreatmentFDto();
        dto.setBaselineTreatment(baselineTreatmentService.findByPid(pid));
        dto.setOtherTreatment(otherTreatmentService.findByPid(pid));
        dto.setMgImmuneTreatment(mgImmuneTreatmentService.findByPid(pid));
        dto.setThymicSurgeryHistory(thymicSurgeryHistoryService.findByPid(pid));
        List<BaselineTreatmentDetailsDto> list = baselineTreatmentDetailsService.findByPid(pid);
        if(list!=null&&list.size()>0) {
            dto.setDrug(list);
        }
        List<ImmunosuppressantDto> dtoList = immunosuppressantService.findByPid(pid);
        if (dtoList != null && dtoList.size() > 0) {
            dto.setImmunosuppressive(dtoList);
        }
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
}