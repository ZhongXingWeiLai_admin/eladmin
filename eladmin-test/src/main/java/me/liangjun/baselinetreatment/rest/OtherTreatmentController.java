/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.baselinetreatment.domain.OtherTreatment;
import me.liangjun.baselinetreatment.service.OtherTreatmentService;
import me.liangjun.baselinetreatment.service.dto.OtherTreatmentQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-10-20
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "other管理")
@RequestMapping("/api/otherTreatment")
public class OtherTreatmentController {

    private final OtherTreatmentService otherTreatmentService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('otherTreatment:list')")
    public void download(HttpServletResponse response, OtherTreatmentQueryCriteria criteria) throws IOException {
        otherTreatmentService.download(otherTreatmentService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询other")
    @ApiOperation("查询other")
    @PreAuthorize("@el.check('otherTreatment:list')")
    public ResponseEntity<Object> query(OtherTreatmentQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(otherTreatmentService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增other")
    @ApiOperation("新增other")
    @PreAuthorize("@el.check('otherTreatment:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody OtherTreatment resources){
        return new ResponseEntity<>(otherTreatmentService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改other")
    @ApiOperation("修改other")
    @PreAuthorize("@el.check('otherTreatment:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody OtherTreatment resources){
        otherTreatmentService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除other")
    @ApiOperation("删除other")
    @PreAuthorize("@el.check('otherTreatment:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        otherTreatmentService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}