/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.baselinetreatment.domain.Immunosuppressant;
import me.liangjun.baselinetreatment.service.ImmunosuppressantService;
import me.liangjun.baselinetreatment.service.dto.ImmunosuppressantQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-10-26
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "immunosuppressant管理")
@RequestMapping("/api/immunosuppressant")
public class ImmunosuppressantController {

    private final ImmunosuppressantService immunosuppressantService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('immunosuppressant:list')")
    public void download(HttpServletResponse response, ImmunosuppressantQueryCriteria criteria) throws IOException {
        immunosuppressantService.download(immunosuppressantService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询immunosuppressant")
    @ApiOperation("查询immunosuppressant")
    @PreAuthorize("@el.check('immunosuppressant:list')")
    public ResponseEntity<Object> query(ImmunosuppressantQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(immunosuppressantService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增immunosuppressant")
    @ApiOperation("新增immunosuppressant")
    @PreAuthorize("@el.check('immunosuppressant:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody Immunosuppressant resources){
        return new ResponseEntity<>(immunosuppressantService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改immunosuppressant")
    @ApiOperation("修改immunosuppressant")
    @PreAuthorize("@el.check('immunosuppressant:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody Immunosuppressant resources){
        immunosuppressantService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除immunosuppressant")
    @ApiOperation("删除immunosuppressant")
    @PreAuthorize("@el.check('immunosuppressant:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        immunosuppressantService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}