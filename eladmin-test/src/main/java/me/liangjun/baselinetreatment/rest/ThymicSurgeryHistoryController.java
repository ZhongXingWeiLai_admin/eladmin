/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.baselinetreatment.domain.ThymicSurgeryHistory;
import me.liangjun.baselinetreatment.service.ThymicSurgeryHistoryService;
import me.liangjun.baselinetreatment.service.dto.ThymicSurgeryHistoryQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-13
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "thymicsurgery管理")
@RequestMapping("/api/thymicSurgeryHistory")
public class ThymicSurgeryHistoryController {

    private final ThymicSurgeryHistoryService thymicSurgeryHistoryService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('thymicSurgeryHistory:list')")
    public void download(HttpServletResponse response, ThymicSurgeryHistoryQueryCriteria criteria) throws IOException {
        thymicSurgeryHistoryService.download(thymicSurgeryHistoryService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询thymicsurgery")
    @ApiOperation("查询thymicsurgery")
    @PreAuthorize("@el.check('thymicSurgeryHistory:list')")
    public ResponseEntity<Object> query(ThymicSurgeryHistoryQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(thymicSurgeryHistoryService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增thymicsurgery")
    @ApiOperation("新增thymicsurgery")
    @PreAuthorize("@el.check('thymicSurgeryHistory:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody ThymicSurgeryHistory resources){
        return new ResponseEntity<>(thymicSurgeryHistoryService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改thymicsurgery")
    @ApiOperation("修改thymicsurgery")
    @PreAuthorize("@el.check('thymicSurgeryHistory:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody ThymicSurgeryHistory resources){
        thymicSurgeryHistoryService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除thymicsurgery")
    @ApiOperation("删除thymicsurgery")
    @PreAuthorize("@el.check('thymicSurgeryHistory:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        thymicSurgeryHistoryService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}