/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.repository;

import me.liangjun.baselinetreatment.domain.Immunosuppressant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-10-26
**/
public interface ImmunosuppressantRepository extends JpaRepository<Immunosuppressant, Long>, JpaSpecificationExecutor<Immunosuppressant> {
    @Query(value = "select * from immunosuppressant where p_id = ?1", nativeQuery = true)
    List<Immunosuppressant> findByPid(Long pid);

    @Query(value = "select * from immunosuppressant where p_id = ?1 and d_name = ?2", nativeQuery = true)
    Immunosuppressant findOne(Long pid, String dName);
}