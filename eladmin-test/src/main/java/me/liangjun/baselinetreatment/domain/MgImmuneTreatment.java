/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-10-20
**/
@Entity
@Data
@Table(name="mg_immune_treatment")
public class MgImmuneTreatment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "glycopathy_use_date")
    @ApiModelProperty(value = "丙种球蛋白_使用日期")
    private Timestamp glycopathyUseDate;

    @Column(name = "glycopathy_dose")
    @ApiModelProperty(value = "丙种球蛋白_使用剂量")
    private String glycopathyDose;

    @Column(name = "glycopathy_effective_time")
    @ApiModelProperty(value = "丙种球蛋白_起效时间")
    private Timestamp glycopathyEffectiveTime;

    @Column(name = "glycopathy_use_effect")
    @ApiModelProperty(value = "丙种球蛋白_使用效果")
    private String glycopathyUseEffect;

    @Column(name = "plasma_replacement_date")
    @ApiModelProperty(value = "血浆置换日期")
    private Timestamp plasmaReplacementDate;

    @Column(name = "plasma_replacement_time")
    @ApiModelProperty(value = "血浆置换_起效时间")
    private Timestamp plasmaReplacementTime;

    @Column(name = "plasma_replacement")
    @ApiModelProperty(value = "血浆置换内容")
    private String plasmaReplacement;

    @Column(name = "immune_adsorption_date")
    @ApiModelProperty(value = "免疫吸附日期")
    private Timestamp immuneAdsorptionDate;

    @Column(name = "immune_adsorption_effective_time")
    @ApiModelProperty(value = "免疫吸附_起效时间")
    private Timestamp immuneAdsorptionEffectiveTime;

    @Column(name = "immune_adsorption_content")
    @ApiModelProperty(value = "免疫吸附内容")
    private String immuneAdsorptionContent;

    public void copy(MgImmuneTreatment source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}