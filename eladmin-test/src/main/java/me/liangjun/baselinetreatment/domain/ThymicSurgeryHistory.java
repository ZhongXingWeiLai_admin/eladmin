/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-13
**/
@Entity
@Data
@Table(name="thymic_surgery_history")
public class ThymicSurgeryHistory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "thymic_surgery_time")
    @ApiModelProperty(value = "胸腺手术时间")
    private Timestamp thymicSurgeryTime;

    @Column(name = "time_interval")
    @ApiModelProperty(value = "与首发症状的时间间隔")
    private String timeInterval;

    @Column(name = "crisis")
    @ApiModelProperty(value = "术前肌无力危象")
    private Integer crisis;

    @Column(name = "crisis_times")
    @ApiModelProperty(value = "危象次数")
    private String crisisTimes;

    @Column(name = "mgfa")
    @ApiModelProperty(value = "术前MGFA分型(术前1周)")
    private String mgfa;

    @Column(name = "spherical_muscle")
    @ApiModelProperty(value = "有无球部肌肉受累")
    private Integer sphericalMuscle;

    @Column(name = "preoperative_medication")
    @ApiModelProperty(value = "术前用药（1月内）")
    private Integer preoperativeMedication;

    @Column(name = "medication_type")
    @ApiModelProperty(value = "用药种类")
    private String medicationType;

    @Column(name = "dose")
    @ApiModelProperty(value = "溴吡斯的明日剂量")
    private String dose;

    @Column(name = "preoperative_use_time")
    @ApiModelProperty(value = "溴吡斯的明术前使用时间")
    private Timestamp preoperativeUseTime;

    @Column(name = "immunosuppressive")
    @ApiModelProperty(value = "免疫抑制剂")
    private String immunosuppressive;

    @Column(name = "bing_qiu")
    @ApiModelProperty(value = "术前丙球冲击（1月内）")
    private Integer bingQiu;

    @Column(name = "hormone")
    @ApiModelProperty(value = "术前激素冲击（1月内）")
    private Integer hormone;

    @Column(name = "plasma")
    @ApiModelProperty(value = "术前血浆置换（1月内）")
    private Integer plasma;

    @Column(name = "fei_gong")
    @ApiModelProperty(value = "术前肺功")
    private Integer feiGong;

    @Column(name = "fcv")
    @ApiModelProperty(value = "FVC")
    private String fcv;

    @Column(name = "fev1")
    @ApiModelProperty(value = "FEV1")
    private String fev1;

    @Column(name = "muscle_weakness")
    @ApiModelProperty(value = "术前肌无力症状")
    private Integer muscleWeakness;

    @Column(name = "surgical_approach")
    @ApiModelProperty(value = "手术方式")
    private Integer surgicalApproach;

    @Column(name = "surgery")
    @ApiModelProperty(value = "剑突下肋缘三孔手术切除:")
    private Integer surgery;

    @Column(name = "scope")
    @ApiModelProperty(value = "切除范围")
    private Integer scope;

    @Column(name = "surgical_duration")
    @ApiModelProperty(value = "手术时长")
    private String surgicalDuration;

    @Column(name = "lose_weight")
    @ApiModelProperty(value = "术中失血量（ml）")
    private String loseWeight;

    @Column(name = "muscle_weakness_after")
    @ApiModelProperty(value = "术后肌无力危象")
    private Integer muscleWeaknessAfter;

    @Column(name = "complication")
    @ApiModelProperty(value = "术后并发症")
    private String complication;

    @Column(name = "pathology")
    @ApiModelProperty(value = "术后病理")
    private String pathology;

    @Column(name = "picture")
    @ApiModelProperty(value = "图片")
    private String picture;

    public void copy(ThymicSurgeryHistory source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}