/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-10-19
**/
@Entity
@Data
@Table(name="baseline_treatment")
public class BaselineTreatment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "other_treatment")
    @ApiModelProperty(value = "其他治疗")
    private Integer otherTreatment;

    @Column(name = "mg_drug_treatment")
    @ApiModelProperty(value = "MG药物治疗")
    private String mgDrugTreatment;

    @Column(name = "mg_immune_treatment")
    @ApiModelProperty(value = "有无MG免疫治疗")
    private Integer mgImmuneTreatment;

    @Column(name = "thymic_surgery_treatment")
    @ApiModelProperty(value = "有无胸腺手术治疗")
    private Integer thymicSurgeryTreatment;

    public void copy(BaselineTreatment source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}