/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.service.mapstruct;

import cn.hutool.core.collection.CollUtil;
import me.zhengjie.base.BaseMapper;
import me.liangjun.baselinetreatment.domain.ThymicSurgeryHistory;
import me.liangjun.baselinetreatment.service.dto.ThymicSurgeryHistoryDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import java.util.Arrays;
import java.util.List;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-13
**/
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ThymicSurgeryHistoryMapper extends BaseMapper<ThymicSurgeryHistoryDto, ThymicSurgeryHistory> {
    @Override
    @Mappings({
            @Mapping(source = "immunosuppressive",target = "immunosuppressive"),
    })
    ThymicSurgeryHistoryDto toDto(ThymicSurgeryHistory entity);

    @Override
    @Mappings({
            @Mapping(source = "immunosuppressive",target = "immunosuppressive"),
    })
    ThymicSurgeryHistory toEntity(ThymicSurgeryHistoryDto dto);

    default List<String> str2List(String src){
        String[] split = src.split(",");
        List<String> result = Arrays.asList(split);
        return result;
    }

    // list转str
    default String list2Str(List<String> src){
        if (CollUtil.isEmpty(src)) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        src.stream().forEach(item -> sb.append(item.replace("\"","")).append(","));
        return sb.substring(0, sb.length() - 1).toString();
    }
}