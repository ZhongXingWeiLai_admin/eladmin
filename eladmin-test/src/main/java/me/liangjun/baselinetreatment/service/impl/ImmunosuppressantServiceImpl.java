/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.service.impl;

import me.liangjun.baselinetreatment.domain.Immunosuppressant;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.baselinetreatment.repository.ImmunosuppressantRepository;
import me.liangjun.baselinetreatment.service.ImmunosuppressantService;
import me.liangjun.baselinetreatment.service.dto.ImmunosuppressantDto;
import me.liangjun.baselinetreatment.service.dto.ImmunosuppressantQueryCriteria;
import me.liangjun.baselinetreatment.service.mapstruct.ImmunosuppressantMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-10-26
**/
@Service
@RequiredArgsConstructor
public class ImmunosuppressantServiceImpl implements ImmunosuppressantService {

    private final ImmunosuppressantRepository immunosuppressantRepository;
    private final ImmunosuppressantMapper immunosuppressantMapper;

    @Override
    public Map<String,Object> queryAll(ImmunosuppressantQueryCriteria criteria, Pageable pageable){
        Page<Immunosuppressant> page = immunosuppressantRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(immunosuppressantMapper::toDto));
    }

    @Override
    public List<ImmunosuppressantDto> queryAll(ImmunosuppressantQueryCriteria criteria){
        return immunosuppressantMapper.toDto(immunosuppressantRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public ImmunosuppressantDto findById(Long id) {
        Immunosuppressant immunosuppressant = immunosuppressantRepository.findById(id).orElseGet(Immunosuppressant::new);
        ValidationUtil.isNull(immunosuppressant.getId(),"Immunosuppressant","id",id);
        return immunosuppressantMapper.toDto(immunosuppressant);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ImmunosuppressantDto create(Immunosuppressant resources) {
        return immunosuppressantMapper.toDto(immunosuppressantRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(Immunosuppressant resources) {
        Immunosuppressant immunosuppressant = immunosuppressantRepository.findById(resources.getId()).orElseGet(Immunosuppressant::new);
        ValidationUtil.isNull( immunosuppressant.getId(),"Immunosuppressant","id",resources.getId());
        immunosuppressant.copy(resources);
        immunosuppressantRepository.save(immunosuppressant);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            immunosuppressantRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<ImmunosuppressantDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (ImmunosuppressantDto immunosuppressant : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", immunosuppressant.getPId());
            map.put("剂量", immunosuppressant.getDose());
            map.put("术前使用时长", immunosuppressant.getDuration());
            map.put("抑制剂名字", immunosuppressant.getDName());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public List<ImmunosuppressantDto> findByPid(Long pid) {
        return immunosuppressantMapper.toDto(immunosuppressantRepository.findByPid(pid));
    }
}