package me.liangjun.baselinetreatment.service.dto;

import lombok.Data;
import me.liangjun.baselinetreatmentdetails.service.dto.BaselineTreatmentDetailsDto;

import java.io.Serializable;
import java.util.List;

@Data
public class BaseLineTreatmentFDto implements Serializable {
    private List<BaselineTreatmentDetailsDto> drug;
    private BaselineTreatmentDto baselineTreatment;
    private MgImmuneTreatmentDto mgImmuneTreatment;
    private ThymicSurgeryHistoryDto thymicSurgeryHistory;
    private OtherTreatmentDto otherTreatment;
    private List<ImmunosuppressantDto> immunosuppressive;

}
