/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.service.impl;

import me.liangjun.baselinetreatment.domain.ThymicSurgeryHistory;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.baselinetreatment.repository.ThymicSurgeryHistoryRepository;
import me.liangjun.baselinetreatment.service.ThymicSurgeryHistoryService;
import me.liangjun.baselinetreatment.service.dto.ThymicSurgeryHistoryDto;
import me.liangjun.baselinetreatment.service.dto.ThymicSurgeryHistoryQueryCriteria;
import me.liangjun.baselinetreatment.service.mapstruct.ThymicSurgeryHistoryMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-13
**/
@Service
@RequiredArgsConstructor
public class ThymicSurgeryHistoryServiceImpl implements ThymicSurgeryHistoryService {

    private final ThymicSurgeryHistoryRepository thymicSurgeryHistoryRepository;
    private final ThymicSurgeryHistoryMapper thymicSurgeryHistoryMapper;

    @Override
    public Map<String,Object> queryAll(ThymicSurgeryHistoryQueryCriteria criteria, Pageable pageable){
        Page<ThymicSurgeryHistory> page = thymicSurgeryHistoryRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(thymicSurgeryHistoryMapper::toDto));
    }

    @Override
    public List<ThymicSurgeryHistoryDto> queryAll(ThymicSurgeryHistoryQueryCriteria criteria){
        return thymicSurgeryHistoryMapper.toDto(thymicSurgeryHistoryRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public ThymicSurgeryHistoryDto findById(Long id) {
        ThymicSurgeryHistory thymicSurgeryHistory = thymicSurgeryHistoryRepository.findById(id).orElseGet(ThymicSurgeryHistory::new);
        ValidationUtil.isNull(thymicSurgeryHistory.getId(),"ThymicSurgeryHistory","id",id);
        return thymicSurgeryHistoryMapper.toDto(thymicSurgeryHistory);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ThymicSurgeryHistoryDto create(ThymicSurgeryHistory resources) {
        return thymicSurgeryHistoryMapper.toDto(thymicSurgeryHistoryRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(ThymicSurgeryHistory resources) {
        ThymicSurgeryHistory thymicSurgeryHistory = thymicSurgeryHistoryRepository.findById(resources.getId()).orElseGet(ThymicSurgeryHistory::new);
        ValidationUtil.isNull( thymicSurgeryHistory.getId(),"ThymicSurgeryHistory","id",resources.getId());
        thymicSurgeryHistory.copy(resources);
        thymicSurgeryHistoryRepository.save(thymicSurgeryHistory);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            thymicSurgeryHistoryRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<ThymicSurgeryHistoryDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (ThymicSurgeryHistoryDto thymicSurgeryHistory : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", thymicSurgeryHistory.getPId());
            map.put("胸腺手术时间", thymicSurgeryHistory.getThymicSurgeryTime());
            map.put("与首发症状的时间间隔", thymicSurgeryHistory.getTimeInterval());
            map.put("术前肌无力危象", thymicSurgeryHistory.getCrisis());
            map.put("危象次数", thymicSurgeryHistory.getCrisisTimes());
            map.put("术前MGFA分型(术前1周)", thymicSurgeryHistory.getMgfa());
            map.put("有无球部肌肉受累", thymicSurgeryHistory.getSphericalMuscle());
            map.put("术前用药（1月内）", thymicSurgeryHistory.getPreoperativeMedication());
            map.put("用药种类", thymicSurgeryHistory.getMedicationType());
            map.put("溴吡斯的明日剂量", thymicSurgeryHistory.getDose());
            map.put("溴吡斯的明术前使用时间", thymicSurgeryHistory.getPreoperativeUseTime());
            map.put("免疫抑制剂", thymicSurgeryHistory.getImmunosuppressive());
            map.put("术前丙球冲击（1月内）", thymicSurgeryHistory.getBingQiu());
            map.put("术前激素冲击（1月内）", thymicSurgeryHistory.getHormone());
            map.put("术前血浆置换（1月内）", thymicSurgeryHistory.getPlasma());
            map.put("术前肺功", thymicSurgeryHistory.getFeiGong());
            map.put("FVC", thymicSurgeryHistory.getFcv());
            map.put("FEV1", thymicSurgeryHistory.getFev1());
            map.put("术前肌无力症状", thymicSurgeryHistory.getMuscleWeakness());
            map.put("手术方式", thymicSurgeryHistory.getSurgicalApproach());
            map.put("剑突下肋缘三孔手术切除:", thymicSurgeryHistory.getSurgery());
            map.put("切除范围", thymicSurgeryHistory.getScope());
            map.put("手术时长", thymicSurgeryHistory.getSurgicalDuration());
            map.put("术中失血量（ml）", thymicSurgeryHistory.getLoseWeight());
            map.put("术后肌无力危象", thymicSurgeryHistory.getMuscleWeaknessAfter());
            map.put("术后并发症", thymicSurgeryHistory.getComplication());
            map.put("术后病理", thymicSurgeryHistory.getPathology());
            map.put("图片", thymicSurgeryHistory.getPicture());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public ThymicSurgeryHistoryDto findByPid(Long pid) {
        return thymicSurgeryHistoryMapper.toDto(thymicSurgeryHistoryRepository.findByPid(pid));
    }
}