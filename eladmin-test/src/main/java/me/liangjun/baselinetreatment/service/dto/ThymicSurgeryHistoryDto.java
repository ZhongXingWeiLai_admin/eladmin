/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;
import java.util.List;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-13
**/
@Data
public class ThymicSurgeryHistoryDto implements Serializable {

    /** ID */
    private Long id;

    /** pID */
    private Long pId;

    /** 胸腺手术时间 */
    private Timestamp thymicSurgeryTime;

    /** 与首发症状的时间间隔 */
    private String timeInterval;

    /** 术前肌无力危象 */
    private Integer crisis;

    /** 危象次数 */
    private String crisisTimes;

    /** 术前MGFA分型(术前1周) */
    private String mgfa;

    /** 有无球部肌肉受累 */
    private Integer sphericalMuscle;

    /** 术前用药（1月内） */
    private Integer preoperativeMedication;

    /** 用药种类 */
    private String medicationType;

    /** 溴吡斯的明日剂量 */
    private String dose;

    /** 溴吡斯的明术前使用时间 */
    private Timestamp preoperativeUseTime;

    /** 免疫抑制剂 */
    private List<String> immunosuppressive;

    /** 术前丙球冲击（1月内） */
    private Integer bingQiu;

    /** 术前激素冲击（1月内） */
    private Integer hormone;

    /** 术前血浆置换（1月内） */
    private Integer plasma;

    /** 术前肺功 */
    private Integer feiGong;

    /** FVC */
    private String fcv;

    /** FEV1 */
    private String fev1;

    /** 术前肌无力症状 */
    private Integer muscleWeakness;

    /** 手术方式 */
    private Integer surgicalApproach;

    /** 剑突下肋缘三孔手术切除: */
    private Integer surgery;

    /** 切除范围 */
    private Integer scope;

    /** 手术时长 */
    private String surgicalDuration;

    /** 术中失血量（ml） */
    private String loseWeight;

    /** 术后肌无力危象 */
    private Integer muscleWeaknessAfter;

    /** 术后并发症 */
    private String complication;

    /** 术后病理 */
    private String pathology;

    /** 图片 */
    private String picture;
}