/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.service.impl;

import me.liangjun.baselinetreatment.domain.OtherTreatment;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.baselinetreatment.repository.OtherTreatmentRepository;
import me.liangjun.baselinetreatment.service.OtherTreatmentService;
import me.liangjun.baselinetreatment.service.dto.OtherTreatmentDto;
import me.liangjun.baselinetreatment.service.dto.OtherTreatmentQueryCriteria;
import me.liangjun.baselinetreatment.service.mapstruct.OtherTreatmentMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-10-20
**/
@Service
@RequiredArgsConstructor
public class OtherTreatmentServiceImpl implements OtherTreatmentService {

    private final OtherTreatmentRepository otherTreatmentRepository;
    private final OtherTreatmentMapper otherTreatmentMapper;

    @Override
    public Map<String,Object> queryAll(OtherTreatmentQueryCriteria criteria, Pageable pageable){
        Page<OtherTreatment> page = otherTreatmentRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(otherTreatmentMapper::toDto));
    }

    @Override
    public List<OtherTreatmentDto> queryAll(OtherTreatmentQueryCriteria criteria){
        return otherTreatmentMapper.toDto(otherTreatmentRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public OtherTreatmentDto findById(Long id) {
        OtherTreatment otherTreatment = otherTreatmentRepository.findById(id).orElseGet(OtherTreatment::new);
        ValidationUtil.isNull(otherTreatment.getId(),"OtherTreatment","id",id);
        return otherTreatmentMapper.toDto(otherTreatment);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public OtherTreatmentDto create(OtherTreatment resources) {
        return otherTreatmentMapper.toDto(otherTreatmentRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(OtherTreatment resources) {
        OtherTreatment otherTreatment = otherTreatmentRepository.findById(resources.getId()).orElseGet(OtherTreatment::new);
        ValidationUtil.isNull( otherTreatment.getId(),"OtherTreatment","id",resources.getId());
        otherTreatment.copy(resources);
        otherTreatmentRepository.save(otherTreatment);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            otherTreatmentRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<OtherTreatmentDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (OtherTreatmentDto otherTreatment : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", otherTreatment.getPId());
            map.put("其他治疗情况", otherTreatment.getOtherTreatmentInfo());
            map.put("其他治疗起效时间", otherTreatment.getOtherTreatmentDate());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public OtherTreatmentDto findByPid(Long pid) {
        return otherTreatmentMapper.toDto(otherTreatmentRepository.findByPid(pid));
    }
}