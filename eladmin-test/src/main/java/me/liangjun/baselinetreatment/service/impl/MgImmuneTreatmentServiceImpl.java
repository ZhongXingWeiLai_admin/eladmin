/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.service.impl;

import me.liangjun.baselinetreatment.domain.MgImmuneTreatment;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.baselinetreatment.repository.MgImmuneTreatmentRepository;
import me.liangjun.baselinetreatment.service.MgImmuneTreatmentService;
import me.liangjun.baselinetreatment.service.dto.MgImmuneTreatmentDto;
import me.liangjun.baselinetreatment.service.dto.MgImmuneTreatmentQueryCriteria;
import me.liangjun.baselinetreatment.service.mapstruct.MgImmuneTreatmentMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-10-20
**/
@Service
@RequiredArgsConstructor
public class MgImmuneTreatmentServiceImpl implements MgImmuneTreatmentService {

    private final MgImmuneTreatmentRepository mgImmuneTreatmentRepository;
    private final MgImmuneTreatmentMapper mgImmuneTreatmentMapper;

    @Override
    public Map<String,Object> queryAll(MgImmuneTreatmentQueryCriteria criteria, Pageable pageable){
        Page<MgImmuneTreatment> page = mgImmuneTreatmentRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(mgImmuneTreatmentMapper::toDto));
    }

    @Override
    public List<MgImmuneTreatmentDto> queryAll(MgImmuneTreatmentQueryCriteria criteria){
        return mgImmuneTreatmentMapper.toDto(mgImmuneTreatmentRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public MgImmuneTreatmentDto findById(Long id) {
        MgImmuneTreatment mgImmuneTreatment = mgImmuneTreatmentRepository.findById(id).orElseGet(MgImmuneTreatment::new);
        ValidationUtil.isNull(mgImmuneTreatment.getId(),"MgImmuneTreatment","id",id);
        return mgImmuneTreatmentMapper.toDto(mgImmuneTreatment);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public MgImmuneTreatmentDto create(MgImmuneTreatment resources) {
        return mgImmuneTreatmentMapper.toDto(mgImmuneTreatmentRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(MgImmuneTreatment resources) {
        MgImmuneTreatment mgImmuneTreatment = mgImmuneTreatmentRepository.findById(resources.getId()).orElseGet(MgImmuneTreatment::new);
        ValidationUtil.isNull( mgImmuneTreatment.getId(),"MgImmuneTreatment","id",resources.getId());
        mgImmuneTreatment.copy(resources);
        mgImmuneTreatmentRepository.save(mgImmuneTreatment);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            mgImmuneTreatmentRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<MgImmuneTreatmentDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (MgImmuneTreatmentDto mgImmuneTreatment : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", mgImmuneTreatment.getPId());
            map.put("丙种球蛋白_使用日期", mgImmuneTreatment.getGlycopathyUseDate());
            map.put("丙种球蛋白_使用剂量", mgImmuneTreatment.getGlycopathyDose());
            map.put("丙种球蛋白_起效时间", mgImmuneTreatment.getGlycopathyEffectiveTime());
            map.put("丙种球蛋白_使用效果", mgImmuneTreatment.getGlycopathyUseEffect());
            map.put("血浆置换日期", mgImmuneTreatment.getPlasmaReplacementDate());
            map.put("血浆置换_起效时间", mgImmuneTreatment.getPlasmaReplacementTime());
            map.put("血浆置换内容", mgImmuneTreatment.getPlasmaReplacement());
            map.put("免疫吸附日期", mgImmuneTreatment.getImmuneAdsorptionDate());
            map.put("免疫吸附_起效时间", mgImmuneTreatment.getImmuneAdsorptionEffectiveTime());
            map.put("免疫吸附内容", mgImmuneTreatment.getImmuneAdsorptionContent());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public MgImmuneTreatmentDto findByPid(Long pid) {
        return mgImmuneTreatmentMapper.toDto(mgImmuneTreatmentRepository.findByPid(pid));
    }
}