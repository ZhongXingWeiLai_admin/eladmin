/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.baselinetreatment.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-10-20
**/
@Data
public class MgImmuneTreatmentDto implements Serializable {

    /** ID */
    private Long id;

    /** pID */
    private Long pId;

    /** 丙种球蛋白_使用日期 */
    private Timestamp glycopathyUseDate;

    /** 丙种球蛋白_使用剂量 */
    private String glycopathyDose;

    /** 丙种球蛋白_起效时间 */
    private Timestamp glycopathyEffectiveTime;

    /** 丙种球蛋白_使用效果 */
    private String glycopathyUseEffect;

    /** 血浆置换日期 */
    private Timestamp plasmaReplacementDate;

    /** 血浆置换_起效时间 */
    private Timestamp plasmaReplacementTime;

    /** 血浆置换内容 */
    private String plasmaReplacement;

    /** 免疫吸附日期 */
    private Timestamp immuneAdsorptionDate;

    /** 免疫吸附_起效时间 */
    private Timestamp immuneAdsorptionEffectiveTime;

    /** 免疫吸附内容 */
    private String immuneAdsorptionContent;
}