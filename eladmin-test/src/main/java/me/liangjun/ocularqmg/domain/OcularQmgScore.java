/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.ocularqmg.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-04
**/
@Entity
@Data
@Table(name="ocular_qmg_score")
public class OcularQmgScore implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "ocular_qmg1")
    @ApiModelProperty(value = "上睑下垂")
    private Integer ocularQmg1;

    @Column(name = "ocular_qmg2")
    @ApiModelProperty(value = "左凝视")
    private Integer ocularQmg2;

    @Column(name = "ocular_qmg3")
    @ApiModelProperty(value = "右凝视")
    private Integer ocularQmg3;

    @Column(name = "ocular_qmg4")
    @ApiModelProperty(value = "上凝视")
    private Integer ocularQmg4;

    @Column(name = "ocular_qmg5")
    @ApiModelProperty(value = "下凝视")
    private Integer ocularQmg5;

    @Column(name = "ocular_qmg6")
    @ApiModelProperty(value = "眼睑闭合")
    private Integer ocularQmg6;

    public void copy(OcularQmgScore source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}