/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.ocularqmg.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.ocularqmg.domain.OcularQmgScore;
import me.liangjun.ocularqmg.service.OcularQmgScoreService;
import me.liangjun.ocularqmg.service.dto.OcularQmgScoreQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-04
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "ocularqmg管理")
@RequestMapping("/api/ocularQmgScore")
public class OcularQmgScoreController {

    private final OcularQmgScoreService ocularQmgScoreService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('ocularQmgScore:list')")
    public void download(HttpServletResponse response, OcularQmgScoreQueryCriteria criteria) throws IOException {
        ocularQmgScoreService.download(ocularQmgScoreService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询ocularqmg")
    @ApiOperation("查询ocularqmg")
    @PreAuthorize("@el.check('ocularQmgScore:list')")
    public ResponseEntity<Object> query(OcularQmgScoreQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(ocularQmgScoreService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增ocularqmg")
    @ApiOperation("新增ocularqmg")
    @PreAuthorize("@el.check('ocularQmgScore:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody OcularQmgScore resources){
        return new ResponseEntity<>(ocularQmgScoreService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改ocularqmg")
    @ApiOperation("修改ocularqmg")
    @PreAuthorize("@el.check('ocularQmgScore:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody OcularQmgScore resources){
        ocularQmgScoreService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除ocularqmg")
    @ApiOperation("删除ocularqmg")
    @PreAuthorize("@el.check('ocularQmgScore:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        ocularQmgScoreService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}