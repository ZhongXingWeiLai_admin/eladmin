/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.ocularqmg.service.impl;

import me.liangjun.ocularqmg.domain.OcularQmgScore;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.ocularqmg.repository.OcularQmgScoreRepository;
import me.liangjun.ocularqmg.service.OcularQmgScoreService;
import me.liangjun.ocularqmg.service.dto.OcularQmgScoreDto;
import me.liangjun.ocularqmg.service.dto.OcularQmgScoreQueryCriteria;
import me.liangjun.ocularqmg.service.mapstruct.OcularQmgScoreMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-04
**/
@Service
@RequiredArgsConstructor
public class OcularQmgScoreServiceImpl implements OcularQmgScoreService {

    private final OcularQmgScoreRepository ocularQmgScoreRepository;
    private final OcularQmgScoreMapper ocularQmgScoreMapper;

    @Override
    public Map<String,Object> queryAll(OcularQmgScoreQueryCriteria criteria, Pageable pageable){
        Page<OcularQmgScore> page = ocularQmgScoreRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(ocularQmgScoreMapper::toDto));
    }

    @Override
    public List<OcularQmgScoreDto> queryAll(OcularQmgScoreQueryCriteria criteria){
        return ocularQmgScoreMapper.toDto(ocularQmgScoreRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public OcularQmgScoreDto findById(Long id) {
        OcularQmgScore ocularQmgScore = ocularQmgScoreRepository.findById(id).orElseGet(OcularQmgScore::new);
        ValidationUtil.isNull(ocularQmgScore.getId(),"OcularQmgScore","id",id);
        return ocularQmgScoreMapper.toDto(ocularQmgScore);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public OcularQmgScoreDto create(OcularQmgScore resources) {
        return ocularQmgScoreMapper.toDto(ocularQmgScoreRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(OcularQmgScore resources) {
        OcularQmgScore ocularQmgScore = ocularQmgScoreRepository.findById(resources.getId()).orElseGet(OcularQmgScore::new);
        ValidationUtil.isNull( ocularQmgScore.getId(),"OcularQmgScore","id",resources.getId());
        ocularQmgScore.copy(resources);
        ocularQmgScoreRepository.save(ocularQmgScore);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            ocularQmgScoreRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<OcularQmgScoreDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (OcularQmgScoreDto ocularQmgScore : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", ocularQmgScore.getPId());
            map.put("上睑下垂", ocularQmgScore.getOcularQmg1());
            map.put("左凝视", ocularQmgScore.getOcularQmg2());
            map.put("右凝视", ocularQmgScore.getOcularQmg3());
            map.put("上凝视", ocularQmgScore.getOcularQmg4());
            map.put("下凝视", ocularQmgScore.getOcularQmg5());
            map.put("眼睑闭合", ocularQmgScore.getOcularQmg6());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public OcularQmgScoreDto findByPid(Long pid) {
        return ocularQmgScoreMapper.toDto(ocularQmgScoreRepository.findByPid(pid));
    }
}