/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.mgqol.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.mgqol.domain.MgQolScore;
import me.liangjun.mgqol.service.MgQolScoreService;
import me.liangjun.mgqol.service.dto.MgQolScoreQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-04
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "mgqol管理")
@RequestMapping("/api/mgQolScore")
public class MgQolScoreController {

    private final MgQolScoreService mgQolScoreService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('mgQolScore:list')")
    public void download(HttpServletResponse response, MgQolScoreQueryCriteria criteria) throws IOException {
        mgQolScoreService.download(mgQolScoreService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询mgqol")
    @ApiOperation("查询mgqol")
    @PreAuthorize("@el.check('mgQolScore:list')")
    public ResponseEntity<Object> query(MgQolScoreQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(mgQolScoreService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增mgqol")
    @ApiOperation("新增mgqol")
    @PreAuthorize("@el.check('mgQolScore:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody MgQolScore resources){
        return new ResponseEntity<>(mgQolScoreService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改mgqol")
    @ApiOperation("修改mgqol")
    @PreAuthorize("@el.check('mgQolScore:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody MgQolScore resources){
        mgQolScoreService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除mgqol")
    @ApiOperation("删除mgqol")
    @PreAuthorize("@el.check('mgQolScore:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        mgQolScoreService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}