/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.mgqol.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-04
**/
@Entity
@Data
@Table(name="mg_qol_score")
public class MgQolScore implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "mg_qol1")
    @ApiModelProperty(value = "我对自己的病情有挫折感")
    private Integer mgQol1;

    @Column(name = "mg_qol2")
    @ApiModelProperty(value = "我用眼有困难")
    private Integer mgQol2;

    @Column(name = "mg_qol3")
    @ApiModelProperty(value = "我吃东西有困难")
    private Integer mgQol3;

    @Column(name = "mg_qol4")
    @ApiModelProperty(value = "因为病情而导致我社交活动受限")
    private Integer mgQol4;

    @Column(name = "mg_qol5")
    @ApiModelProperty(value = "我的病情限制了我的兴趣爱好活动")
    private Integer mgQol5;

    @Column(name = "mg_qol6")
    @ApiModelProperty(value = "我不能很好地满足家庭需要")
    private Integer mgQol6;

    @Column(name = "mg_qol7")
    @ApiModelProperty(value = "我需要根据我的身体情况定制计划")
    private Integer mgQol7;

    @Column(name = "mg_qol8")
    @ApiModelProperty(value = "我的职业技能和工作状态受到了影响")
    private Integer mgQol8;

    @Column(name = "mg_qol9")
    @ApiModelProperty(value = "我说话有困难")
    private Integer mgQol9;

    @Column(name = "mg_qol10")
    @ApiModelProperty(value = "我骑车、开车有困难")
    private Integer mgQol10;

    @Column(name = "mg_qol11")
    @ApiModelProperty(value = "我对我的病情感到抑郁")
    private Integer mgQol11;

    @Column(name = "mg_qol12")
    @ApiModelProperty(value = "我行走有困难")
    private Integer mgQol12;

    @Column(name = "mg_qol13")
    @ApiModelProperty(value = "我对我的病情感到悲哀、悲痛、绝望")
    private Integer mgQol13;

    @Column(name = "mg_qol14")
    @ApiModelProperty(value = "我在个人打扮、穿着方面有困难、悲痛、绝望")
    private Integer mgQol14;

    public void copy(MgQolScore source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}