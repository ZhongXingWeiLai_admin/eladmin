/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.mgqol.service.impl;

import me.liangjun.mgqol.domain.MgQolScore;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.mgqol.repository.MgQolScoreRepository;
import me.liangjun.mgqol.service.MgQolScoreService;
import me.liangjun.mgqol.service.dto.MgQolScoreDto;
import me.liangjun.mgqol.service.dto.MgQolScoreQueryCriteria;
import me.liangjun.mgqol.service.mapstruct.MgQolScoreMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-04
**/
@Service
@RequiredArgsConstructor
public class MgQolScoreServiceImpl implements MgQolScoreService {

    private final MgQolScoreRepository mgQolScoreRepository;
    private final MgQolScoreMapper mgQolScoreMapper;

    @Override
    public Map<String,Object> queryAll(MgQolScoreQueryCriteria criteria, Pageable pageable){
        Page<MgQolScore> page = mgQolScoreRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(mgQolScoreMapper::toDto));
    }

    @Override
    public List<MgQolScoreDto> queryAll(MgQolScoreQueryCriteria criteria){
        return mgQolScoreMapper.toDto(mgQolScoreRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public MgQolScoreDto findById(Long id) {
        MgQolScore mgQolScore = mgQolScoreRepository.findById(id).orElseGet(MgQolScore::new);
        ValidationUtil.isNull(mgQolScore.getId(),"MgQolScore","id",id);
        return mgQolScoreMapper.toDto(mgQolScore);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public MgQolScoreDto create(MgQolScore resources) {
        return mgQolScoreMapper.toDto(mgQolScoreRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(MgQolScore resources) {
        MgQolScore mgQolScore = mgQolScoreRepository.findById(resources.getId()).orElseGet(MgQolScore::new);
        ValidationUtil.isNull( mgQolScore.getId(),"MgQolScore","id",resources.getId());
        mgQolScore.copy(resources);
        mgQolScoreRepository.save(mgQolScore);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            mgQolScoreRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<MgQolScoreDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (MgQolScoreDto mgQolScore : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", mgQolScore.getPId());
            map.put("我对自己的病情有挫折感", mgQolScore.getMgQol1());
            map.put("我用眼有困难", mgQolScore.getMgQol2());
            map.put("我吃东西有困难", mgQolScore.getMgQol3());
            map.put("因为病情而导致我社交活动受限", mgQolScore.getMgQol4());
            map.put("我的病情限制了我的兴趣爱好活动", mgQolScore.getMgQol5());
            map.put("我不能很好地满足家庭需要", mgQolScore.getMgQol6());
            map.put("我需要根据我的身体情况定制计划", mgQolScore.getMgQol7());
            map.put("我的职业技能和工作状态受到了影响", mgQolScore.getMgQol8());
            map.put("我说话有困难", mgQolScore.getMgQol9());
            map.put("我骑车、开车有困难", mgQolScore.getMgQol10());
            map.put("我对我的病情感到抑郁", mgQolScore.getMgQol11());
            map.put("我行走有困难", mgQolScore.getMgQol12());
            map.put("我对我的病情感到悲哀、悲痛、绝望", mgQolScore.getMgQol13());
            map.put("我在个人打扮、穿着方面有困难、悲痛、绝望", mgQolScore.getMgQol14());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public MgQolScoreDto findByPid(Long pid) {
        return mgQolScoreMapper.toDto(mgQolScoreRepository.findByPid(pid));
    }
}