/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.mgqol.service.dto;

import lombok.Data;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-04
**/
@Data
public class MgQolScoreDto implements Serializable {

    /** ID */
    private Long id;

    /** pID */
    private Long pId;

    /** 我对自己的病情有挫折感 */
    private Integer mgQol1;

    /** 我用眼有困难 */
    private Integer mgQol2;

    /** 我吃东西有困难 */
    private Integer mgQol3;

    /** 因为病情而导致我社交活动受限 */
    private Integer mgQol4;

    /** 我的病情限制了我的兴趣爱好活动 */
    private Integer mgQol5;

    /** 我不能很好地满足家庭需要 */
    private Integer mgQol6;

    /** 我需要根据我的身体情况定制计划 */
    private Integer mgQol7;

    /** 我的职业技能和工作状态受到了影响 */
    private Integer mgQol8;

    /** 我说话有困难 */
    private Integer mgQol9;

    /** 我骑车、开车有困难 */
    private Integer mgQol10;

    /** 我对我的病情感到抑郁 */
    private Integer mgQol11;

    /** 我行走有困难 */
    private Integer mgQol12;

    /** 我对我的病情感到悲哀、悲痛、绝望 */
    private Integer mgQol13;

    /** 我在个人打扮、穿着方面有困难、悲痛、绝望 */
    private Integer mgQol14;
}