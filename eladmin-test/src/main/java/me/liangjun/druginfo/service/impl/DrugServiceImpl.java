/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.druginfo.service.impl;

import me.liangjun.druginfo.domain.Drug;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.druginfo.repository.DrugRepository;
import me.liangjun.druginfo.service.DrugService;
import me.liangjun.druginfo.service.dto.DrugDto;
import me.liangjun.druginfo.service.dto.DrugQueryCriteria;
import me.liangjun.druginfo.service.mapstruct.DrugMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-04
**/
@Service
@RequiredArgsConstructor
public class DrugServiceImpl implements DrugService {

    private final DrugRepository drugRepository;
    private final DrugMapper drugMapper;

    @Override
    public Map<String,Object> queryAll(DrugQueryCriteria criteria, Pageable pageable){
        Page<Drug> page = drugRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(drugMapper::toDto));
    }

    @Override
    public List<DrugDto> queryAll(DrugQueryCriteria criteria){
        return drugMapper.toDto(drugRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public DrugDto findById(Long id) {
        Drug drug = drugRepository.findById(id).orElseGet(Drug::new);
        ValidationUtil.isNull(drug.getId(),"Drug","id",id);
        return drugMapper.toDto(drug);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public DrugDto create(Drug resources) {
        return drugMapper.toDto(drugRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(Drug resources) {
        Drug drug = drugRepository.findById(resources.getId()).orElseGet(Drug::new);
        ValidationUtil.isNull( drug.getId(),"Drug","id",resources.getId());
        drug.copy(resources);
        drugRepository.save(drug);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            drugRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<DrugDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (DrugDto drug : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("其他治疗情况", drug.getName());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}