/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.drug.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.drug.domain.DrugDetails;
import me.liangjun.drug.service.DrugDetailsService;
import me.liangjun.drug.service.dto.DrugDetailsQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-08
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "drug管理")
@RequestMapping("/api/drugDetails")
public class DrugDetailsController {

    private final DrugDetailsService drugDetailsService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('drugDetails:list')")
    public void download(HttpServletResponse response, DrugDetailsQueryCriteria criteria) throws IOException {
        drugDetailsService.download(drugDetailsService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询drug")
    @ApiOperation("查询drug")
    @PreAuthorize("@el.check('drugDetails:list')")
    public ResponseEntity<Object> query(DrugDetailsQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(drugDetailsService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增drug")
    @ApiOperation("新增drug")
    @PreAuthorize("@el.check('drugDetails:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody DrugDetails resources){
        return new ResponseEntity<>(drugDetailsService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改drug")
    @ApiOperation("修改drug")
    @PreAuthorize("@el.check('drugDetails:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody DrugDetails resources){
        drugDetailsService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除drug")
    @ApiOperation("删除drug")
    @PreAuthorize("@el.check('drugDetails:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        drugDetailsService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}