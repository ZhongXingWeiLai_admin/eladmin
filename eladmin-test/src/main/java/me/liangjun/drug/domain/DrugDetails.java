/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.drug.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-08
**/
@Entity
@Data
@Table(name="drug_details")
public class DrugDetails implements Serializable {

    @Column(name = "p_id")
    @ApiModelProperty(value = "ID")
    private Long pId;

    @Column(name = "start_date")
    @ApiModelProperty(value = "开始用药时间")
    private Timestamp startDate;

    @Column(name = "end_date")
    @ApiModelProperty(value = "结束用药时间")
    private Timestamp endDate;

    @Column(name = "days")
    @ApiModelProperty(value = "用药天数")
    private String days;

    @Column(name = "use_method")
    @ApiModelProperty(value = "用法")
    private String useMethod;

    @Column(name = "onset_date")
    @ApiModelProperty(value = "起效时间")
    private Timestamp onsetDate;

    @Column(name = "side_effect")
    @ApiModelProperty(value = "副作用")
    private String sideEffect;

    @Column(name = "stop_state")
    @ApiModelProperty(value = "是否停药")
    private String stopState;

    @Column(name = "reason")
    @ApiModelProperty(value = "停药原因")
    private String reason;

    @Column(name = "aggravate_state")
    @ApiModelProperty(value = "是否-过性加重")
    private String aggravateState;

    @Column(name = "treatment_details")
    @ApiModelProperty(value = "治疗情况")
    private String treatmentDetails;

    @Column(name = "gene")
    @ApiModelProperty(value = "基因")
    private String gene;

    @Column(name = "suppression_position")
    @ApiModelProperty(value = "抑制位置")
    private String suppressionPosition;

    @Column(name = "genotype")
    @ApiModelProperty(value = "基因型")
    private String genotype;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "d_id")
    @ApiModelProperty(value = "药编号")
    private Long dId;

    @Column(name = "take")
    @ApiModelProperty(value = "每次服用（粒）")
    private String take;

    @Column(name = "dose")
    @ApiModelProperty(value = "每次剂量（mg）")
    private String dose;

    @Column(name = "dose_today")
    @ApiModelProperty(value = "当日服药剂量（mg）")
    private String doseToday;

    @Column(name = "dose_total")
    @ApiModelProperty(value = "目前总服药剂量")
    private String doseTotal;

    @Column(name = "improve")
    @ApiModelProperty(value = "服药后症状改善情况/使用效果/使用时长")
    private String improve;

    @Column(name = "abnormal_describe")
    @ApiModelProperty(value = "异常指标描述")
    private String abnormalDescribe;

    @Column(name = "side_effect_abnormal_index")
    @ApiModelProperty(value = "副作用异常指标")
    private String sideEffectAbnormalIndex;

    @Column(name = "ta_check")
    @ApiModelProperty(value = "他克莫司相关检查")
    private String taCheck;

    @Column(name = "ta_treatment")
    @ApiModelProperty(value = "他克莫司治疗情况")
    private String taTreatment;

    @Column(name = "ta_concentration")
    @ApiModelProperty(value = "他克莫司（ng/ml）浓度")
    private String taConcentration;

    @Column(name = "ta_detection_date")
    @ApiModelProperty(value = "检测日期")
    private String taDetectionDate;

    @Column(name = "ta_take_wu")
    @ApiModelProperty(value = "服用五酯滴丸")
    private String taTakeWu;

    @Column(name = "ta_take_wu_dose")
    @ApiModelProperty(value = "日用药剂量（mg/kg）五酯滴丸")
    private String taTakeWuDose;

    @Column(name = "ta_take_wu_effective_time")
    @ApiModelProperty(value = "起效时间")
    private String taTakeWuEffectiveTime;

    @Column(name = "ta_take_wu_cd")
    @ApiModelProperty(value = "C/D值五酯滴丸")
    private String taTakeWuCd;

    public void copy(DrugDetails source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}