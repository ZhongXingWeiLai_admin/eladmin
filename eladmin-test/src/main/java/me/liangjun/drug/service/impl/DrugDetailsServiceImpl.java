/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.drug.service.impl;

import me.liangjun.drug.domain.DrugDetails;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.drug.repository.DrugDetailsRepository;
import me.liangjun.drug.service.DrugDetailsService;
import me.liangjun.drug.service.dto.DrugDetailsDto;
import me.liangjun.drug.service.dto.DrugDetailsQueryCriteria;
import me.liangjun.drug.service.mapstruct.DrugDetailsMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-08
**/
@Service
@RequiredArgsConstructor
public class DrugDetailsServiceImpl implements DrugDetailsService {

    private final DrugDetailsRepository drugDetailsRepository;
    private final DrugDetailsMapper drugDetailsMapper;

    @Override
    public Map<String,Object> queryAll(DrugDetailsQueryCriteria criteria, Pageable pageable){
        Page<DrugDetails> page = drugDetailsRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(drugDetailsMapper::toDto));
    }

    @Override
    public List<DrugDetailsDto> queryAll(DrugDetailsQueryCriteria criteria){
        return drugDetailsMapper.toDto(drugDetailsRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public DrugDetailsDto findById(Long id) {
        DrugDetails drugDetails = drugDetailsRepository.findById(id).orElseGet(DrugDetails::new);
        ValidationUtil.isNull(drugDetails.getId(),"DrugDetails","id",id);
        return drugDetailsMapper.toDto(drugDetails);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public DrugDetailsDto create(DrugDetails resources) {
        return drugDetailsMapper.toDto(drugDetailsRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(DrugDetails resources) {
        DrugDetails drugDetails = drugDetailsRepository.findById(resources.getId()).orElseGet(DrugDetails::new);
        ValidationUtil.isNull( drugDetails.getId(),"DrugDetails","id",resources.getId());
        drugDetails.copy(resources);
        drugDetailsRepository.save(drugDetails);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            drugDetailsRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<DrugDetailsDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (DrugDetailsDto drugDetails : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("ID", drugDetails.getPId());
            map.put("开始用药时间", drugDetails.getStartDate());
            map.put("结束用药时间", drugDetails.getEndDate());
            map.put("用药天数", drugDetails.getDays());
            map.put("用法", drugDetails.getUseMethod());
            map.put("起效时间", drugDetails.getOnsetDate());
            map.put("副作用", drugDetails.getSideEffect());
            map.put("是否停药", drugDetails.getStopState());
            map.put("停药原因", drugDetails.getReason());
            map.put("是否-过性加重", drugDetails.getAggravateState());
            map.put("治疗情况", drugDetails.getTreatmentDetails());
            map.put("基因", drugDetails.getGene());
            map.put("抑制位置", drugDetails.getSuppressionPosition());
            map.put("基因型", drugDetails.getGenotype());
            map.put("药编号", drugDetails.getDId());
            map.put("每次服用（粒）", drugDetails.getTake());
            map.put("每次剂量（mg）", drugDetails.getDose());
            map.put("当日服药剂量（mg）", drugDetails.getDoseToday());
            map.put("目前总服药剂量", drugDetails.getDoseTotal());
            map.put("服药后症状改善情况/使用效果", drugDetails.getImprove());
            map.put("异常指标描述", drugDetails.getAbnormalDescribe());
            map.put("副作用异常指标", drugDetails.getSideEffectAbnormalIndex());
            map.put("他克莫司相关检查", drugDetails.getTaCheck());
            map.put("他克莫司治疗情况", drugDetails.getTaTreatment());
            map.put("他克莫司（ng/ml）浓度", drugDetails.getTaConcentration());
            map.put("检测日期", drugDetails.getTaDetectionDate());
            map.put("服用五酯滴丸", drugDetails.getTaTakeWu());
            map.put("日用药剂量（mg/kg）五酯滴丸", drugDetails.getTaTakeWuDose());
            map.put("起效时间", drugDetails.getTaTakeWuEffectiveTime());
            map.put("C/D值五酯滴丸", drugDetails.getTaTakeWuCd());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}