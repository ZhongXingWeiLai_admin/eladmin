/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.drug.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-08
**/
@Data
public class DrugDetailsDto implements Serializable {

    /** ID */
    private Long pId;

    /** 开始用药时间 */
    private Timestamp startDate;

    /** 结束用药时间 */
    private Timestamp endDate;

    /** 用药天数 */
    private String days;

    /** 用法 */
    private String useMethod;

    /** 起效时间 */
    private Timestamp onsetDate;

    /** 副作用 */
    private String sideEffect;

    /** 是否停药 */
    private String stopState;

    /** 停药原因 */
    private String reason;

    /** 是否-过性加重 */
    private String aggravateState;

    /** 治疗情况 */
    private String treatmentDetails;

    /** 基因 */
    private String gene;

    /** 抑制位置 */
    private String suppressionPosition;

    /** 基因型 */
    private String genotype;

    /** ID */
    private Long id;

    /** 药编号 */
    private Long dId;

    /** 每次服用（粒） */
    private String take;

    /** 每次剂量（mg） */
    private String dose;

    /** 当日服药剂量（mg） */
    private String doseToday;

    /** 目前总服药剂量 */
    private String doseTotal;

    /** 服药后症状改善情况/使用效果 */
    private String improve;

    /** 异常指标描述 */
    private String abnormalDescribe;

    /** 副作用异常指标 */
    private String sideEffectAbnormalIndex;

    /** 他克莫司相关检查 */
    private String taCheck;

    /** 他克莫司治疗情况 */
    private String taTreatment;

    /** 他克莫司（ng/ml）浓度 */
    private String taConcentration;

    /** 检测日期 */
    private String taDetectionDate;

    /** 服用五酯滴丸 */
    private String taTakeWu;

    /** 日用药剂量（mg/kg）五酯滴丸 */
    private String taTakeWuDose;

    /** 起效时间 */
    private String taTakeWuEffectiveTime;

    /** C/D值五酯滴丸 */
    private String taTakeWuCd;
}