/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.treatmentbefore.rest;

import me.liangjun.treatmentbefore.service.dto.TreatmentBeforeEnrollmentDto;
import me.zhengjie.annotation.AnonymousAccess;
import me.zhengjie.annotation.Log;
import me.liangjun.treatmentbefore.domain.TreatmentBeforeEnrollment;
import me.liangjun.treatmentbefore.service.TreatmentBeforeEnrollmentService;
import me.liangjun.treatmentbefore.service.dto.TreatmentBeforeEnrollmentQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-04
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "treatmentbefore管理")
@RequestMapping("/api/treatmentBeforeEnrollment")
public class TreatmentBeforeEnrollmentController {

    private final TreatmentBeforeEnrollmentService treatmentBeforeEnrollmentService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('treatmentBeforeEnrollment:list')")
    public void download(HttpServletResponse response, TreatmentBeforeEnrollmentQueryCriteria criteria) throws IOException {
        treatmentBeforeEnrollmentService.download(treatmentBeforeEnrollmentService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询treatmentbefore")
    @ApiOperation("查询treatmentbefore")
    @PreAuthorize("@el.check('treatmentBeforeEnrollment:list')")
    public ResponseEntity<Object> query(TreatmentBeforeEnrollmentQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(treatmentBeforeEnrollmentService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @GetMapping(value = "findByPid")
    @Log("查询treatmentbefore")
    @ApiOperation("查询treatmentbefore")
//    @PreAuthorize("@el.check('treatmentBeforeEnrollment:list')")
    @AnonymousAccess
    public ResponseEntity<Object> findByPid(@RequestParam("pid") Long pid){
        TreatmentBeforeEnrollmentDto treatmentBeforeEnrollmentDto = treatmentBeforeEnrollmentService.findByPid(pid);
        if (treatmentBeforeEnrollmentDto.getId() == null){
            treatmentBeforeEnrollmentDto.setId(0L);
        }
        if (treatmentBeforeEnrollmentDto.getPId() == null){
            treatmentBeforeEnrollmentDto.setPId(0L);
        }
        if (treatmentBeforeEnrollmentDto.getState() == null){
            treatmentBeforeEnrollmentDto.setState(0);
        }
        if (treatmentBeforeEnrollmentDto.getMgTreatment() == null){
            treatmentBeforeEnrollmentDto.setMgTreatment(new ArrayList<>());
        }
        return new ResponseEntity<>(treatmentBeforeEnrollmentDto,HttpStatus.OK);
    }

    @PostMapping
    @Log("新增treatmentbefore")
    @ApiOperation("新增treatmentbefore")
    @PreAuthorize("@el.check('treatmentBeforeEnrollment:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody TreatmentBeforeEnrollmentDto resources){
        return new ResponseEntity<>(treatmentBeforeEnrollmentService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改treatmentbefore")
    @ApiOperation("修改treatmentbefore")
    @PreAuthorize("@el.check('treatmentBeforeEnrollment:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody TreatmentBeforeEnrollment resources){
        treatmentBeforeEnrollmentService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除treatmentbefore")
    @ApiOperation("删除treatmentbefore")
    @PreAuthorize("@el.check('treatmentBeforeEnrollment:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        treatmentBeforeEnrollmentService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}