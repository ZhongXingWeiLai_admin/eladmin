/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.treatmentbefore.service.dto;

import lombok.Data;
import me.liangjun.drug.service.dto.DrugDetailsDto;

import java.io.Serializable;
import java.util.List;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-04
**/
@Data

public class TreatmentBeforeEnrollmentDto implements Serializable {

    private Long id;

    /** ID */
    private Long pId;

    /** 入組前是否接受治療 */
    private Integer state;

    /** 用药 */
    private List<String> mgTreatment;

    private List<DrugDetailsDto> medicals;

}