package me.liangjun.util;

import me.zhengjie.utils.DateUtil;
import org.hibernate.engine.jdbc.SerializableBlobProxy;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Proxy;
import java.sql.Blob;
import java.util.Date;

/**
 * SQL查询结果转换工具类
 */
public abstract class SqlParamUtil {

    public static String getBlankToNULL(String str) {
        if (str == null)
            return null;
        else if (str.length() == 0 || "".equals(str.trim()))
            return null;
        return str;
    }

    /**
     * 获取BLOG字段的值
     * 
     * @param blob
     * @return
     */
    public static byte[] getBlob(Object blob) {
        if (blob == null) {
            return null;
        }
        SerializableBlobProxy proxy = (SerializableBlobProxy ) Proxy.getInvocationHandler(blob);
        if (proxy instanceof SerializableBlobProxy) {
            InputStream input = null;
            ByteArrayOutputStream baos = null;
            try {
                input = ((SerializableBlobProxy) proxy).getWrappedBlob().getBinaryStream();
                baos = new ByteArrayOutputStream();
                byte[] by = new byte[1024];
                int j = 0;
                while ((j = input.read(by)) != -1) {
                    baos.write(by, 0, j);
                }
                return baos.toByteArray();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (input != null)
                        input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (baos != null)
                        baos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        return null;
    }

    public static byte[] blobToBytes(Blob blob) {
        InputStream is = null;
        byte[] b = null;
        try {
            is = blob.getBinaryStream();
            b = new byte[(int) blob.length()];
            is.read(b);
            return b;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
                is = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return b;
    }

    /**
     * 生成唯一KEY
     * 
     * @param k1
     * @param k2
     * @return
     */
    public static String getBuildKey(Object k1, Object k2) {
        return k1.toString() + "_" + k2.toString();
    }

    /**
     * 合并两个字符对象成字符对象中间用c分隔
     * 
     * @param obj1
     * @param obj2
     * @param c
     * @return
     */
    public static String marge(String obj1, String obj2, String c) {
        if (obj1 != null && obj2 != null) {
            if (obj1.endsWith(c)) {
                return obj1 + obj2;
            } else {
                return obj1 + c + obj2;
            }
        } else if (obj1 != null) {
            return obj1;
        } else if (obj2 != null) {
            return obj2;
        }
        return null;
    }

    /**
     * 生成多个唯一查询IN or =
     * 
     * @param size
     * @return
     */
    public static String getEqualOrIn(int size) {
        if (size == 1) {
            return "=?";
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append(" in(");
            for (int i = 0; i < size; i++) {
                sb.append("?,");
            }
            return sb.deleteCharAt(sb.length() - 1).append(")").toString();
        }
    }

    /**
     * 从数据库中查询结果转换成长整型
     * 
     * @param o
     *            数据库中查询结果
     * @return 长整型(如果没结果为null)
     */
    public static long getLong0(Object o) {
        Long retLong = getLong(o);
        if (retLong != null)
            return retLong;
        return 0;
    }

    /**
     * 从数据库中查询结果转换成长整型
     * 
     * @param o
     *            数据库中查询结果
     * @return 长整型(如果没结果为null)
     */
    public static Long getLong(Object o) {
        Long retLong = null;
        if (o == null)
            return retLong;
        else if (o instanceof Number) {
            retLong = ((Number) o).longValue();
        } else {
            throw new ClassCastException(
                    "this object is " + o.getClass() + " not instanceof Long");
        }
        return retLong;
    }

    /**
     * 从数据库中查询结果转换成长整型
     * 
     * @param o
     *            数据库中查询结果
     * @return 长整型(如果没结果为null)
     */
    public static int getInt(Object o) {
        Integer retInteger = getInteger(o);
        if (retInteger != null) {
            return retInteger;
        }
        return 0;
    }

    /**
     * 从数据库中查询结果转换成长整型
     * 
     * @param o
     *            数据库中查询结果
     * @return 长整型(如果没结果为null)
     */
    public static Integer getInteger(Object o) {
        Integer retInteger = null;
        if (o == null)
            return retInteger;
        else if (o instanceof Number) {
            retInteger = ((Number) o).intValue();
        } else {
            throw new ClassCastException(
                    "this object is " + o.getClass() + " not instanceof Integer");
        }
        return retInteger;
    }

    /**
     * 从数据库中查询结果转换成原生字节
     * 
     * @param o
     * @return
     */
    public static byte getByte0(Object o) {
        Byte retByte = getByte(o);
        if (retByte != null) {
            return retByte;
        }
        return 0;
    }

    /**
     * 从数据库中查询结果转换成字节对象
     * 
     * @param o
     *            数据库中查询结果
     * @return 字节对象(如果没结果为null)
     */
    public static Byte getByte(Object o) {
        Byte retByte = null;
        if (o == null)
            return retByte;
        else if (o instanceof Number) {
            retByte = ((Number) o).byteValue();
        } else {
            throw new ClassCastException(
                    "this object is " + o.getClass() + " not instanceof Byte");
        }
        return retByte;
    }

    /**
     * 从数据库中查询结果转换
     * 
     * @param o
     *            数据库中查询结果
     * @return 长整型(如果没结果为null)
     */
    public static Short getShort(Object o) {
        Short retShort = null;
        if (o == null)
            return retShort;
        else if (o instanceof Number) {
            retShort = ((Number) o).shortValue();
        } else {
            throw new ClassCastException(
                    "this object is " + o.getClass() + " not instanceof Long");
        }
        return retShort;
    }

    public static short getShort0(Object o) {
        Short retShort = getShort(o);
        if (retShort != null) {
            return retShort;
        }
        return 0;
    }

    public static double getDouble0(Object o) {
        Double retDouble = getDouble(o);
        if (retDouble != null) {
            return retDouble;
        }
        return 0;
    }

    /**
     * 从数据库中查询结果转换成Double型
     * 
     * @param o
     *            数据库中查询结果
     * @return 长整型(如果没结果为null)
     */
    public static Double getDouble(Object o) {
        Double retDouble = null;
        if (o == null)
            return retDouble;
        else if (o instanceof Number) {
            retDouble = ((Number) o).doubleValue();
        } else {
            throw new ClassCastException(
                    "this object is " + o.getClass() + " not instanceof Double");
        }
        return retDouble;
    }

    /**
     * 从数据库中查询结果转换成Date型
     * 
     * @param o
     *            数据库中查询结果
     * @return 日期时间 (如果没结果为null)
     */
    public static Date getDate(Object o) {
        Date retDate = null;
        if (o == null)
            return retDate;
        else if (o instanceof Date) {
            retDate = (Date) o;
        } else if (o instanceof String) {
//            retDate = DateUtil.parseTime((String) o);
        } else {
            throw new ClassCastException(
                    "this object is " + o.getClass() + " not instanceof Date");
        }
        return retDate;
    }

    /**
     * 从数据库中查询结果转换成Date型转秒数
     * 
     * @param o
     *            数据库中查询结果
     * @return 日期秒数 (如果没结果为0)
     */
    public static int getDateSec(Object o) {
        Date retDate = getDate(o);
        if (retDate != null) {
            return (int) (retDate.getTime() / 1000);
        }
        return 0;
    }

    public static long getDateMill(Object o) {
        Date retDate = getDate(o);
        if (retDate != null) {
            return retDate.getTime();
        }
        return 0;
    }

    /**
     * 将数据库字段转换成yyyy-MM-dd HH:mm:ss字符
     * 
     * @param properName
     * @return sql部分语句
     */
    public static String to_MySQLHH24char(String properName) {
        return "date_format(" + properName + ", '%Y-%m-%d %H:%i:%s')";
    }

    /**
     * 将数据库字段转换成yyyy-MM-dd HH:mm:ss字符
     * 
     * @param properName
     * @return sql部分语句
     */
    public static String to_MySQLHH24Date(String properName) {
        return "str_to_date(" + properName + ", '%Y-%m-%d %H:%i:%s')";
    }

    /**
     * 将数据库字段转换成yyyy-MM-dd HH:mm:ss字符
     * 
     * @param properName
     * @return sql部分语句
     */
    public static String to_OracleHH24char(String properName) {
        return "to_char(" + properName + ", 'yyyy-MM-dd hh24:mi:ss')";
    }

    /**
     * 将yyyy-MM-dd HH:mm:ss字符转换成日期字段
     * 
     * @param properName
     * @return sql部分语句
     */
    public static String to_OracleHH24Date(String properName) {
        return "to_date(" + properName + ", 'yyyy-MM-dd hh24:mi:ss')";
    }

}
