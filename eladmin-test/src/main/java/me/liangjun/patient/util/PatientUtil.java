package me.liangjun.patient.util;

import me.liangjun.patient.domain.Patient;
import me.liangjun.patient.service.dto.PatientDto;

public class PatientUtil {
    public static void resources2patient(Patient resources) {
        if (resources.getProvince() != null) {
            String arr = resources.getProvince().replaceAll("\"", "");
            arr = arr.substring(1, arr.length() - 1);
            String[] split = arr.split(",");
            if (split.length >= 3) {
                resources.setProvince(split[0]);
                resources.setCity(split[1]);
                resources.setArea(split[2]);
            }
        }
        if (resources.getProvince2() != null) {
            String arr2 = resources.getProvince2().replaceAll("\"", "");
            arr2 = arr2.substring(1, arr2.length() - 1);
            String[] split1 = arr2.split(",");
            if (split1.length >= 3) {
                resources.setProvince2(split1[0]);
                resources.setCity2(split1[1]);
                resources.setArea2(split1[2]);
            }
        }
    }

    public static void patient2resources(PatientDto resources) {
        String province = resources.getProvince();
        String city = resources.getCity();
        String area = resources.getArea();
        String[] parr1 = new String[3];
        parr1[0] = province;
        parr1[1] = city;
        parr1[2] = area;
        resources.setProvinceArr(parr1);
        String province2 = resources.getProvince2();
        String city2 = resources.getCity2();
        String area2 = resources.getArea2();
        String[] parr2 = new String[3];
        parr2[0] = province2;
        parr2[1] = city2;
        parr2[2] = area2;
        resources.setProvinceArr2(parr2);


    }
}
