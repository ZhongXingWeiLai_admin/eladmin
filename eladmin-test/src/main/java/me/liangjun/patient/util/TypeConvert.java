package me.liangjun.patient.util;

import me.liangjun.patient.enums.SEX;
import org.mapstruct.Named;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author jun
 * @version 1.0
 * @date 2021/9/2 3:45 下午
 */
@Component
public class TypeConvert {
    @Named("getLabelById")
    public static String getLabelById(int id){
        return Arrays.stream(SEX.values())
                .filter(sex -> sex.getId()==id)
                .map(SEX::getLabel)
                .findFirst()
                .orElse("男");
    }
}
