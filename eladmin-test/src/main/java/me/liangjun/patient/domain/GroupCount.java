package me.liangjun.patient.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GroupCount {
    private int sex;
    private long count;
}
