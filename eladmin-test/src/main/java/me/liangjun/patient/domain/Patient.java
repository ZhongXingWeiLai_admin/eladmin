/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.patient.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-05
**/
@Entity
@Data
@Table(name="patient")
public class Patient implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "name",nullable = false)
    @NotBlank
    @ApiModelProperty(value = "患者名称")
    private String name;

    @Column(name = "sex")
    @ApiModelProperty(value = "性别")
    private Integer sex;

    @Column(name = "phone")
    @ApiModelProperty(value = "电话")
    private String phone;

    @Column(name = "age")
    @ApiModelProperty(value = "年龄")
    private Integer age;

    @Column(name = "level")
    @ApiModelProperty(value = "体力劳动等级")
    private Integer level;

    @Column(name = "nation")
    @ApiModelProperty(value = "民族")
    private String nation;

    @Column(name = "education_background")
    @ApiModelProperty(value = "教育程度")
    private String educationBackground;

    @Column(name = "marry")
    @ApiModelProperty(value = "是否结婚")
    private Integer marry;

    @Column(name = "fertility")
    @ApiModelProperty(value = "生育情况")
    private String fertility;

    @Column(name = "profession")
    @ApiModelProperty(value = "职业")
    private String profession;

    @Column(name = "first_hospital")
    @ApiModelProperty(value = "首诊医院")
    private String firstHospital;

    @Column(name = "first_office")
    @ApiModelProperty(value = "首诊科室")
    private String firstOffice;

    @Column(name = "draw_blood")
    @ApiModelProperty(value = "是否抽血")
    private Integer drawBlood;

    @Column(name = "join_time")
    @ApiModelProperty(value = "入组时间")
    private Timestamp joinTime;

    @Column(name = "contacts")
    @ApiModelProperty(value = "联系人")
    private String contacts;

    @Column(name = "phone2")
    @ApiModelProperty(value = "联系电话")
    private String phone2;

    @Column(name = "phone1")
    @ApiModelProperty(value = "联系电话1")
    private String phone1;

    @Column(name = "province")
    @ApiModelProperty(value = "省")
    private String province;

    @Column(name = "city")
    @ApiModelProperty(value = "市")
    private String city;

    @Column(name = "area")
    @ApiModelProperty(value = "区")
    private String area;

    @Column(name = "province2")
    @ApiModelProperty(value = "长期居住地省")
    private String province2;

    @Column(name = "city2")
    @ApiModelProperty(value = "长期居住地市")
    private String city2;

    @Column(name = "area2")
    @ApiModelProperty(value = "长期居住区")
    private String area2;

    @Column(name = "birth")
    @ApiModelProperty(value = "出生日期")
    private Timestamp birth;

    @Column(name = "money")
    @ApiModelProperty(value = "家庭收入")
    private Integer money;

    @Column(name = "percentage")
    @ApiModelProperty(value = "病例完整度")
    private Integer percentage;

    @Column(name = "id_card")
    @ApiModelProperty(value = "身份证号")
    private String idCard;


    @Column(name = "relation")
    @ApiModelProperty(value = "关系")
    private String relation;


    @Column(name = "doctor")
    @ApiModelProperty(value = "负责医生")
    private String doctor;


    @Column(name = "affiliation")
    @ApiModelProperty(value = "所属机构")
    private String affiliation;


    public void copy(Patient source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}