package me.liangjun.patient.service.dto;

import lombok.Data;

import java.util.List;
@Data
public class PieData {
    private List<Pair> data;
    private String name;
    private List<String> legend;
}
