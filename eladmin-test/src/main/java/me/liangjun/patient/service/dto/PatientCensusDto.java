package me.liangjun.patient.service.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PatientCensusDto {
    /**
     * 病例总数
     */
    private long patientCount;
    /**
     * 90天内新增病例
     */
    private long newAddCount;
    /**
     * 完整病例数
     */
    private long completeCount;
    /**
     * 缺失病例数
     */
    private long lackCount;
    /**
     * 分布月份 YYYY-MM
     */
    private String[] xAxisData;
    /**
     * 月份分布病例
     */
    private int[] lineChartData;
    /**
     * 男人女人分布
     */
    private PieData genderData;
    /**
     * 年龄分布
     */
    private PieData ageData;

}
