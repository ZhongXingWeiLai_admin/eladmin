/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.patient.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Column;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-05
**/
@Data
public class PatientDto implements Serializable {

    /** ID */
    private Long id;

    /** 患者名称 */
    private String name;

    /** 性别 */
    private Integer sex;

    /** 电话 */
    private String phone;

    /** 年龄 */
    private Integer age;

    /** 体力劳动等级 */
    private Integer level;

    /** 民族 */
    private String nation;

    /** 教育程度 */
    private String educationBackground;

    /** 是否结婚 */
    private Integer marry;

    /** 生育情况 */
    private String fertility;

    /** 职业 */
    private String profession;

    /** 首诊医院 */
    private String firstHospital;

    /** 首诊科室 */
    private String firstOffice;

    /** 是否抽血 */
    private Integer drawBlood;

    /** 入组时间 */
    private Timestamp joinTime;

    /** 联系人 */
    private String contacts;

    /** 联系电话 */
    private String phone2;

    /** 联系电话1 */
    private String phone1;

    /** 省 */
    private String province;
    private String[] provinceArr;

    /** 市 */
    private String city;

    /** 区 */
    private String area;

    /** 长期居住地省 */
    private String province2;
    private String[] provinceArr2;

    /** 长期居住地市 */
    private String city2;

    /** 长期居住区 */
    private String area2;

    /** 出生日期 */
    private Timestamp birth;

    /** 家庭收入 */
    private Integer money;

    /** 病例完整度 */
    private Integer percentage;

    /**
     * 身份证号
     */
    private String idCard;

    /**
     * 关系
     */
    private String relation;

    /**
     * 负责医生
     */
    private String doctor;

    /**
     * 所属机构
     */
    private String affiliation;
}