package me.liangjun.patient.service.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Pair {
    private String name;
    private String value;
}
