/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.patient.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.util.List;
import me.zhengjie.annotation.Query;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-05
**/
@Data
public class PatientQueryCriteria{

    /** 模糊 */
    @Query(type = Query.Type.INNER_LIKE)
    private String name;

    /** 精确 */
    @Query
    private Integer sex;

    /** 精确 */
    @Query
    private String phone;

    /** 精确 */
    @Query
    private Integer age;

    /** 精确 */
    @Query
    private Integer level;

    /** 精确 */
    @Query
    private String nation;

    /** 精确 */
    @Query
    private String educationBackground;

    /** 精确 */
    @Query
    private Integer marry;

    /** 精确 */
    @Query
    private String fertility;

    /** 精确 */
    @Query
    private String firstHospital;

    /** 精确 */
    @Query
    private String firstOffice;

    /** 精确 */
    @Query
    private Integer drawBlood;
    /** BETWEEN */
    @Query(type = Query.Type.BETWEEN)
    private List<Timestamp> joinTime;
}