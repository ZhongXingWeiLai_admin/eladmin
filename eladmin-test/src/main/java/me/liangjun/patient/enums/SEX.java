package me.liangjun.patient.enums;

import lombok.Getter;

/**
 * @author jun
 * @version 1.0
 * @date 2021/9/2 2:28 下午
 */
@Getter
public enum SEX {
    MALE(1,"男"),
    FAMALE(2,"女")
    ;
    private int id;
    private String label;
    SEX(int id,String label){
        this.id = id;
        this.label = label;
    }

}
