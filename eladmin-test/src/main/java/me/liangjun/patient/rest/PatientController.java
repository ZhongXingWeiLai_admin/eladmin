package me.liangjun.patient.rest;

import me.zhengjie.annotation.AnonymousAccess;
import me.zhengjie.annotation.Log;
import me.liangjun.patient.domain.Patient;
import me.liangjun.patient.service.PatientService;
import me.liangjun.patient.service.dto.PatientQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-05
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "patient管理")
@RequestMapping("/api/patient")
public class PatientController {

    private final PatientService patientService;

    @GetMapping(value = "/findById")
    @Log("查询patient")
    @ApiOperation("查询patient")
    @PreAuthorize("@el.check('patient:list')")
//    @AnonymousAccess
    public ResponseEntity<Object> findById(@RequestParam("id") Long id) {
        return new ResponseEntity<>(patientService.findById(id), HttpStatus.OK);
    }

    @Log("统计患者数据")
    @ApiOperation("统计患者数据")
    @GetMapping(value = "/census")
//    @AnonymousAccess
    @PreAuthorize("@el.check('patient:list')")
    public ResponseEntity<Object> census() throws IOException {
        return new ResponseEntity<>(patientService.census(), HttpStatus.OK);
    }

    @Log("患者分布中心数据")
    @ApiOperation("患者分布中心数据")
    @GetMapping(value = "/findPatientGroupByProvince")
//    @AnonymousAccess
    @PreAuthorize("@el.check('patient:list')")
    public ResponseEntity<Object> findPatientGroupByProvince() {
        return new ResponseEntity<>(patientService.findPatientGroupByProvince(), HttpStatus.OK);
    }
    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('patient:list')")
    public void download(HttpServletResponse response, PatientQueryCriteria criteria) throws IOException {
        patientService.download(patientService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询patient")
    @ApiOperation("查询patient")
    @PreAuthorize("@el.check('patient:list')")
    public ResponseEntity<Object> query(PatientQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(patientService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增patient")
    @ApiOperation("新增patient")
    @PreAuthorize("@el.check('patient:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody Patient resources){
        return new ResponseEntity<>(patientService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改patient")
    @ApiOperation("修改patient")
    @PreAuthorize("@el.check('patient:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody Patient resources){
        patientService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除patient")
    @ApiOperation("删除patient")
    @PreAuthorize("@el.check('patient:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        patientService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}