package me.liangjun.patient.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import me.liangjun.eyemovements.service.EyeMovementsScoreService;
import me.liangjun.healthcheckup.service.HealthCheckupService;
import me.liangjun.medicalhistorybefore.service.dto.BaseLineCaseDto;
import me.liangjun.mg.service.MgScoreService;
import me.liangjun.mgadl.service.MgAdlScoreService;
import me.liangjun.mgqol.service.MgQolScoreService;
import me.liangjun.ocularqmg.service.OcularQmgScoreService;
import me.liangjun.qmg.service.QmgScoreService;
import me.zhengjie.annotation.AnonymousAccess;
import me.zhengjie.annotation.Log;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Api(tags = "患者详情管理")
@RequestMapping("/api/patient/detail")
public class PatientDetailController {

    private final HealthCheckupService healthCheckupService;
    private final EyeMovementsScoreService eyeMovementsScoreService;
    private final MgScoreService mgScoreService;
    private final MgAdlScoreService mgAdlScoreService;
    private final MgQolScoreService mgQolScoreService;
    private final OcularQmgScoreService ocularQmgScoreService;
    private final QmgScoreService qmgScoreService;

    @GetMapping(value = "/queryBaseLineCase")
    @Log("查询基线情况")
    @ApiOperation("查询基线情况")
    @AnonymousAccess
    public ResponseEntity<Object> queryBaseLineCase(@RequestParam("pid") Long pid) {
        BaseLineCaseDto baseLineCaseDto = new BaseLineCaseDto();
        baseLineCaseDto.setHealthCheckup(healthCheckupService.findByPid(pid));
        baseLineCaseDto.setEyeMovementsScore(eyeMovementsScoreService.findByPid(pid));
        baseLineCaseDto.setMgScore(mgScoreService.findByPid(pid));
        baseLineCaseDto.setMgAdlScore(mgAdlScoreService.findByPid(pid));
        baseLineCaseDto.setMgQolScore(mgQolScoreService.findByPid(pid));
        baseLineCaseDto.setOcularQmgScore(ocularQmgScoreService.findByPid(pid));
        baseLineCaseDto.setQmgScore(qmgScoreService.findByPid(pid));
        return new ResponseEntity<>(baseLineCaseDto, HttpStatus.OK);
    }
}
