/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.specimencollection.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.specimencollection.domain.ThymusSpecimen;
import me.liangjun.specimencollection.service.ThymusSpecimenService;
import me.liangjun.specimencollection.service.dto.ThymusSpecimenQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-16
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "thymusSpecimen管理")
@RequestMapping("/api/thymusSpecimen")
public class ThymusSpecimenController {

    private final ThymusSpecimenService thymusSpecimenService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('thymusSpecimen:list')")
    public void download(HttpServletResponse response, ThymusSpecimenQueryCriteria criteria) throws IOException {
        thymusSpecimenService.download(thymusSpecimenService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询thymusSpecimen")
    @ApiOperation("查询thymusSpecimen")
    @PreAuthorize("@el.check('thymusSpecimen:list')")
    public ResponseEntity<Object> query(ThymusSpecimenQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(thymusSpecimenService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增thymusSpecimen")
    @ApiOperation("新增thymusSpecimen")
    @PreAuthorize("@el.check('thymusSpecimen:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody ThymusSpecimen resources){
        return new ResponseEntity<>(thymusSpecimenService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改thymusSpecimen")
    @ApiOperation("修改thymusSpecimen")
    @PreAuthorize("@el.check('thymusSpecimen:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody ThymusSpecimen resources){
        thymusSpecimenService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除thymusSpecimen")
    @ApiOperation("删除thymusSpecimen")
    @PreAuthorize("@el.check('thymusSpecimen:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        thymusSpecimenService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}