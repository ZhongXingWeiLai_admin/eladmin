/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.specimencollection.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.specimencollection.domain.Sample;
import me.liangjun.specimencollection.service.SampleService;
import me.liangjun.specimencollection.service.dto.SampleQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-26
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "sample管理")
@RequestMapping("/api/sample")
public class SampleController {

    private final SampleService sampleService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('sample:list')")
    public void download(HttpServletResponse response, SampleQueryCriteria criteria) throws IOException {
        sampleService.download(sampleService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询sample")
    @ApiOperation("查询sample")
    @PreAuthorize("@el.check('sample:list')")
    public ResponseEntity<Object> query(SampleQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(sampleService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增sample")
    @ApiOperation("新增sample")
    @PreAuthorize("@el.check('sample:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody Sample resources){
        return new ResponseEntity<>(sampleService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改sample")
    @ApiOperation("修改sample")
    @PreAuthorize("@el.check('sample:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody Sample resources){
        sampleService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除sample")
    @ApiOperation("删除sample")
    @PreAuthorize("@el.check('sample:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        sampleService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}