/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.specimencollection.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.specimencollection.domain.BloodSpecimen;
import me.liangjun.specimencollection.service.BloodSpecimenService;
import me.liangjun.specimencollection.service.dto.BloodSpecimenQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-26
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "bloodSpecimen管理")
@RequestMapping("/api/bloodSpecimen")
public class BloodSpecimenController {

    private final BloodSpecimenService bloodSpecimenService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('bloodSpecimen:list')")
    public void download(HttpServletResponse response, BloodSpecimenQueryCriteria criteria) throws IOException {
        bloodSpecimenService.download(bloodSpecimenService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询bloodSpecimen")
    @ApiOperation("查询bloodSpecimen")
    @PreAuthorize("@el.check('bloodSpecimen:list')")
    public ResponseEntity<Object> query(BloodSpecimenQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(bloodSpecimenService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增bloodSpecimen")
    @ApiOperation("新增bloodSpecimen")
    @PreAuthorize("@el.check('bloodSpecimen:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody BloodSpecimen resources){
        return new ResponseEntity<>(bloodSpecimenService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改bloodSpecimen")
    @ApiOperation("修改bloodSpecimen")
    @PreAuthorize("@el.check('bloodSpecimen:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody BloodSpecimen resources){
        bloodSpecimenService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除bloodSpecimen")
    @ApiOperation("删除bloodSpecimen")
    @PreAuthorize("@el.check('bloodSpecimen:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        bloodSpecimenService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}