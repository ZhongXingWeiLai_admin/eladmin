/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.specimencollection.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-16
**/
@Entity
@Data
@Table(name="thymus_specimen")
public class ThymusSpecimen implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "specimen_collection_time")
    @ApiModelProperty(value = "标本采集时间")
    private Timestamp specimenCollectionTime;

    @Column(name = "thymus_superior")
    @ApiModelProperty(value = "胸腺上级（管）")
    private String thymusSuperior;

    @Column(name = "thymus_middle")
    @ApiModelProperty(value = "胸腺中部（管）")
    private String thymusMiddle;

    @Column(name = "thymus_inferior")
    @ApiModelProperty(value = "胸腺下级（管）")
    private String thymusInferior;

    @Column(name = "tumor_tissue")
    @ApiModelProperty(value = "瘤体组织（管）")
    private String tumorTissue;

    @Column(name = "storage_location")
    @ApiModelProperty(value = "储存位置")
    private String storageLocation;

    public void copy(ThymusSpecimen source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}