/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.specimencollection.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-26
**/
@Entity
@Data
@Table(name="sample")
public class Sample implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "sample_name")
    @ApiModelProperty(value = "样本名称")
    private Timestamp sampleName;

    @Column(name = "sample_num")
    @ApiModelProperty(value = "样本数量")
    private String sampleNum;

    @Column(name = "sample_refrigerator")
    @ApiModelProperty(value = "样本存放零下80℃冰箱（号）")
    private String sampleRefrigerator;

    @Column(name = "sample_storage_layer")
    @ApiModelProperty(value = "样本存放层")
    private String sampleStorageLayer;

    @Column(name = "sample_storage_rack")
    @ApiModelProperty(value = "样本存放架")
    private String sampleStorageRack;

    @Column(name = "sample_storage_sublayer")
    @ApiModelProperty(value = "样本存放亚层")
    private String sampleStorageSublayer;

    @Column(name = "sample_storage_box")
    @ApiModelProperty(value = "样本存放盒")
    private String sampleStorageBox;

    public void copy(Sample source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}