/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.specimencollection.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-26
**/
@Entity
@Data
@Table(name="blood_specimen")
public class BloodSpecimen implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "is_immunosuppressant")
    @ApiModelProperty(value = "抽血前3个月内是否服用免疫抑制剂")
    private Integer isImmunosuppressant;

    @Column(name = "immunosuppressant_options")
    @ApiModelProperty(value = "免疫抑制剂选项")
    private String immunosuppressantOptions;

    @Column(name = "is_immunotherapy")
    @ApiModelProperty(value = "抽血前1个月是否进行免疫治疗")
    private Integer isImmunotherapy;

    @Column(name = "immunotherapy_options")
    @ApiModelProperty(value = "免疫治疗选项")
    private String immunotherapyOptions;

    @Column(name = "blood_specimen")
    @ApiModelProperty(value = "血液标本")
    private Integer bloodSpecimen;

    @Column(name = "specimen_collection_time")
    @ApiModelProperty(value = "标本采集时间")
    private Timestamp specimenCollectionTime;

    @Column(name = "specimen_no")
    @ApiModelProperty(value = "标本号")
    private String specimenNo;

    @Column(name = "fasting")
    @ApiModelProperty(value = "是否空腹")
    private Integer fasting;

    @Column(name = "specimen_collection_category")
    @ApiModelProperty(value = "标本采集类别")
    private Integer specimenCollectionCategory;

    @Column(name = "purple_anticoagulant_tube")
    @ApiModelProperty(value = "紫头抗凝管6ml（管）")
    private String purpleAnticoagulantTube;

    @Column(name = "red_anticoagulant_tube")
    @ApiModelProperty(value = "红头促凝管10ml（管）")
    private String redAnticoagulantTube;

    @Column(name = "specimen_processing_time")
    @ApiModelProperty(value = "标本处理时间")
    private Timestamp specimenProcessingTime;

    @Column(name = "serum")
    @ApiModelProperty(value = "血清（管）")
    private String serum;

    @Column(name = "plasma")
    @ApiModelProperty(value = "血浆（管）")
    private String plasma;

    @Column(name = "leukocyte")
    @ApiModelProperty(value = "白细胞（管）")
    private String leukocyte;

    @Column(name = "lymphocyte")
    @ApiModelProperty(value = "淋巴细胞（管）")
    private String lymphocyte;

    @Column(name = "dna")
    @ApiModelProperty(value = "DNA（管）")
    private String dna;

    @Column(name = "result")
    @ApiModelProperty(value = "结果")
    private String result;

    @Column(name = "sample_storage_time")
    @ApiModelProperty(value = "样本存储时间")
    private Timestamp sampleStorageTime;

    @Column(name = "sample_type")
    @ApiModelProperty(value = "样本种类")
    private String sampleType;

    public void copy(BloodSpecimen source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}