/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.specimencollection.service.impl;

import me.liangjun.specimencollection.domain.Sample;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.specimencollection.repository.SampleRepository;
import me.liangjun.specimencollection.service.SampleService;
import me.liangjun.specimencollection.service.dto.SampleDto;
import me.liangjun.specimencollection.service.dto.SampleQueryCriteria;
import me.liangjun.specimencollection.service.mapstruct.SampleMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-26
**/
@Service
@RequiredArgsConstructor
public class SampleServiceImpl implements SampleService {

    private final SampleRepository sampleRepository;
    private final SampleMapper sampleMapper;

    @Override
    public Map<String,Object> queryAll(SampleQueryCriteria criteria, Pageable pageable){
        Page<Sample> page = sampleRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(sampleMapper::toDto));
    }

    @Override
    public List<SampleDto> queryAll(SampleQueryCriteria criteria){
        return sampleMapper.toDto(sampleRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public SampleDto findById(Long id) {
        Sample sample = sampleRepository.findById(id).orElseGet(Sample::new);
        ValidationUtil.isNull(sample.getId(),"Sample","id",id);
        return sampleMapper.toDto(sample);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SampleDto create(Sample resources) {
        return sampleMapper.toDto(sampleRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(Sample resources) {
        Sample sample = sampleRepository.findById(resources.getId()).orElseGet(Sample::new);
        ValidationUtil.isNull( sample.getId(),"Sample","id",resources.getId());
        sample.copy(resources);
        sampleRepository.save(sample);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            sampleRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<SampleDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (SampleDto sample : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", sample.getPId());
            map.put("样本名称", sample.getSampleName());
            map.put("样本数量", sample.getSampleNum());
            map.put("样本存放零下80℃冰箱（号）", sample.getSampleRefrigerator());
            map.put("样本存放层", sample.getSampleStorageLayer());
            map.put("样本存放架", sample.getSampleStorageRack());
            map.put("样本存放亚层", sample.getSampleStorageSublayer());
            map.put("样本存放盒", sample.getSampleStorageBox());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}