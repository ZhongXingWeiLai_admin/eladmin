/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.specimencollection.service.impl;

import me.liangjun.specimencollection.domain.BloodSpecimen;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.specimencollection.repository.BloodSpecimenRepository;
import me.liangjun.specimencollection.service.BloodSpecimenService;
import me.liangjun.specimencollection.service.dto.BloodSpecimenDto;
import me.liangjun.specimencollection.service.dto.BloodSpecimenQueryCriteria;
import me.liangjun.specimencollection.service.mapstruct.BloodSpecimenMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-26
**/
@Service
@RequiredArgsConstructor
public class BloodSpecimenServiceImpl implements BloodSpecimenService {

    private final BloodSpecimenRepository bloodSpecimenRepository;
    private final BloodSpecimenMapper bloodSpecimenMapper;

    @Override
    public Map<String,Object> queryAll(BloodSpecimenQueryCriteria criteria, Pageable pageable){
        Page<BloodSpecimen> page = bloodSpecimenRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(bloodSpecimenMapper::toDto));
    }

    @Override
    public List<BloodSpecimenDto> queryAll(BloodSpecimenQueryCriteria criteria){
        return bloodSpecimenMapper.toDto(bloodSpecimenRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public BloodSpecimenDto findById(Long id) {
        BloodSpecimen bloodSpecimen = bloodSpecimenRepository.findById(id).orElseGet(BloodSpecimen::new);
        ValidationUtil.isNull(bloodSpecimen.getId(),"BloodSpecimen","id",id);
        return bloodSpecimenMapper.toDto(bloodSpecimen);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BloodSpecimenDto create(BloodSpecimen resources) {
        return bloodSpecimenMapper.toDto(bloodSpecimenRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(BloodSpecimen resources) {
        BloodSpecimen bloodSpecimen = bloodSpecimenRepository.findById(resources.getId()).orElseGet(BloodSpecimen::new);
        ValidationUtil.isNull( bloodSpecimen.getId(),"BloodSpecimen","id",resources.getId());
        bloodSpecimen.copy(resources);
        bloodSpecimenRepository.save(bloodSpecimen);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            bloodSpecimenRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<BloodSpecimenDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (BloodSpecimenDto bloodSpecimen : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", bloodSpecimen.getPId());
            map.put("抽血前3个月内是否服用免疫抑制剂", bloodSpecimen.getIsImmunosuppressant());
            map.put("免疫抑制剂选项", bloodSpecimen.getImmunosuppressantOptions());
            map.put("抽血前1个月是否进行免疫治疗", bloodSpecimen.getIsImmunotherapy());
            map.put("免疫治疗选项", bloodSpecimen.getImmunotherapyOptions());
            map.put("血液标本", bloodSpecimen.getBloodSpecimen());
            map.put("标本采集时间", bloodSpecimen.getSpecimenCollectionTime());
            map.put("标本号", bloodSpecimen.getSpecimenNo());
            map.put("是否空腹", bloodSpecimen.getFasting());
            map.put("标本采集类别", bloodSpecimen.getSpecimenCollectionCategory());
            map.put("紫头抗凝管6ml（管）", bloodSpecimen.getPurpleAnticoagulantTube());
            map.put("红头促凝管10ml（管）", bloodSpecimen.getRedAnticoagulantTube());
            map.put("标本处理时间", bloodSpecimen.getSpecimenProcessingTime());
            map.put("血清（管）", bloodSpecimen.getSerum());
            map.put("血浆（管）", bloodSpecimen.getPlasma());
            map.put("白细胞（管）", bloodSpecimen.getLeukocyte());
            map.put("淋巴细胞（管）", bloodSpecimen.getLymphocyte());
            map.put("DNA（管）", bloodSpecimen.getDna());
            map.put("结果", bloodSpecimen.getResult());
            map.put("样本存储时间", bloodSpecimen.getSampleStorageTime());
            map.put("样本种类", bloodSpecimen.getSampleType());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}