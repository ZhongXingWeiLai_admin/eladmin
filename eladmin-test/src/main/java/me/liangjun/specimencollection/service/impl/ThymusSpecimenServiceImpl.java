/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.specimencollection.service.impl;

import me.liangjun.specimencollection.domain.ThymusSpecimen;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.specimencollection.repository.ThymusSpecimenRepository;
import me.liangjun.specimencollection.service.ThymusSpecimenService;
import me.liangjun.specimencollection.service.dto.ThymusSpecimenDto;
import me.liangjun.specimencollection.service.dto.ThymusSpecimenQueryCriteria;
import me.liangjun.specimencollection.service.mapstruct.ThymusSpecimenMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-16
**/
@Service
@RequiredArgsConstructor
public class ThymusSpecimenServiceImpl implements ThymusSpecimenService {

    private final ThymusSpecimenRepository thymusSpecimenRepository;
    private final ThymusSpecimenMapper thymusSpecimenMapper;

    @Override
    public Map<String,Object> queryAll(ThymusSpecimenQueryCriteria criteria, Pageable pageable){
        Page<ThymusSpecimen> page = thymusSpecimenRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(thymusSpecimenMapper::toDto));
    }

    @Override
    public List<ThymusSpecimenDto> queryAll(ThymusSpecimenQueryCriteria criteria){
        return thymusSpecimenMapper.toDto(thymusSpecimenRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public ThymusSpecimenDto findById(Long id) {
        ThymusSpecimen thymusSpecimen = thymusSpecimenRepository.findById(id).orElseGet(ThymusSpecimen::new);
        ValidationUtil.isNull(thymusSpecimen.getId(),"ThymusSpecimen","id",id);
        return thymusSpecimenMapper.toDto(thymusSpecimen);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ThymusSpecimenDto create(ThymusSpecimen resources) {
        return thymusSpecimenMapper.toDto(thymusSpecimenRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(ThymusSpecimen resources) {
        ThymusSpecimen thymusSpecimen = thymusSpecimenRepository.findById(resources.getId()).orElseGet(ThymusSpecimen::new);
        ValidationUtil.isNull( thymusSpecimen.getId(),"ThymusSpecimen","id",resources.getId());
        thymusSpecimen.copy(resources);
        thymusSpecimenRepository.save(thymusSpecimen);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            thymusSpecimenRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<ThymusSpecimenDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (ThymusSpecimenDto thymusSpecimen : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", thymusSpecimen.getPId());
            map.put("标本采集时间", thymusSpecimen.getSpecimenCollectionTime());
            map.put("胸腺上级（管）", thymusSpecimen.getThymusSuperior());
            map.put("胸腺中部（管）", thymusSpecimen.getThymusMiddle());
            map.put("胸腺下级（管）", thymusSpecimen.getThymusInferior());
            map.put("瘤体组织（管）", thymusSpecimen.getTumorTissue());
            map.put("储存位置", thymusSpecimen.getStorageLocation());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}