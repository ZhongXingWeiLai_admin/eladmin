/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.specimencollection.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-26
**/
@Data
public class BloodSpecimenDto implements Serializable {

    /** ID */
    private Long id;

    /** pID */
    private Long pId;

    /** 抽血前3个月内是否服用免疫抑制剂 */
    private Integer isImmunosuppressant;

    /** 免疫抑制剂选项 */
    private String immunosuppressantOptions;

    /** 抽血前1个月是否进行免疫治疗 */
    private Integer isImmunotherapy;

    /** 免疫治疗选项 */
    private String immunotherapyOptions;

    /** 血液标本 */
    private Integer bloodSpecimen;

    /** 标本采集时间 */
    private Timestamp specimenCollectionTime;

    /** 标本号 */
    private String specimenNo;

    /** 是否空腹 */
    private Integer fasting;

    /** 标本采集类别 */
    private Integer specimenCollectionCategory;

    /** 紫头抗凝管6ml（管） */
    private String purpleAnticoagulantTube;

    /** 红头促凝管10ml（管） */
    private String redAnticoagulantTube;

    /** 标本处理时间 */
    private Timestamp specimenProcessingTime;

    /** 血清（管） */
    private String serum;

    /** 血浆（管） */
    private String plasma;

    /** 白细胞（管） */
    private String leukocyte;

    /** 淋巴细胞（管） */
    private String lymphocyte;

    /** DNA（管） */
    private String dna;

    /** 结果 */
    private String result;

    /** 样本存储时间 */
    private Timestamp sampleStorageTime;

    /** 样本种类 */
    private String sampleType;
}