/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.qmg.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-04
**/
@Entity
@Data
@Table(name="qmg_score")
public class QmgScore implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "diplopia")
    @ApiModelProperty(value = "左右视出现复视")
    private Integer diplopia;

    @Column(name = "blepharoptosis")
    @ApiModelProperty(value = "上视出现眼睑下垂")
    private Integer blepharoptosis;

    @Column(name = "eyelid_closure")
    @ApiModelProperty(value = "眼睑闭合:")
    private Integer eyelidClosure;

    @Column(name = "swallow")
    @ApiModelProperty(value = "吞咽100ml水")
    private Integer swallow;

    @Column(name = "count")
    @ApiModelProperty(value = "数数1-50")
    private Integer count;

    @Column(name = "sit_up1")
    @ApiModelProperty(value = "坐位右上肢抬起90度")
    private Integer sitUp1;

    @Column(name = "sit_up2")
    @ApiModelProperty(value = "坐位左上肢抬起90度")
    private Integer sitUp2;

    @Column(name = "vital_capacity")
    @ApiModelProperty(value = "肺活量（%预计值）")
    private Integer vitalCapacity;

    @Column(name = "right_hand_grip")
    @ApiModelProperty(value = "右手握力kgW")
    private Integer rightHandGrip;

    @Column(name = "left_hand_grip")
    @ApiModelProperty(value = "左手握力kgW")
    private Integer leftHandGrip;

    @Column(name = "pwwtt")
    @ApiModelProperty(value = "平卧位抬头（秒）")
    private Integer pwwtt;

    @Column(name = "pwwtq1")
    @ApiModelProperty(value = "平卧位右下肢抬起（秒）")
    private Integer pwwtq1;

    @Column(name = "pwwtq2")
    @ApiModelProperty(value = "平卧位左下肢抬起（秒）")
    private Integer pwwtq2;


    @Column(name = "sex")
    @ApiModelProperty(value = "握力性别")
    private Integer sex;


    public void copy(QmgScore source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}