/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.qmg.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.qmg.domain.QmgScore;
import me.liangjun.qmg.service.QmgScoreService;
import me.liangjun.qmg.service.dto.QmgScoreQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-04
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "qmg管理")
@RequestMapping("/api/qmgScore")
public class QmgScoreController {

    private final QmgScoreService qmgScoreService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('qmgScore:list')")
    public void download(HttpServletResponse response, QmgScoreQueryCriteria criteria) throws IOException {
        qmgScoreService.download(qmgScoreService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询qmg")
    @ApiOperation("查询qmg")
    @PreAuthorize("@el.check('qmgScore:list')")
    public ResponseEntity<Object> query(QmgScoreQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(qmgScoreService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增qmg")
    @ApiOperation("新增qmg")
    @PreAuthorize("@el.check('qmgScore:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody QmgScore resources){
        return new ResponseEntity<>(qmgScoreService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改qmg")
    @ApiOperation("修改qmg")
    @PreAuthorize("@el.check('qmgScore:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody QmgScore resources){
        qmgScoreService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除qmg")
    @ApiOperation("删除qmg")
    @PreAuthorize("@el.check('qmgScore:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        qmgScoreService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}