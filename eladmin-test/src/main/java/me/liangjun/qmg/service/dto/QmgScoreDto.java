/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.qmg.service.dto;

import lombok.Data;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-04
**/
@Data
public class QmgScoreDto implements Serializable {

    /** ID */
    private Long id;

    /** pID */
    private Long pId;

    /** 左右视出现复视 */
    private Integer diplopia;

    /** 上视出现眼睑下垂 */
    private Integer blepharoptosis;

    /** 眼睑闭合: */
    private Integer eyelidClosure;

    /** 吞咽100ml水 */
    private Integer swallow;

    /** 数数1-50 */
    private Integer count;

    /** 坐位右上肢抬起90度 */
    private Integer sitUp1;

    /** 坐位左上肢抬起90度 */
    private Integer sitUp2;

    /** 肺活量（%预计值） */
    private Integer vitalCapacity;

    /** 右手握力kgW */
    private Integer rightHandGrip;

    /** 左手握力kgW */
    private Integer leftHandGrip;

    /** 平卧位抬头（秒） */
    private Integer pwwtt;

    /** 平卧位右下肢抬起（秒） */
    private Integer pwwtq1;

    /** 平卧位左下肢抬起（秒） */
    private Integer pwwtq2;

    /** 握力性别*/
    private Integer sex;

}