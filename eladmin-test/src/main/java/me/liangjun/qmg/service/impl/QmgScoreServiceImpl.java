/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.qmg.service.impl;

import me.liangjun.qmg.domain.QmgScore;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.qmg.repository.QmgScoreRepository;
import me.liangjun.qmg.service.QmgScoreService;
import me.liangjun.qmg.service.dto.QmgScoreDto;
import me.liangjun.qmg.service.dto.QmgScoreQueryCriteria;
import me.liangjun.qmg.service.mapstruct.QmgScoreMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-04
**/
@Service
@RequiredArgsConstructor
public class QmgScoreServiceImpl implements QmgScoreService {

    private final QmgScoreRepository qmgScoreRepository;
    private final QmgScoreMapper qmgScoreMapper;

    @Override
    public Map<String,Object> queryAll(QmgScoreQueryCriteria criteria, Pageable pageable){
        Page<QmgScore> page = qmgScoreRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(qmgScoreMapper::toDto));
    }

    @Override
    public List<QmgScoreDto> queryAll(QmgScoreQueryCriteria criteria){
        return qmgScoreMapper.toDto(qmgScoreRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public QmgScoreDto findById(Long id) {
        QmgScore qmgScore = qmgScoreRepository.findById(id).orElseGet(QmgScore::new);
        ValidationUtil.isNull(qmgScore.getId(),"QmgScore","id",id);
        return qmgScoreMapper.toDto(qmgScore);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public QmgScoreDto create(QmgScore resources) {
        return qmgScoreMapper.toDto(qmgScoreRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(QmgScore resources) {
        QmgScore qmgScore = qmgScoreRepository.findById(resources.getId()).orElseGet(QmgScore::new);
        ValidationUtil.isNull( qmgScore.getId(),"QmgScore","id",resources.getId());
        qmgScore.copy(resources);
        qmgScoreRepository.save(qmgScore);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            qmgScoreRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<QmgScoreDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (QmgScoreDto qmgScore : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", qmgScore.getPId());
            map.put("左右视出现复视", qmgScore.getDiplopia());
            map.put("上视出现眼睑下垂", qmgScore.getBlepharoptosis());
            map.put("眼睑闭合:", qmgScore.getEyelidClosure());
            map.put("吞咽100ml水", qmgScore.getSwallow());
            map.put("数数1-50", qmgScore.getCount());
            map.put("坐位右上肢抬起90度", qmgScore.getSitUp1());
            map.put("坐位左上肢抬起90度", qmgScore.getSitUp2());
            map.put("肺活量（%预计值）", qmgScore.getVitalCapacity());
            map.put("右手握力kgW", qmgScore.getRightHandGrip());
            map.put("左手握力kgW", qmgScore.getLeftHandGrip());
            map.put("平卧位抬头（秒）", qmgScore.getPwwtt());
            map.put("平卧位右下肢抬起（秒）", qmgScore.getPwwtq1());
            map.put("平卧位左下肢抬起（秒）", qmgScore.getPwwtq2());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public QmgScoreDto findByPid(Long pid) {
        return qmgScoreMapper.toDto(qmgScoreRepository.findByPid(pid));
    }
}