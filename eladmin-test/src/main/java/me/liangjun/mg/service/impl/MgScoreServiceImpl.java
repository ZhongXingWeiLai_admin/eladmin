/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.mg.service.impl;

import me.liangjun.mg.domain.MgScore;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.mg.repository.MgScoreRepository;
import me.liangjun.mg.service.MgScoreService;
import me.liangjun.mg.service.dto.MgScoreDto;
import me.liangjun.mg.service.dto.MgScoreQueryCriteria;
import me.liangjun.mg.service.mapstruct.MgScoreMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-11
**/
@Service
@RequiredArgsConstructor
public class MgScoreServiceImpl implements MgScoreService {

    private final MgScoreRepository mgScoreRepository;
    private final MgScoreMapper mgScoreMapper;

    @Override
    public Map<String,Object> queryAll(MgScoreQueryCriteria criteria, Pageable pageable){
        Page<MgScore> page = mgScoreRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(mgScoreMapper::toDto));
    }

    @Override
    public List<MgScoreDto> queryAll(MgScoreQueryCriteria criteria){
        return mgScoreMapper.toDto(mgScoreRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public MgScoreDto findById(Long id) {
        MgScore mgScore = mgScoreRepository.findById(id).orElseGet(MgScore::new);
        ValidationUtil.isNull(mgScore.getId(),"MgScore","id",id);
        return mgScoreMapper.toDto(mgScore);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public MgScoreDto create(MgScore resources) {
        return mgScoreMapper.toDto(mgScoreRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(MgScore resources) {
        MgScore mgScore = mgScoreRepository.findById(resources.getId()).orElseGet(MgScore::new);
        ValidationUtil.isNull( mgScore.getId(),"MgScore","id",resources.getId());
        mgScore.copy(resources);
        mgScoreRepository.save(mgScore);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            mgScoreRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<MgScoreDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (MgScoreDto mgScore : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", mgScore.getPId());
            map.put("眼睑下垂（上视）", mgScore.getEyelid());
            map.put("复视（左右视）", mgScore.getDiplopia());
            map.put("闭目", mgScore.getClose());
            map.put("言语", mgScore.getSpeech());
            map.put("咀嚼", mgScore.getChew());
            map.put("吞咽", mgScore.getSwallow());
            map.put("呼吸", mgScore.getBreathe());
            map.put("屈伸颈", mgScore.getFlexibleNeck());
            map.put("肩部外展", mgScore.getShoulderOutfield());
            map.put("屈髋（平卧位）", mgScore.getPile());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public MgScoreDto findByPid(Long pid) {
        return mgScoreMapper.toDto(mgScoreRepository.findByPid(pid));
    }
}