/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.mg.service.dto;

import lombok.Data;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-11
**/
@Data
public class MgScoreDto implements Serializable {

    /** ID */
    private Long id;

    /** pID */
    private Long pId;

    /** 眼睑下垂（上视） */
    private Integer eyelid;

    /** 复视（左右视） */
    private Integer diplopia;

    /** 闭目 */
    private Integer close;

    /** 言语 */
    private Integer speech;

    /** 咀嚼 */
    private Integer chew;

    /** 吞咽 */
    private Integer swallow;

    /** 呼吸 */
    private Integer breathe;

    /** 屈伸颈 */
    private Integer flexibleNeck;

    /** 肩部外展 */
    private Integer shoulderOutfield;

    /** 屈髋（平卧位） */
    private Integer pile;
}