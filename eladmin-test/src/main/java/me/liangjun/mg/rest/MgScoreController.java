/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.mg.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.mg.domain.MgScore;
import me.liangjun.mg.service.MgScoreService;
import me.liangjun.mg.service.dto.MgScoreQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-11
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "mg管理")
@RequestMapping("/api/mgScore")
public class MgScoreController {

    private final MgScoreService mgScoreService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('mgScore:list')")
    public void download(HttpServletResponse response, MgScoreQueryCriteria criteria) throws IOException {
        mgScoreService.download(mgScoreService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询mg")
    @ApiOperation("查询mg")
    @PreAuthorize("@el.check('mgScore:list')")
    public ResponseEntity<Object> query(MgScoreQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(mgScoreService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增mg")
    @ApiOperation("新增mg")
    @PreAuthorize("@el.check('mgScore:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody MgScore resources){
        return new ResponseEntity<>(mgScoreService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改mg")
    @ApiOperation("修改mg")
    @PreAuthorize("@el.check('mgScore:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody MgScore resources){
        mgScoreService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除mg")
    @ApiOperation("删除mg")
    @PreAuthorize("@el.check('mgScore:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        mgScoreService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}