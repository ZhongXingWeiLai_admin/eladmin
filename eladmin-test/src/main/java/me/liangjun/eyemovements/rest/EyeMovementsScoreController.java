/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.eyemovements.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.eyemovements.domain.EyeMovementsScore;
import me.liangjun.eyemovements.service.EyeMovementsScoreService;
import me.liangjun.eyemovements.service.dto.EyeMovementsScoreQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-04
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "eyemovements管理")
@RequestMapping("/api/eyeMovementsScore")
public class EyeMovementsScoreController {

    private final EyeMovementsScoreService eyeMovementsScoreService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('eyeMovementsScore:list')")
    public void download(HttpServletResponse response, EyeMovementsScoreQueryCriteria criteria) throws IOException {
        eyeMovementsScoreService.download(eyeMovementsScoreService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询eyemovements")
    @ApiOperation("查询eyemovements")
    @PreAuthorize("@el.check('eyeMovementsScore:list')")
    public ResponseEntity<Object> query(EyeMovementsScoreQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(eyeMovementsScoreService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增eyemovements")
    @ApiOperation("新增eyemovements")
    @PreAuthorize("@el.check('eyeMovementsScore:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody EyeMovementsScore resources){
        return new ResponseEntity<>(eyeMovementsScoreService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改eyemovements")
    @ApiOperation("修改eyemovements")
    @PreAuthorize("@el.check('eyeMovementsScore:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody EyeMovementsScore resources){
        eyeMovementsScoreService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除eyemovements")
    @ApiOperation("删除eyemovements")
    @PreAuthorize("@el.check('eyeMovementsScore:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        eyeMovementsScoreService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}