
/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.eyemovements.service.dto;

import lombok.Data;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-04
**/
@Data
public class EyeMovementsScoreDto implements Serializable {

    /** ID */
    private Long id;

    /** pID */
    private Long pId;

    /** 左眼向上 */
    private Integer eyeMovements1;

    /** 左眼向下 */
    private Integer eyeMovements2;

    /** 左眼向左 */
    private Integer eyeMovements3;

    /** 左眼向右 */
    private Integer eyeMovements4;

    /** 右眼向上 */
    private Integer eyeMovements5;

    /** 右眼向下 */
    private Integer eyeMovements6;

    /** 右眼向左 */
    private Integer eyeMovements7;

    /** 右眼向右 */
    private Integer eyeMovements8;
}