/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.eyemovements.service.impl;

import me.liangjun.eyemovements.domain.EyeMovementsScore;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.eyemovements.repository.EyeMovementsScoreRepository;
import me.liangjun.eyemovements.service.EyeMovementsScoreService;
import me.liangjun.eyemovements.service.dto.EyeMovementsScoreDto;
import me.liangjun.eyemovements.service.dto.EyeMovementsScoreQueryCriteria;
import me.liangjun.eyemovements.service.mapstruct.EyeMovementsScoreMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-04
**/
@Service
@RequiredArgsConstructor
public class EyeMovementsScoreServiceImpl implements EyeMovementsScoreService {

    private final EyeMovementsScoreRepository eyeMovementsScoreRepository;
    private final EyeMovementsScoreMapper eyeMovementsScoreMapper;

    @Override
    public Map<String,Object> queryAll(EyeMovementsScoreQueryCriteria criteria, Pageable pageable){
        Page<EyeMovementsScore> page = eyeMovementsScoreRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(eyeMovementsScoreMapper::toDto));
    }

    @Override
    public List<EyeMovementsScoreDto> queryAll(EyeMovementsScoreQueryCriteria criteria){
        return eyeMovementsScoreMapper.toDto(eyeMovementsScoreRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public EyeMovementsScoreDto findById(Long id) {
        EyeMovementsScore eyeMovementsScore = eyeMovementsScoreRepository.findById(id).orElseGet(EyeMovementsScore::new);
        ValidationUtil.isNull(eyeMovementsScore.getId(),"EyeMovementsScore","id",id);
        return eyeMovementsScoreMapper.toDto(eyeMovementsScore);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public EyeMovementsScoreDto create(EyeMovementsScore resources) {
        return eyeMovementsScoreMapper.toDto(eyeMovementsScoreRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(EyeMovementsScore resources) {
        EyeMovementsScore eyeMovementsScore = eyeMovementsScoreRepository.findById(resources.getId()).orElseGet(EyeMovementsScore::new);
        ValidationUtil.isNull( eyeMovementsScore.getId(),"EyeMovementsScore","id",resources.getId());
        eyeMovementsScore.copy(resources);
        eyeMovementsScoreRepository.save(eyeMovementsScore);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            eyeMovementsScoreRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<EyeMovementsScoreDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (EyeMovementsScoreDto eyeMovementsScore : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", eyeMovementsScore.getPId());
            map.put("左眼向上", eyeMovementsScore.getEyeMovements1());
            map.put("左眼向下", eyeMovementsScore.getEyeMovements2());
            map.put("左眼向左", eyeMovementsScore.getEyeMovements3());
            map.put("左眼向右", eyeMovementsScore.getEyeMovements4());
            map.put("右眼向上", eyeMovementsScore.getEyeMovements5());
            map.put("右眼向下", eyeMovementsScore.getEyeMovements6());
            map.put("右眼向左", eyeMovementsScore.getEyeMovements7());
            map.put("右眼向右", eyeMovementsScore.getEyeMovements8());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public EyeMovementsScoreDto findByPid(Long pid) {
        return eyeMovementsScoreMapper.toDto(eyeMovementsScoreRepository.findByPid(pid));
    }
}