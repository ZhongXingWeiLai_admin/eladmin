/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.eyemovements.repository;

import me.liangjun.eyemovements.domain.EyeMovementsScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-04
**/
public interface EyeMovementsScoreRepository extends JpaRepository<EyeMovementsScore, Long>, JpaSpecificationExecutor<EyeMovementsScore> {
    @Query(value = "select * from eye_movements_score where p_id = ?1", nativeQuery = true)
    EyeMovementsScore findByPid(long pid);
}