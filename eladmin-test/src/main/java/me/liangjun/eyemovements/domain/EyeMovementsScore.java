/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.eyemovements.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-04
**/
@Entity
@Data
@Table(name="eye_movements_score")
public class EyeMovementsScore implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "eye_movements1")
    @ApiModelProperty(value = "左眼向上")
    private Integer eyeMovements1;

    @Column(name = "eye_movements2")
    @ApiModelProperty(value = "左眼向下")
    private Integer eyeMovements2;

    @Column(name = "eye_movements3")
    @ApiModelProperty(value = "左眼向左")
    private Integer eyeMovements3;

    @Column(name = "eye_movements4")
    @ApiModelProperty(value = "左眼向右")
    private Integer eyeMovements4;

    @Column(name = "eye_movements5")
    @ApiModelProperty(value = "右眼向上")
    private Integer eyeMovements5;

    @Column(name = "eye_movements6")
    @ApiModelProperty(value = "右眼向下")
    private Integer eyeMovements6;

    @Column(name = "eye_movements7")
    @ApiModelProperty(value = "右眼向左")
    private Integer eyeMovements7;

    @Column(name = "eye_movements8")
    @ApiModelProperty(value = "右眼向右")
    private Integer eyeMovements8;

    public void copy(EyeMovementsScore source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}