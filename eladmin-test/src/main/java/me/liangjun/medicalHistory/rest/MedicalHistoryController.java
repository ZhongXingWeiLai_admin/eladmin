/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.medicalHistory.rest;

import me.liangjun.medicalHistory.service.dto.MedicalHistoryDto;
import me.zhengjie.annotation.AnonymousAccess;
import me.zhengjie.annotation.Log;
import me.liangjun.medicalHistory.domain.MedicalHistory;
import me.liangjun.medicalHistory.service.MedicalHistoryService;
import me.liangjun.medicalHistory.service.dto.MedicalHistoryQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author jun
* @date 2021-09-08
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "medicalHistory管理")
@RequestMapping("/api/medicalHistory")
public class MedicalHistoryController {

    private final MedicalHistoryService medicalHistoryService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('medicalHistory:list')")
    public void download(HttpServletResponse response, MedicalHistoryQueryCriteria criteria) throws IOException {
        medicalHistoryService.download(medicalHistoryService.queryAll(criteria), response);
    }


    @GetMapping(value = "findByPid")
    @Log("查询medicalHistory")
    @ApiOperation("查询medicalHistory")
//    @PreAuthorize("@el.check('medicalHistory:list')")
    @AnonymousAccess
    public ResponseEntity<Object> findByPid(@RequestParam("id") Long id){
        MedicalHistoryDto medicalHistoryDto = medicalHistoryService.findById(id);
        if (medicalHistoryDto == null){
            return new ResponseEntity<>("null",HttpStatus.OK);

        }
        return new ResponseEntity<>(medicalHistoryDto,HttpStatus.OK);
    }

    @GetMapping
    @Log("查询medicalHistory")
    @ApiOperation("查询medicalHistory")
    @PreAuthorize("@el.check('medicalHistory:list')")
    public ResponseEntity<Object> query(MedicalHistoryQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(medicalHistoryService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增medicalHistory")
    @ApiOperation("新增medicalHistory")
    @PreAuthorize("@el.check('medicalHistory:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody MedicalHistoryDto resources){
        return new ResponseEntity<>(medicalHistoryService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改medicalHistory")
    @ApiOperation("修改medicalHistory")
    @PreAuthorize("@el.check('medicalHistory:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody MedicalHistory resources){
        medicalHistoryService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除medicalHistory")
    @ApiOperation("删除medicalHistory")
    @PreAuthorize("@el.check('medicalHistory:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        medicalHistoryService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}