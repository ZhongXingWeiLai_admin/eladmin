/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.medicalHistory.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author jun
* @date 2021-09-08
**/
@Entity
@Data
@Table(name="medical_history")
public class MedicalHistory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "患者id")
    private Long pId;

    @Column(name = "first_morbidity_time")
    @ApiModelProperty(value = "首次发病时间")
    private Timestamp firstMorbidityTime;

    @Column(name = "morbidity_age")
    @ApiModelProperty(value = "发病年龄")
    private Integer morbidityAge;

    @Column(name = "progress")
    @ApiModelProperty(value = "病程")
    private String progress;

    @Column(name = "confirmed_time")
    @ApiModelProperty(value = "确诊时间")
    private Timestamp confirmedTime;

    @Column(name = "initial_symptom")
    @ApiModelProperty(value = "首发症状")
    private String initialSymptom;

    @Column(name = "detail")
    @ApiModelProperty(value = "首发症状的具体内容")
    private String detail;

    @Column(name = "reason")
    @ApiModelProperty(value = "诱因")
    private Integer reason;

    @Column(name = "most_symptoms")
    @ApiModelProperty(value = "最重症状")
    private String mostSymptoms;

    @Column(name = "most_symptoms_time")
    @ApiModelProperty(value = "病情最重时间")
    private Timestamp mostSymptomsTime;

    @Column(name = "osserman")
    @ApiModelProperty(value = "osserman分型")
    private Integer osserman;

    @Column(name = "mgfa")
    @ApiModelProperty(value = "mgfa分型")
    private Integer mgfa;

    @Column(name = "img")
    @ApiModelProperty(value = "病例图片")
    private String img;

    @Column(name = "t_interval")
    @ApiModelProperty(value = "间隔")
    private Integer tInterval;

    @Column(name = "ignition")
    @ApiModelProperty(value = "眼外肌无力")
    private String ignition;

    @Column(name = "spherical_symptom")
    @ApiModelProperty(value = "球部症状")
    private String sphericalSymptom;

    @Column(name = "fatigue")
    @ApiModelProperty(value = "肢体无力-疲劳")
    private String fatigue;

    @Column(name = "plant_neurogenic")
    @ApiModelProperty(value = "植物神经受累")
    private String plantNeurogenic;

    @Column(name = "is_most_symptoms")
    @ApiModelProperty(value = "是否有最重症状")
    private Integer isMostSymptoms;

    @Column(name = "specific_cause")
    @ApiModelProperty(value = "具体诱因")
    private String specificCause;

    public void copy(MedicalHistory source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}