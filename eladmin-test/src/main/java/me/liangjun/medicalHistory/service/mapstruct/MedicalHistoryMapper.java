/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.medicalHistory.service.mapstruct;

import cn.hutool.core.collection.CollUtil;
import me.zhengjie.base.BaseMapper;
import me.liangjun.medicalHistory.domain.MedicalHistory;
import me.liangjun.medicalHistory.service.dto.MedicalHistoryDto;
import org.mapstruct.*;

import java.util.Arrays;
import java.util.List;

/**
* @website https://el-admin.vip
* @author jun
* @date 2021-09-08
**/
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MedicalHistoryMapper extends BaseMapper<MedicalHistoryDto, MedicalHistory> {

    @Override
    @Mappings({
            @Mapping(source = "initialSymptom",target = "initialSymptom"),
            @Mapping(source = "mostSymptoms",target = "mostSymptoms"),
            @Mapping(source = "ignition",target = "ignition"),
            @Mapping(source = "sphericalSymptom",target = "sphericalSymptom"),
            @Mapping(source = "fatigue",target = "fatigue"),
            @Mapping(source = "plantNeurogenic",target = "plantNeurogenic"),
            @Mapping(source = "specificCause",target = "specificCause"),
    })
    MedicalHistoryDto toDto(MedicalHistory entity);

    @Override
    @Mappings({
            @Mapping(source = "initialSymptom",target = "initialSymptom"),
            @Mapping(source = "mostSymptoms",target = "mostSymptoms"),
            @Mapping(source = "ignition",target = "ignition"),
            @Mapping(source = "sphericalSymptom",target = "sphericalSymptom"),
            @Mapping(source = "fatigue",target = "fatigue"),
            @Mapping(source = "plantNeurogenic",target = "plantNeurogenic"),
            @Mapping(source = "specificCause",target = "specificCause"),
    })
    MedicalHistory toEntity(MedicalHistoryDto medicalHistoryDto);

    @Override
    List<MedicalHistoryDto> toDto(List<MedicalHistory> entityList);

    @Override
    List<MedicalHistory> toEntity(List<MedicalHistoryDto> dtoList);

    default List<String> str2List(String src){
        String[] split = src.split(",");
        List<String> result = Arrays.asList(split);
        return result;
    }

    // list转str
    default String list2Str(List<String> src){
        if (CollUtil.isEmpty(src)) {
            return "";
        }
        StringBuffer sb = new StringBuffer();
        src.stream().forEach(item -> sb.append(item.replace("\"","")).append(","));
        return sb.substring(0, sb.length() - 1).toString();
    }
}