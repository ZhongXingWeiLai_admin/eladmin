/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.medicalHistory.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;
import java.util.List;

/**
* @website https://el-admin.vip
* @description /
* @author jun
* @date 2021-09-08
**/
@Data
public class MedicalHistoryDto implements Serializable {

    /** ID */
    private Long id;

    /** 患者id */
    private Long pId;

    /** 首次发病时间 */
    private Timestamp firstMorbidityTime;

    /** 发病年龄 */
    private Integer morbidityAge;

    /** 病程 */
    private String progress;

    /** 确诊时间 */
    private Timestamp confirmedTime;

    /** 首发症状 */
    private List<String> initialSymptom;

    /** 首发症状的具体内容 */
    private String detail;

    /** 诱因 */
    private Integer reason;

    /** 最重症状 */
    private List<String> mostSymptoms;

    /** 病情最重时间 */
    private Timestamp mostSymptomsTime;

    /** osserman分型 */
    private Integer osserman;

    /** mgfa分型 */
    private Integer mgfa;

    /** 病例图片 */
    private String img;

    /** 间隔 */
    private Integer tInterval;

    /** 眼外肌无力 */
    private List<String> ignition;

    /** 球部症状 */
    private List<String> sphericalSymptom;

    /** 肢体无力-疲劳 */
    private List<String> fatigue;

    /** 植物神经受累 */
    private List<String> plantNeurogenic;

    /** 是否有最重症状 */
    private Integer isMostSymptoms;

    /** 具体诱因 */
    private List<String> specificCause;
}