/*
 *  Copyright 2019-2020 Zheng Jie
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package me.liangjun.medicalHistory.service.impl;

import me.liangjun.medicalHistory.domain.MedicalHistory;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.medicalHistory.repository.MedicalHistoryRepository;
import me.liangjun.medicalHistory.service.MedicalHistoryService;
import me.liangjun.medicalHistory.service.dto.MedicalHistoryDto;
import me.liangjun.medicalHistory.service.dto.MedicalHistoryQueryCriteria;
import me.liangjun.medicalHistory.service.mapstruct.MedicalHistoryMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;

import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * @author jun
 * @website https://el-admin.vip
 * @description 服务实现
 * @date 2021-09-08
 **/
@Service
@RequiredArgsConstructor
public class MedicalHistoryServiceImpl implements MedicalHistoryService {

    private final MedicalHistoryRepository medicalHistoryRepository;
    private final MedicalHistoryMapper medicalHistoryMapper;

    @Override
    public Map<String, Object> queryAll(MedicalHistoryQueryCriteria criteria, Pageable pageable) {
        Page<MedicalHistory> page = medicalHistoryRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(medicalHistoryMapper::toDto));
    }

    @Override
    public List<MedicalHistoryDto> queryAll(MedicalHistoryQueryCriteria criteria) {
        return medicalHistoryMapper.toDto(medicalHistoryRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    @Transactional
    public MedicalHistoryDto findById(Long id) {
        MedicalHistory medicalHistory = medicalHistoryRepository.findByPid(id);
//        ValidationUtil.isNull(medicalHistory.getId(),"MedicalHistory","id",id);
        return medicalHistoryMapper.toDto(medicalHistory);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public MedicalHistoryDto create(MedicalHistoryDto resources) {
        MedicalHistory byPid = medicalHistoryRepository.findByPid(resources.getPId());
        if (byPid != null) {
            byPid.copy(medicalHistoryMapper.toEntity(resources));
            medicalHistoryRepository.save(byPid);
        } else {
            medicalHistoryRepository.save(medicalHistoryMapper.toEntity(resources));
        }

        return resources;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(MedicalHistory resources) {
        MedicalHistory medicalHistory = medicalHistoryRepository.findById(resources.getId()).orElseGet(MedicalHistory::new);
        ValidationUtil.isNull(medicalHistory.getId(), "MedicalHistory", "id", resources.getId());
        medicalHistory.copy(resources);
        medicalHistoryRepository.save(medicalHistory);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            medicalHistoryRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<MedicalHistoryDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (MedicalHistoryDto medicalHistory : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("患者id", medicalHistory.getPId());
            map.put("首次发病时间", medicalHistory.getFirstMorbidityTime());
            map.put("发病年龄", medicalHistory.getMorbidityAge());
            map.put("病程", medicalHistory.getProgress());
            map.put("确诊时间", medicalHistory.getConfirmedTime());
            map.put("首发症状的具体内容", medicalHistory.getDetail());
            map.put("诱因", medicalHistory.getReason());
            map.put("最重症状", medicalHistory.getMostSymptoms());
            map.put("病情最重时间", medicalHistory.getMostSymptomsTime());
            map.put("osserman分型", medicalHistory.getOsserman());
            map.put("mgfa分型", medicalHistory.getMgfa());
            map.put("病例图片", medicalHistory.getImg());
            map.put("间隔", medicalHistory.getTInterval());
            map.put("眼外肌无力", medicalHistory.getIgnition());
            map.put("球部症状", medicalHistory.getSphericalSymptom());
            map.put("肢体无力-疲劳", medicalHistory.getFatigue());
            map.put("植物神经受累", medicalHistory.getPlantNeurogenic());
            map.put("是否有最重症状", medicalHistory.getIsMostSymptoms());
            map.put("具体诱因", medicalHistory.getSpecificCause());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}