/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.typingrecord.service.impl;

import me.liangjun.typingrecord.domain.TypingRecord;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.typingrecord.repository.TypingRecordRepository;
import me.liangjun.typingrecord.service.TypingRecordService;
import me.liangjun.typingrecord.service.dto.TypingRecordDto;
import me.liangjun.typingrecord.service.dto.TypingRecordQueryCriteria;
import me.liangjun.typingrecord.service.mapstruct.TypingRecordMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-04
**/
@Service
@RequiredArgsConstructor
public class TypingRecordServiceImpl implements TypingRecordService {

    private final TypingRecordRepository typingRecordRepository;
    private final TypingRecordMapper typingRecordMapper;

    @Override
    public Map<String,Object> queryAll(TypingRecordQueryCriteria criteria, Pageable pageable){
        Page<TypingRecord> page = typingRecordRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(typingRecordMapper::toDto));
    }

    @Override
    public List<TypingRecordDto> queryAll(TypingRecordQueryCriteria criteria){
        return typingRecordMapper.toDto(typingRecordRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public TypingRecordDto findById(Long id) {
        TypingRecord typingRecord = typingRecordRepository.findById(id).orElseGet(TypingRecord::new);
        ValidationUtil.isNull(typingRecord.getId(),"TypingRecord","id",id);
        return typingRecordMapper.toDto(typingRecord);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public TypingRecordDto create(TypingRecord resources) {
        return typingRecordMapper.toDto(typingRecordRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(TypingRecord resources) {
        TypingRecord typingRecord = typingRecordRepository.findById(resources.getId()).orElseGet(TypingRecord::new);
        ValidationUtil.isNull( typingRecord.getId(),"TypingRecord","id",resources.getId());
        typingRecord.copy(resources);
        typingRecordRepository.save(typingRecord);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            typingRecordRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<TypingRecordDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (TypingRecordDto typingRecord : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", typingRecord.getPId());
            map.put("Ossrman分型记录", typingRecord.getOssrman());
            map.put("MGFA分型记录", typingRecord.getMgfa());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}