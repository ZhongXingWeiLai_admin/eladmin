/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.typingrecord.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.typingrecord.domain.TypingRecord;
import me.liangjun.typingrecord.service.TypingRecordService;
import me.liangjun.typingrecord.service.dto.TypingRecordQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-04
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "typingrecord管理")
@RequestMapping("/api/typingRecord")
public class TypingRecordController {

    private final TypingRecordService typingRecordService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('typingRecord:list')")
    public void download(HttpServletResponse response, TypingRecordQueryCriteria criteria) throws IOException {
        typingRecordService.download(typingRecordService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询typingrecord")
    @ApiOperation("查询typingrecord")
    @PreAuthorize("@el.check('typingRecord:list')")
    public ResponseEntity<Object> query(TypingRecordQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(typingRecordService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增typingrecord")
    @ApiOperation("新增typingrecord")
    @PreAuthorize("@el.check('typingRecord:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody TypingRecord resources){
        return new ResponseEntity<>(typingRecordService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改typingrecord")
    @ApiOperation("修改typingrecord")
    @PreAuthorize("@el.check('typingRecord:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody TypingRecord resources){
        typingRecordService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除typingrecord")
    @ApiOperation("删除typingrecord")
    @PreAuthorize("@el.check('typingRecord:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        typingRecordService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}