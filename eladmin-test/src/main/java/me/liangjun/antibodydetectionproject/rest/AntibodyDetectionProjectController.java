/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.antibodydetectionproject.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.antibodydetectionproject.domain.AntibodyDetectionProject;
import me.liangjun.antibodydetectionproject.service.AntibodyDetectionProjectService;
import me.liangjun.antibodydetectionproject.service.dto.AntibodyDetectionProjectQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-04
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "antibodydetectionproject管理")
@RequestMapping("/api/antibodyDetectionProject")
public class AntibodyDetectionProjectController {

    private final AntibodyDetectionProjectService antibodyDetectionProjectService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('antibodyDetectionProject:list')")
    public void download(HttpServletResponse response, AntibodyDetectionProjectQueryCriteria criteria) throws IOException {
        antibodyDetectionProjectService.download(antibodyDetectionProjectService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询antibodydetectionproject")
    @ApiOperation("查询antibodydetectionproject")
    @PreAuthorize("@el.check('antibodyDetectionProject:list')")
    public ResponseEntity<Object> query(AntibodyDetectionProjectQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(antibodyDetectionProjectService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增antibodydetectionproject")
    @ApiOperation("新增antibodydetectionproject")
    @PreAuthorize("@el.check('antibodyDetectionProject:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody AntibodyDetectionProject resources){
        return new ResponseEntity<>(antibodyDetectionProjectService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改antibodydetectionproject")
    @ApiOperation("修改antibodydetectionproject")
    @PreAuthorize("@el.check('antibodyDetectionProject:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody AntibodyDetectionProject resources){
        antibodyDetectionProjectService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除antibodydetectionproject")
    @ApiOperation("删除antibodydetectionproject")
    @PreAuthorize("@el.check('antibodyDetectionProject:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        antibodyDetectionProjectService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}