/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.antibodydetectionproject.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-04
**/
@Entity
@Data
@Table(name="antibody_detection_project")
public class AntibodyDetectionProject implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "a_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "抗体检测ID")
    private Long aId;

    @Column(name = "antibody_type")
    @ApiModelProperty(value = "检测时间")
    private Integer antibodyType;

    @Column(name = "antibody_result")
    @ApiModelProperty(value = "结果")
    private String antibodyResult;

    @Column(name = "antibody_method")
    @ApiModelProperty(value = "检查方法")
    private String antibodyMethod;

    @Column(name = "antibody_num")
    @ApiModelProperty(value = "数值结果")
    private String antibodyNum;

    @Column(name = "picture")
    @ApiModelProperty(value = "图片")
    private String picture;

    public void copy(AntibodyDetectionProject source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}