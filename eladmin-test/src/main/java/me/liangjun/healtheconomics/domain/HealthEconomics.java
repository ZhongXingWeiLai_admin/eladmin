/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.healtheconomics.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-16
**/
@Entity
@Data
@Table(name="health_economics")
public class HealthEconomics implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "ID")
    private Long id;

    @Column(name = "p_id",nullable = false)
    @NotNull
    @ApiModelProperty(value = "pID")
    private Long pId;

    @Column(name = "follow_up_start_date")
    @ApiModelProperty(value = "实际随访日期-起始时间")
    private Timestamp followUpStartDate;

    @Column(name = "follow_up_end_date")
    @ApiModelProperty(value = "实际随访日期-起始时间")
    private Timestamp followUpEndDate;

    @Column(name = "mg_cost")
    @ApiModelProperty(value = "MG治疗总费用（元）")
    private String mgCost;

    @Column(name = "medical_insurance_type")
    @ApiModelProperty(value = "医保类型")
    private String medicalInsuranceType;

    @Column(name = "is_remote_medical")
    @ApiModelProperty(value = "是否异地就医")
    private Integer isRemoteMedical;

    @Column(name = "edical_insurance_reimbursement_ratio")
    @ApiModelProperty(value = "医保报销比例")
    private String edicalInsuranceReimbursementRatio;

    @Column(name = "is_have_supplementary_medical_insurance")
    @ApiModelProperty(value = "是否有补充医疗保险")
    private Integer isHaveSupplementaryMedicalInsurance;

    @Column(name = "business_insurance_type")
    @ApiModelProperty(value = "商业保险类型")
    private String businessInsuranceType;

    @Column(name = "business_insurance_reimbursement_ratio")
    @ApiModelProperty(value = "商业保险报销比例")
    private String businessInsuranceReimbursementRatio;

    @Column(name = "edical_insurance_reimbursement_cost")
    @ApiModelProperty(value = "医保报销费用（元）")
    private String edicalInsuranceReimbursementCost;

    @Column(name = "own_cost")
    @ApiModelProperty(value = "自费金额（元）")
    private String ownCost;

    @Column(name = "mg_hospitalization_start_date")
    @ApiModelProperty(value = "MG患者住院起始时间")
    private Timestamp mgHospitalizationStartDate;

    @Column(name = "mg_hospitalization_end_date")
    @ApiModelProperty(value = "MG患者住院结束时间")
    private Timestamp mgHospitalizationEndDate;

    @Column(name = "mg_hospitalization_days")
    @ApiModelProperty(value = "MG患者住院时长（天）")
    private String mgHospitalizationDays;

    @Column(name = "mg_hospitalization_cost")
    @ApiModelProperty(value = "MG患者住院期间总费用（元）")
    private String mgHospitalizationCost;

    @Column(name = "extra_hospital_treatment_cost")
    @ApiModelProperty(value = "院外附加治疗费用（元）")
    private String extraHospitalTreatmentCost;

    @Column(name = "blood_products_protein_products_cost")
    @ApiModelProperty(value = "血液制品和蛋白质制品费用（元）")
    private String bloodProductsProteinProductsCost;

    @Column(name = "family_members")
    @ApiModelProperty(value = "陪护家属人数")
    private String familyMembers;

    @Column(name = "visits_number_transportation")
    @ApiModelProperty(value = "乘坐交通工具前往就诊次数")
    private String visitsNumberTransportation;

    @Column(name = "transportation_cost_average")
    @ApiModelProperty(value = "平均每次人均交通费用（元）")
    private String transportationCostAverage;

    @Column(name = "total_transportation_cost")
    @ApiModelProperty(value = "总交通费（元）")
    private String totalTransportationCost;

    @Column(name = "out_of_hospital_accommodation_days")
    @ApiModelProperty(value = "患者院外住宿时长（天）")
    private String outOfHospitalAccommodationDays;

    @Column(name = "out_of_hospital_accommodation_cost_average")
    @ApiModelProperty(value = "患者院外住宿日均费用（元）")
    private String outOfHospitalAccommodationCostAverage;

    @Column(name = "diet_cost_average")
    @ApiModelProperty(value = "患者膳食日均费用（元）")
    private String dietCostAverage;

    @Column(name = "family_members_out_of_hospital_accommodation_days")
    @ApiModelProperty(value = "家属院外住宿时长（天）")
    private String familyMembersOutOfHospitalAccommodationDays;

    @Column(name = "family_members_out_of_hospital_accommodation_cost_average")
    @ApiModelProperty(value = "家属院外住宿日均费用（元）")
    private String familyMembersOutOfHospitalAccommodationCostAverage;

    @Column(name = "family_members_diet_cost_average")
    @ApiModelProperty(value = "家属膳食日均费用（元）")
    private String familyMembersDietCostAverage;

    @Column(name = "total_diet_cost")
    @ApiModelProperty(value = "总食宿费（元）")
    private String totalDietCost;

    @Column(name = "paramedic_num")
    @ApiModelProperty(value = "护理人员人数")
    private String paramedicNum;

    @Column(name = "escort_fee")
    @ApiModelProperty(value = "总陪护费（非家属）（元）")
    private String escortFee;

    @Column(name = "non_medical_costs")
    @ApiModelProperty(value = "直接非医疗成本总费用（元）")
    private String nonMedicalCosts;

    @Column(name = "days_lost")
    @ApiModelProperty(value = "患者误工天数")
    private String daysLost;

    @Column(name = "work_type")
    @ApiModelProperty(value = "患者工种")
    private String workType;

    @Column(name = "loss_cost")
    @ApiModelProperty(value = "患者误工总损失费（元）")
    private String lossCost;

    @Column(name = "family_members_days_lost")
    @ApiModelProperty(value = "家属误工天数")
    private String familyMembersDaysLost;

    @Column(name = "family_members_work_type")
    @ApiModelProperty(value = "家属工种")
    private String familyMembersWorkType;

    @Column(name = "family_members_loss_cost")
    @ApiModelProperty(value = "家属误工总损失费（元）")
    private String familyMembersLossCost;

    @Column(name = "is_change_work_type")
    @ApiModelProperty(value = "患者是否因患病更换工种")
    private Integer isChangeWorkType;

    @Column(name = "is_suspension")
    @ApiModelProperty(value = "患者是否因患病而休学")
    private Integer isSuspension;

    @Column(name = "id_retire")
    @ApiModelProperty(value = "患者是否因患病而退休")
    private Integer idRetire;

    @Column(name = "indirect_cost")
    @ApiModelProperty(value = "间接成本总费用（元）")
    private String indirectCost;

    @Column(name = "eq_5d_5l1")
    @ApiModelProperty(value = "行动能力----我四处活动")
    private Integer eq5d5l1;

    @Column(name = "eq_5d_5l2")
    @ApiModelProperty(value = "自我照顾----我自己洗澡或洗衣")
    private Integer eq5d5l2;

    @Column(name = "eq_5d_5l3")
    @ApiModelProperty(value = "日常活动（如工作、学习、家务、家庭或休闲活动）")
    private Integer eq5d5l3;

    @Column(name = "eq_5d_5l4")
    @ApiModelProperty(value = "行动能力----我四处活动")
    private Integer eq5d5l4;

    @Column(name = "eq_5d_5l5")
    @ApiModelProperty(value = "焦虑或沮丧")
    private Integer eq5d5l5;

    public void copy(HealthEconomics source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}