/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.healtheconomics.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author wei
* @date 2021-09-16
**/
@Data
public class HealthEconomicsDto implements Serializable {

    /** ID */
    private Long id;

    /** pID */
    private Long pId;

    /** 实际随访日期-起始时间 */
    private Timestamp followUpStartDate;

    /** 实际随访日期-起始时间 */
    private Timestamp followUpEndDate;

    /** MG治疗总费用（元） */
    private String mgCost;

    /** 医保类型 */
    private String medicalInsuranceType;

    /** 是否异地就医 */
    private Integer isRemoteMedical;

    /** 医保报销比例 */
    private String edicalInsuranceReimbursementRatio;

    /** 是否有补充医疗保险 */
    private Integer isHaveSupplementaryMedicalInsurance;

    /** 商业保险类型 */
    private String businessInsuranceType;

    /** 商业保险报销比例 */
    private String businessInsuranceReimbursementRatio;

    /** 医保报销费用（元） */
    private String edicalInsuranceReimbursementCost;

    /** 自费金额（元） */
    private String ownCost;

    /** MG患者住院起始时间 */
    private Timestamp mgHospitalizationStartDate;

    /** MG患者住院结束时间 */
    private Timestamp mgHospitalizationEndDate;

    /** MG患者住院时长（天） */
    private String mgHospitalizationDays;

    /** MG患者住院期间总费用（元） */
    private String mgHospitalizationCost;

    /** 院外附加治疗费用（元） */
    private String extraHospitalTreatmentCost;

    /** 血液制品和蛋白质制品费用（元） */
    private String bloodProductsProteinProductsCost;

    /** 陪护家属人数 */
    private String familyMembers;

    /** 乘坐交通工具前往就诊次数 */
    private String visitsNumberTransportation;

    /** 平均每次人均交通费用（元） */
    private String transportationCostAverage;

    /** 总交通费（元） */
    private String totalTransportationCost;

    /** 患者院外住宿时长（天） */
    private String outOfHospitalAccommodationDays;

    /** 患者院外住宿日均费用（元） */
    private String outOfHospitalAccommodationCostAverage;

    /** 患者膳食日均费用（元） */
    private String dietCostAverage;

    /** 家属院外住宿时长（天） */
    private String familyMembersOutOfHospitalAccommodationDays;

    /** 家属院外住宿日均费用（元） */
    private String familyMembersOutOfHospitalAccommodationCostAverage;

    /** 家属膳食日均费用（元） */
    private String familyMembersDietCostAverage;

    /** 总食宿费（元） */
    private String totalDietCost;

    /** 护理人员人数 */
    private String paramedicNum;

    /** 总陪护费（非家属）（元） */
    private String escortFee;

    /** 直接非医疗成本总费用（元） */
    private String nonMedicalCosts;

    /** 患者误工天数 */
    private String daysLost;

    /** 患者工种 */
    private String workType;

    /** 患者误工总损失费（元） */
    private String lossCost;

    /** 家属误工天数 */
    private String familyMembersDaysLost;

    /** 家属工种 */
    private String familyMembersWorkType;

    /** 家属误工总损失费（元） */
    private String familyMembersLossCost;

    /** 患者是否因患病更换工种 */
    private Integer isChangeWorkType;

    /** 患者是否因患病而休学 */
    private Integer isSuspension;

    /** 患者是否因患病而退休 */
    private Integer idRetire;

    /** 间接成本总费用（元） */
    private String indirectCost;

    /** 行动能力----我四处活动 */
    private Integer eq5d5l1;

    /** 自我照顾----我自己洗澡或洗衣 */
    private Integer eq5d5l2;

    /** 日常活动（如工作、学习、家务、家庭或休闲活动） */
    private Integer eq5d5l3;

    /** 行动能力----我四处活动 */
    private Integer eq5d5l4;

    /** 焦虑或沮丧 */
    private Integer eq5d5l5;
}