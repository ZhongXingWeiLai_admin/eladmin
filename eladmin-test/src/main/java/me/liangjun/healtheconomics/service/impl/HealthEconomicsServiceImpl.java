/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.healtheconomics.service.impl;

import me.liangjun.healtheconomics.domain.HealthEconomics;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import me.liangjun.healtheconomics.repository.HealthEconomicsRepository;
import me.liangjun.healtheconomics.service.HealthEconomicsService;
import me.liangjun.healtheconomics.service.dto.HealthEconomicsDto;
import me.liangjun.healtheconomics.service.dto.HealthEconomicsQueryCriteria;
import me.liangjun.healtheconomics.service.mapstruct.HealthEconomicsMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author wei
* @date 2021-09-16
**/
@Service
@RequiredArgsConstructor
public class HealthEconomicsServiceImpl implements HealthEconomicsService {

    private final HealthEconomicsRepository healthEconomicsRepository;
    private final HealthEconomicsMapper healthEconomicsMapper;

    @Override
    public Map<String,Object> queryAll(HealthEconomicsQueryCriteria criteria, Pageable pageable){
        Page<HealthEconomics> page = healthEconomicsRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(healthEconomicsMapper::toDto));
    }

    @Override
    public List<HealthEconomicsDto> queryAll(HealthEconomicsQueryCriteria criteria){
        return healthEconomicsMapper.toDto(healthEconomicsRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public HealthEconomicsDto findById(Long id) {
        HealthEconomics healthEconomics = healthEconomicsRepository.findById(id).orElseGet(HealthEconomics::new);
        ValidationUtil.isNull(healthEconomics.getId(),"HealthEconomics","id",id);
        return healthEconomicsMapper.toDto(healthEconomics);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public HealthEconomicsDto create(HealthEconomics resources) {
        return healthEconomicsMapper.toDto(healthEconomicsRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(HealthEconomics resources) {
        HealthEconomics healthEconomics = healthEconomicsRepository.findById(resources.getId()).orElseGet(HealthEconomics::new);
        ValidationUtil.isNull( healthEconomics.getId(),"HealthEconomics","id",resources.getId());
        healthEconomics.copy(resources);
        healthEconomicsRepository.save(healthEconomics);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            healthEconomicsRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<HealthEconomicsDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (HealthEconomicsDto healthEconomics : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("pID", healthEconomics.getPId());
            map.put("实际随访日期-起始时间", healthEconomics.getFollowUpStartDate());
            map.put("实际随访日期-起始时间", healthEconomics.getFollowUpEndDate());
            map.put("MG治疗总费用（元）", healthEconomics.getMgCost());
            map.put("医保类型", healthEconomics.getMedicalInsuranceType());
            map.put("是否异地就医", healthEconomics.getIsRemoteMedical());
            map.put("医保报销比例", healthEconomics.getEdicalInsuranceReimbursementRatio());
            map.put("是否有补充医疗保险", healthEconomics.getIsHaveSupplementaryMedicalInsurance());
            map.put("商业保险类型", healthEconomics.getBusinessInsuranceType());
            map.put("商业保险报销比例", healthEconomics.getBusinessInsuranceReimbursementRatio());
            map.put("医保报销费用（元）", healthEconomics.getEdicalInsuranceReimbursementCost());
            map.put("自费金额（元）", healthEconomics.getOwnCost());
            map.put("MG患者住院起始时间", healthEconomics.getMgHospitalizationStartDate());
            map.put("MG患者住院结束时间", healthEconomics.getMgHospitalizationEndDate());
            map.put("MG患者住院时长（天）", healthEconomics.getMgHospitalizationDays());
            map.put("MG患者住院期间总费用（元）", healthEconomics.getMgHospitalizationCost());
            map.put("院外附加治疗费用（元）", healthEconomics.getExtraHospitalTreatmentCost());
            map.put("血液制品和蛋白质制品费用（元）", healthEconomics.getBloodProductsProteinProductsCost());
            map.put("陪护家属人数", healthEconomics.getFamilyMembers());
            map.put("乘坐交通工具前往就诊次数", healthEconomics.getVisitsNumberTransportation());
            map.put("平均每次人均交通费用（元）", healthEconomics.getTransportationCostAverage());
            map.put("总交通费（元）", healthEconomics.getTotalTransportationCost());
            map.put("患者院外住宿时长（天）", healthEconomics.getOutOfHospitalAccommodationDays());
            map.put("患者院外住宿日均费用（元）", healthEconomics.getOutOfHospitalAccommodationCostAverage());
            map.put("患者膳食日均费用（元）", healthEconomics.getDietCostAverage());
            map.put("家属院外住宿时长（天）", healthEconomics.getFamilyMembersOutOfHospitalAccommodationDays());
            map.put("家属院外住宿日均费用（元）", healthEconomics.getFamilyMembersOutOfHospitalAccommodationCostAverage());
            map.put("家属膳食日均费用（元）", healthEconomics.getFamilyMembersDietCostAverage());
            map.put("总食宿费（元）", healthEconomics.getTotalDietCost());
            map.put("护理人员人数", healthEconomics.getParamedicNum());
            map.put("总陪护费（非家属）（元）", healthEconomics.getEscortFee());
            map.put("直接非医疗成本总费用（元）", healthEconomics.getNonMedicalCosts());
            map.put("患者误工天数", healthEconomics.getDaysLost());
            map.put("患者工种", healthEconomics.getWorkType());
            map.put("患者误工总损失费（元）", healthEconomics.getLossCost());
            map.put("家属误工天数", healthEconomics.getFamilyMembersDaysLost());
            map.put("家属工种", healthEconomics.getFamilyMembersWorkType());
            map.put("家属误工总损失费（元）", healthEconomics.getFamilyMembersLossCost());
            map.put("患者是否因患病更换工种", healthEconomics.getIsChangeWorkType());
            map.put("患者是否因患病而休学", healthEconomics.getIsSuspension());
            map.put("患者是否因患病而退休", healthEconomics.getIdRetire());
            map.put("间接成本总费用（元）", healthEconomics.getIndirectCost());
            map.put("行动能力----我四处活动", healthEconomics.getEq5d5l1());
            map.put("自我照顾----我自己洗澡或洗衣", healthEconomics.getEq5d5l2());
            map.put("日常活动（如工作、学习、家务、家庭或休闲活动）", healthEconomics.getEq5d5l3());
            map.put("行动能力----我四处活动", healthEconomics.getEq5d5l4());
            map.put("焦虑或沮丧", healthEconomics.getEq5d5l5());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}