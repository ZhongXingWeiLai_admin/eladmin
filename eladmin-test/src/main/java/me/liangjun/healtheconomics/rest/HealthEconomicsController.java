/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package me.liangjun.healtheconomics.rest;

import me.zhengjie.annotation.Log;
import me.liangjun.healtheconomics.domain.HealthEconomics;
import me.liangjun.healtheconomics.service.HealthEconomicsService;
import me.liangjun.healtheconomics.service.dto.HealthEconomicsQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author wei
* @date 2021-09-16
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "healtHeconomics管理")
@RequestMapping("/api/healthEconomics")
public class HealthEconomicsController {

    private final HealthEconomicsService healthEconomicsService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('healthEconomics:list')")
    public void download(HttpServletResponse response, HealthEconomicsQueryCriteria criteria) throws IOException {
        healthEconomicsService.download(healthEconomicsService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询healtHeconomics")
    @ApiOperation("查询healtHeconomics")
    @PreAuthorize("@el.check('healthEconomics:list')")
    public ResponseEntity<Object> query(HealthEconomicsQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(healthEconomicsService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增healtHeconomics")
    @ApiOperation("新增healtHeconomics")
    @PreAuthorize("@el.check('healthEconomics:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody HealthEconomics resources){
        return new ResponseEntity<>(healthEconomicsService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改healtHeconomics")
    @ApiOperation("修改healtHeconomics")
    @PreAuthorize("@el.check('healthEconomics:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody HealthEconomics resources){
        healthEconomicsService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除healtHeconomics")
    @ApiOperation("删除healtHeconomics")
    @PreAuthorize("@el.check('healthEconomics:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        healthEconomicsService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}