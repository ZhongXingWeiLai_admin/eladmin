package me.zhengjie.config;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ProvinceInfo {
    private String code;
    private String name;
    public ProvinceInfo(String code,String name){
        this.code = code;
        this.name = name;
    }
    public static List<ProvinceInfo> getAllProvince(){
        List<ProvinceInfo> provinceInfos = new ArrayList<>();
        provinceInfos.add(new ProvinceInfo("110000","北京市"));
        provinceInfos.add(new ProvinceInfo("120000","天津市"));
        provinceInfos.add(new ProvinceInfo("130000","河北省"));
        provinceInfos.add(new ProvinceInfo("140000","山西省"));
        provinceInfos.add(new ProvinceInfo("150000","内蒙古自治区"));
        provinceInfos.add(new ProvinceInfo("210000","辽宁省"));
        provinceInfos.add(new ProvinceInfo("220000","吉林省"));
        provinceInfos.add(new ProvinceInfo("230000","黑龙江省"));
        provinceInfos.add(new ProvinceInfo("310000","上海市"));
        provinceInfos.add(new ProvinceInfo("320000","江苏省"));
        provinceInfos.add(new ProvinceInfo("330000","浙江省"));
        provinceInfos.add(new ProvinceInfo("340000","安徽省"));
        provinceInfos.add(new ProvinceInfo("350000","福建省"));
        provinceInfos.add(new ProvinceInfo("360000","江西省"));
        provinceInfos.add(new ProvinceInfo("370000","山东省"));
        provinceInfos.add(new ProvinceInfo("410000","河南省"));
        provinceInfos.add(new ProvinceInfo("420000","湖北省"));
        provinceInfos.add(new ProvinceInfo("430000","湖南省"));
        provinceInfos.add(new ProvinceInfo("440000","广东省"));
        provinceInfos.add(new ProvinceInfo("450000","广西壮族自治区"));
        provinceInfos.add(new ProvinceInfo("460000","海南省"));
        provinceInfos.add(new ProvinceInfo("500000","重庆市"));
        provinceInfos.add(new ProvinceInfo("510000","四川省"));
        provinceInfos.add(new ProvinceInfo("520000","贵州省"));
        provinceInfos.add(new ProvinceInfo("530000","云南省"));
        provinceInfos.add(new ProvinceInfo("540000","西藏自治区"));
        provinceInfos.add(new ProvinceInfo("610000","陕西省"));
        provinceInfos.add(new ProvinceInfo("620000","甘肃省"));
        provinceInfos.add(new ProvinceInfo("630000","青海省"));
        provinceInfos.add(new ProvinceInfo("640000","宁夏回族自治区"));
        provinceInfos.add(new ProvinceInfo("650000","新疆维吾尔自治区"));
        provinceInfos.add(new ProvinceInfo("710000","台湾省"));
        provinceInfos.add(new ProvinceInfo("810000","香港特别行政区"));
        provinceInfos.add(new ProvinceInfo("820000","澳门特别行政区"));
        return provinceInfos;
    }
}
